﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class ShowPanelsScript : MonoBehaviour, IShowPanelsScript
{
    //    private static readonly JCsLogger log = new JCsLogger(typeof(ShowPanelsScript));
    private NeedleOperationScript _needleOperationScript;
    private PointVisibilityScript _pointVisibilityScript;
    private GuiPanels _guiPanels;
    private List<string> instructions = new List<string>();
    public GameObject patientselectionpanelposition;
    public GameObject transparencyPanelposition;
    void Start()
    {
        try
        {
            _guiPanels = Camera.main.GetComponent<GuiPanels>();
            _needleOperationScript = Camera.main.GetComponent<NeedleOperationScript>();
            _pointVisibilityScript = Camera.main.GetComponent<PointVisibilityScript>();
            ShowLoginPanel();
        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }
    }

    public void ShowHome()
    {
        try
        {
            _guiPanels.Mainmenupanel.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_guiPanels.Menupanel.transform.GetChild(1).gameObject);
            Color32 color = EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color;
            EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color = new Color32(color.r, color.g, color.b, 175);
            _guiPanels.Patientpanel.gameObject.SetActive(false);
            _guiPanels.Addprofilepanel.gameObject.SetActive(false);
            _guiPanels.EditProfilePanel.SetActive(false);
            _guiPanels.Treatmentpanel.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = false;
            _guiPanels.instructionPanel.SetActive(false);
            _guiPanels.SessionEditorPanel.SetActive(false);
            _guiPanels.sessionHistoryPanel.SetActive(false);
            _guiPanels.MeridianPanel.SetActive(false);
            _guiPanels.ColorBox.SetActive(false);
            _guiPanels.Menupanel.gameObject.SetActive(true);
            TranslationScript.TranslateMenupanel();
            _guiPanels.patientselectionpanel.gameObject.SetActive(true);
            TranslationScript.TranslatePatientSelectionPanel();
            _guiPanels.Treatmentpanel.gameObject.SetActive(true);
            TranslationScript.TranslateTreatmentpanel();
            _guiPanels.Buttonpanel.SetActive(false);
            _guiPanels.transparencyPanel.SetActive(false);
            _guiPanels.FinancePanel.SetActive(false);
            _guiPanels.patientselectionpanel.transform.GetChild(1).gameObject.SetActive(true);
        }
        catch (Exception exception)
        {

            LogScript.Log(exception);
        }

    }
    #region IShowPanelsScript Members
    public void TreatmentLogButtonClick()
    {
        try
        {
            int DoctorID = Camera.main.GetComponent<UserIdScript>().DoctorID;
            _needleOperationScript.LoadAllpatient(DoctorID);
            _guiPanels.Mainmenupanel.SetActive(true);
            _guiPanels.Menupanel.SetActive(true);
            TranslationScript.TranslateMenupanel();
            _guiPanels.Patientpanel.gameObject.SetActive(false);
            _guiPanels.Addprofilepanel.gameObject.SetActive(false);
            _guiPanels.EditProfilePanel.SetActive(false);
            _guiPanels.Treatmentpanel.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = false;
            _guiPanels.instructionPanel.SetActive(false);
            _guiPanels.SessionEditorPanel.SetActive(false);
            _guiPanels.sessionHistoryPanel.SetActive(false);
            _guiPanels.MeridianPanel.SetActive(false);
            _guiPanels.ColorBox.SetActive(false);
            _guiPanels.FinancePanel.SetActive(false);
            _guiPanels.patientselectionpanel.gameObject.SetActive(true);
            TranslationScript.TranslatePatientSelectionPanel();
            _guiPanels.Treatmentpanel.gameObject.SetActive(true);
            TranslationScript.TranslateTreatmentpanel();
            _guiPanels.Buttonpanel.SetActive(false);
            _guiPanels.transparencyPanel.SetActive(false);
            _guiPanels.Treatmentpanel.transform.GetChild(3).GetComponent<Button>().interactable = (PointVisibilityScript.selectedNeedles.Count > 0) ? true : false;
            _guiPanels.patientselectionpanel.transform.GetChild(1).gameObject.SetActive(true);
            _guiPanels.transparencyPanel.transform.localPosition = transparencyPanelposition.transform.localPosition;
            _guiPanels.patientselectionpanel.transform.localPosition = patientselectionpanelposition.transform.localPosition;
            PointVisibilityScript.isSessionOperating = false;
            _guiPanels.Buttonpanel.transform.GetChild(3).gameObject.SetActive(false);
        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }

    }
    public void PatientProfileButtonClick()
    {
        try
        {
            _guiPanels.Patientpanel.gameObject.SetActive(true);
            TranslationScript.TranslatePatientPanel();
            _guiPanels.Treatmentpanel.gameObject.SetActive(false);
            _guiPanels.Addprofilepanel.gameObject.SetActive(false);
            _guiPanels.sessionHistoryPanel.gameObject.SetActive(false);
            _guiPanels.EditProfilePanel.gameObject.SetActive(false);
            _guiPanels.patientselectionpanel.gameObject.SetActive(false);
            _guiPanels.SessionEditorPanel.SetActive(false);
            _guiPanels.instructionPanel.SetActive(false);
            _guiPanels.FinancePanel.SetActive(false);
        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }

    }
    public void MasterButtonClick()
    {
        try
        {
            _guiPanels.Treatmentpanel.gameObject.SetActive(false);
            _guiPanels.Patientpanel.gameObject.SetActive(false);
            _guiPanels.Addprofilepanel.gameObject.SetActive(false);
            _guiPanels.EditProfilePanel.gameObject.SetActive(false);
            _guiPanels.patientselectionpanel.gameObject.SetActive(false);
            _guiPanels.SessionEditorPanel.SetActive(false);
            _guiPanels.instructionPanel.SetActive(false);
            _guiPanels.sessionHistoryPanel.SetActive(false);
            _guiPanels.Mainmenupanel.gameObject.SetActive(false);
            _guiPanels.Buttonpanel.gameObject.SetActive(true);
            TranslationScript.TranslateButtonpanel();
            _guiPanels.MeridianPanel.SetActive(true);
            TranslationScript.TranslateMeridianPanel();
            _guiPanels.transparencyPanel.SetActive(true);
            TranslationScript.TranslatetransparencyPanel();
            _guiPanels.transparencyPanel.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0f);
            _guiPanels.transparencyPanel.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0f);
            _guiPanels.transparencyPanel.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0f);
            _guiPanels.transparencyPanel.transform.position = new Vector3(_guiPanels.transparencyPanel.transform.position.x, 0f, 0f);
            _guiPanels.FinancePanel.SetActive(false);
            _guiPanels.Buttonpanel.transform.GetChild(0).gameObject.SetActive(true);
            _guiPanels.Buttonpanel.transform.GetChild(3).gameObject.SetActive(false);
            StartCoroutine(_pointVisibilityScript.WaitUntillNeedleLoaded());
#if UNITY_ANDROID || UNITY_IOS
            _guiPanels.Buttonpanel.transform.GetChild(2).gameObject.SetActive(true);
#endif
        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }

    }
    public void InstructionsButtonClick()
    {
        try
        {
            instructions.Clear();
            _guiPanels.instructionPanel.SetActive(true);
            TranslationScript.TranslateinstructionPanel();
            _guiPanels.Treatmentpanel.SetActive(false);
            _guiPanels.Patientpanel.SetActive(false);
            _guiPanels.patientselectionpanel.SetActive(false);
            _guiPanels.Addprofilepanel.SetActive(false);
            _guiPanels.EditProfilePanel.SetActive(false);
            _guiPanels.SessionEditorPanel.SetActive(false);
            _guiPanels.sessionHistoryPanel.SetActive(false);
            _guiPanels.FinancePanel.SetActive(false);
#if UNITY_STANDALONE
            for (int i = 1; i <= 9; i++)
            {
                instructions.Add(Language.Get("ins_p" + i));
            }
#endif
#if UNITY_ANDROID || UNITY_IOS
       for (int i = 1; i <= 9; i++)
        {
            instructions.Add(Language.Get("ins_m" + i));
        }
#endif
            for (int i = 0; i < instructions.Count; i++)
            {
                _guiPanels.instructionPanel.transform.GetChild(1).GetChild(i).GetComponent<Text>().text = instructions[i];
            }
        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }

    }
    public void AddsessionButtonClick()
    {
        try
        {
            if (PointVisibilityScript.selectedNeedles.Count > 0)
                ShowErrorPanel("Please finish current session first before adding a new one", "Warning");
            else
                StartCoroutine("waitandaddsession");
        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }

    }
    public void ContinueSession()
    {
        StartCoroutine("waitandaddsession");
    }


    private IEnumerator waitandaddsession()
    {
        try
        {
            PointVisibilityScript.isSessionOperating = true;
            _guiPanels.patientselectionpanel.SetActive(false);
            _guiPanels.Mainmenupanel.SetActive(false);
            _guiPanels.SessionEditorPanel.SetActive(true);
            TranslationScript.TranslateSessionEditorPanel();
            _guiPanels.transparencyPanel.SetActive(true);
            TranslationScript.TranslatetransparencyPanel();
            _guiPanels.Buttonpanel.SetActive(true);
            TranslationScript.TranslateButtonpanel();
            _guiPanels.transparencyPanel.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0f);
            _guiPanels.transparencyPanel.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0f);
            _guiPanels.transparencyPanel.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0f);
            _guiPanels.transparencyPanel.transform.position = new Vector3(_guiPanels.transparencyPanel.transform.position.x, 0f, 0f);
            _guiPanels.FinancePanel.SetActive(false);
            _guiPanels.Buttonpanel.transform.GetChild(0).gameObject.SetActive(false);
            _guiPanels.Buttonpanel.transform.GetChild(3).gameObject.SetActive(true);
            ButtonClickScript.IsMouseoverUIControl = false;
            PointVisibilityScript.ReadOnly = true;
            Camera.main.GetComponent<NeedleScript>().enabled = false;
            _guiPanels.SessionEditorPanel.transform.GetChild(7).GetComponentInChildren<Text>().text = DateTimePickerScript.selecteddate.ToShortDateString();

        }
        catch (Exception exception)
        {
            LogScript.Log(exception);
        }
        yield return StartCoroutine(_pointVisibilityScript.WaitUntillNeedleLoaded());

    }
    public void ResetSessionEditor()
    {
        _guiPanels.SessionEditorPanel.transform.GetChild(2).GetComponent<InputField>().text = string.Empty;
        _guiPanels.SessionEditorPanel.transform.GetChild(7).GetComponentInChildren<Text>().text = DateTimePickerScript.selecteddate.ToShortDateString();
        _guiPanels.SessionEditorPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Session_Duration");
        _guiPanels.SessionEditorPanel.transform.GetChild(2).GetComponentInChildren<Text>().color = new Color(0, 0f, 0f, 128);
        for (int i = _guiPanels.SessionEditorPanel.transform.GetChild(3).GetChild(0).transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(_guiPanels.SessionEditorPanel.transform.GetChild(3).GetChild(0).GetChild(i).gameObject);
        }
        for (int j = 0; j < _guiPanels.Body.transform.childCount; j++)
        {
            GameObject needle = _guiPanels.Body.transform.GetChild(j).gameObject;
            if (needle.tag == CommonTypes.PrefabTag.needle.ToString())
            {
                Color r = PointVisibilityHelper.SetNeedleDefaultColor();
                needle.GetComponent<Renderer>().material.color = r;
            }
        }
        PointVisibilityScript.selectedNeedles.Clear();
        TreatmentLogButtonClick();
    }
    public void ShowHistoryButtonClick()
    {
        _guiPanels.Treatmentpanel.SetActive(false);
        _guiPanels.patientselectionpanel.SetActive(true);
        TranslationScript.TranslatePatientSelectionPanel();
        _guiPanels.patientselectionpanel.transform.GetChild(1).gameObject.SetActive(false);
        _guiPanels.SessionEditorPanel.SetActive(false);
        _guiPanels.FinancePanel.SetActive(false);
        _guiPanels.Buttonpanel.SetActive(true);
        TranslationScript.TranslateButtonpanel();
        _guiPanels.Mainmenupanel.SetActive(false);
        _guiPanels.Buttonpanel.transform.GetChild(0).gameObject.SetActive(false);
        _guiPanels.Buttonpanel.transform.GetChild(3).gameObject.SetActive(false);
        _guiPanels.sessionHistoryPanel.SetActive(true);
        TranslationScript.TranslatesessionHistoryPanel();
        _guiPanels.transparencyPanel.SetActive(true);
        TranslationScript.TranslatetransparencyPanel();
        _guiPanels.transparencyPanel.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 1f);
        _guiPanels.transparencyPanel.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 1f);
        _guiPanels.transparencyPanel.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1f);
        _guiPanels.transparencyPanel.GetComponent<RectTransform>().localPosition.Set(_guiPanels.transparencyPanel.transform.localPosition.x, 0f, 0f);
        //_guiPanels.sessionHistoryPanel.transform.localPosition = new Vector3(_guiPanels.Mainmenupanel.transform.localPosition.x + (_guiPanels.Mainmenupanel.GetComponent<RectTransform>().rect.width / 2 - _guiPanels.sessionHistoryPanel.GetComponent<RectTransform>().rect.width), _guiPanels.sessionHistoryPanel.transform.localPosition.y);
        _guiPanels.patientselectionpanel.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 1f);
        _guiPanels.patientselectionpanel.GetComponent<RectTransform>().anchorMin = new Vector2(1f, 1f);
        _guiPanels.patientselectionpanel.GetComponent<RectTransform>().pivot = new Vector2(1f, 1f);
        _guiPanels.patientselectionpanel.transform.localPosition = new Vector3(_guiPanels.sessionHistoryPanel.transform.localPosition.x, _guiPanels.Buttonpanel.transform.localPosition.y);
        PointVisibilityScript.ReadOnly = true;
        PointVisibilityScript.AllPatientsessions.Clear();
        ButtonClickScript.IsMouseoverUIControl = false;
        Camera.main.GetComponent<NeedleScript>().enabled = false;
        StartCoroutine("waitforneedlesandsessionlaoding");
    }
    IEnumerator waitforneedlesandsessionlaoding()
    {
        yield return StartCoroutine(_pointVisibilityScript.WaitUntillNeedleLoaded());
        ShowMessage(Language.Get("WORD_Loading") + "...");
        yield return StartCoroutine(_needleOperationScript.LoadAllSession());
        HideMessageDialogbox();
        _pointVisibilityScript.RefreshHistoryPanel();
    }
    public void AddprofileButtonClick()
    {
        _guiPanels.Patientpanel.gameObject.SetActive(false);
        _guiPanels.Treatmentpanel.gameObject.SetActive(false);
        _guiPanels.EditProfilePanel.gameObject.SetActive(false);
        _guiPanels.FinancePanel.SetActive(false);
        _guiPanels.SessionEditorPanel.SetActive(false);
        _guiPanels.Addprofilepanel.gameObject.SetActive(true);
        TranslationScript.TranslateAddprofilepanel();
        _pointVisibilityScript.resetaddpatientfield();
    }
    public void ChangeColor()
    {
        _guiPanels.ColorBox.SetActive(true);
        TranslationScript.TranslateColorBox();
    }
    public void MenuButtonClick()
    {
        if (InvisbleInputfieldScript.IsNameInserted)
        {
            _guiPanels.Body.GetComponent<Newline>().DeleteAllLine();
            PointVisibilityScript.ReadOnly = false;
            _guiPanels.patientselectionpanel.GetComponent<RectTransform>().anchorMax = new Vector2(0f, 1f);
            _guiPanels.patientselectionpanel.GetComponent<RectTransform>().anchorMin = new Vector2(0f, 1f);
            _guiPanels.patientselectionpanel.GetComponent<RectTransform>().pivot = new Vector2(0f, 1f);
            _guiPanels.patientselectionpanel.transform.localPosition = patientselectionpanelposition.transform.localPosition;
            Camera.main.GetComponent<NeedleScript>().enabled = true;
            TreatmentLogButtonClick();
            DestroyNeedleAndPoints();
            DestroyNameLabelforNeedle();
            _guiPanels.Buttonpanel.transform.GetChild(3).gameObject.SetActive(false);
            if (ButtonClickScript.isPatientSelected)
            {
                _guiPanels.Treatmentpanel.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = true;
            }
            Destroy(_guiPanels.Body.GetComponent<Newline>());
            _guiPanels.Body.gameObject.SetActive(false);
        }
    }

    public void DestroyNameLabelforNeedle()
    {
        for (int i = _guiPanels.Labelcanvas.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(_guiPanels.Labelcanvas.transform.GetChild(i).gameObject);
        }
    }

    private void DestroyNeedleAndPoints()
    {
        for (int i = _guiPanels.Body.transform.childCount - 1; i >= 0; i--)
        {
            if (_guiPanels.Body.transform.GetChild(i).tag == CommonTypes.PrefabTag.needle.ToString())
            {
                ShowNeedleNameScript label = _guiPanels.Body.transform.GetChild(i).gameObject.GetComponent<ShowNeedleNameScript>();
                if (label != null)
                {
                    Destroy(label.Label);
                    Destroy(label);
                }
            }
            if (_guiPanels.Body.transform.GetChild(i).tag == CommonTypes.PrefabTag.needle.ToString() || _guiPanels.Body.transform.GetChild(i).tag == CommonTypes.PrefabTag.point.ToString())
            {
                DestroyImmediate(_guiPanels.Body.transform.GetChild(i).gameObject);
            }
        }
    }

    public void menu()
    {
        ShowHome();
    }
    public void EditprofileButtonClick()
    {
        _guiPanels.EditProfilePanel.gameObject.SetActive(true);
        TranslationScript.TranslateEditProfilePanel();
        _guiPanels.Addprofilepanel.gameObject.SetActive(false);
        _pointVisibilityScript.reseteditpatientfield();
        _guiPanels.Patientpanel.gameObject.SetActive(false);
        _guiPanels.Treatmentpanel.gameObject.SetActive(false);
        _guiPanels.FinancePanel.SetActive(false);
        _guiPanels.SessionEditorPanel.SetActive(false);
    }
    public void HideMessageDialogbox()
    {
        _guiPanels.ErrorPanel.SetActive(false);
        JessicaRotationScript.isPaused = false;
    }

    public void ShowErrorPanel(string massege, string title)
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        JessicaRotationScript.isPaused = true;
        if (massege == "Could not resolve host: accupuntureservice.de.wvps46-163-110-202.dedicated.hosteurope.de; Host not found")
        {
            massege = "Could not Connect to Internet!!!\nPlease Retry...";
        }
        _guiPanels.ErrorPanel.gameObject.SetActive(true);
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(1).gameObject.SetActive(true);
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(2).gameObject.SetActive(true);
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = title;
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = massege;
        if (title != string.Empty)
        {
            NavigationScript.previousSelectedObejct = EventSystem.current.currentSelectedGameObject;
            EventSystem.current.SetSelectedGameObject(_guiPanels.ErrorPanel.transform.GetChild(1).GetChild(2).gameObject, new BaseEventData(EventSystem.current));
        }
    }
    public void ShowMessage(string message)
    {
        ShowErrorPanel(message, string.Empty);
        _guiPanels.ErrorPanel.transform.GetChild(0).localScale = new Vector2(.6f, .5f);
        _guiPanels.ErrorPanel.transform.GetChild(1).localScale = new Vector2(.6f, .5f);
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);
        _guiPanels.ErrorPanel.transform.GetChild(1).GetChild(2).gameObject.SetActive(false);
    }
    public void ShowFinancePanelButtonClick()
    {
        Color32 color = _guiPanels.Menupanel.transform.GetChild(2).GetComponent<Image>().color;
        _guiPanels.Menupanel.transform.GetChild(2).GetComponent<Image>().color = new Color32(color.r, color.g, color.b, 255);
        _guiPanels.Buttonpanel.SetActive(false);
        _guiPanels.Menupanel.SetActive(false);
        _guiPanels.FinancePanel.SetActive(true);
        TranslationScript.TranslateFinancePanel();
        _guiPanels.Patientpanel.gameObject.SetActive(false);
        _guiPanels.Addprofilepanel.gameObject.SetActive(false);
        _guiPanels.EditProfilePanel.SetActive(false);
        _guiPanels.instructionPanel.SetActive(false);
        _guiPanels.SessionEditorPanel.SetActive(false);
        _guiPanels.sessionHistoryPanel.SetActive(false);
        _guiPanels.MeridianPanel.SetActive(false);
        _guiPanels.ColorBox.SetActive(false);
        _guiPanels.Treatmentpanel.gameObject.SetActive(false);
        _guiPanels.Buttonpanel.SetActive(false);
        _guiPanels.transparencyPanel.SetActive(false);
        _guiPanels.patientselectionpanel.SetActive(false);
        SendButtonClick();
    }
    public void ShowAdminPanel()
    {
        _guiPanels.LoginPanel.SetActive(true);
        _guiPanels.SignUppanel.SetActive(false);
        _guiPanels.Userloginpanel.SetActive(false);
        _guiPanels.AppointmentPanel.SetActive(false);
        _guiPanels.AdminPanel.SetActive(true);
    }
    public void SendButtonClick()
    {
        _guiPanels.FinancePanel.transform.GetChild(2).gameObject.SetActive(true);
        _guiPanels.FinancePanel.transform.GetChild(3).gameObject.SetActive(false);
        TranslationScript.TranslateFinancePanel();
        _guiPanels.FinancePanel.transform.GetChild(0).GetComponent<Image>().color = GetActiveColor();
        _guiPanels.FinancePanel.transform.GetChild(1).GetComponent<Image>().color = GetInActiveColor();
    }
    /// <summary>
    /// Create the Active color for tab
    /// </summary>
    /// <returns>Color32</returns>
    static Color32 GetActiveColor()
    {
        byte Red = 255;
        byte Green = 255;
        byte Blue = 255;
        byte Alpha = 170;
        return new Color32(Red, Green, Blue, Alpha);
    }
    /// <summary>
    /// Create the Inactive color for tab
    /// </summary>
    /// <returns>Color32</returns>
    static Color32 GetInActiveColor()
    {
        byte Red = 255;
        byte Green = 255;
        byte Blue = 255;
        byte Alpha = 100;
        return new Color32(Red, Green, Blue, Alpha);
    }
    public void RecieveButtonClick()
    {
        _guiPanels.FinancePanel.transform.GetChild(3).gameObject.SetActive(true);
        _guiPanels.FinancePanel.transform.GetChild(2).gameObject.SetActive(false);
        TranslationScript.TranslateFinancePanel();
        _guiPanels.FinancePanel.transform.GetChild(0).GetComponent<Image>().color = GetInActiveColor();
        _guiPanels.FinancePanel.transform.GetChild(1).GetComponent<Image>().color = GetActiveColor();
    }
    public void ShowLoginPanel()
    {
        _guiPanels.LoginPanel.SetActive(true);
        _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(0).GetComponent<InputField>().Select();
        _guiPanels.SignUppanel.SetActive(false);
        _guiPanels.AdminPanel.SetActive(false);
        _guiPanels.Userloginpanel.SetActive(true);
        _guiPanels.AppointmentPanel.SetActive(false);
        TranslationScript.TranslateLoginPanel();
    }
    public void SignupCancel()
    {
        ShowAdminPanel();
        Color32 color = _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(3).GetComponent<Image>().color;
        _guiPanels.SignUppanel.transform.GetChild(0).GetChild(16).GetComponent<Image>().color = new Color32(color.r, color.g, color.b, 255);
        _guiPanels.AdminPanel.transform.GetChild(0).GetComponent<Image>().color = new Color32(color.r, color.g, color.b, 255);
    }
    public void ShowSignUpPanel()
    {
        _guiPanels.LoginPanel.SetActive(true);
        Color32 color = _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(3).GetComponent<Image>().color;
        _guiPanels.AdminPanel.transform.GetChild(0).GetComponent<Image>().color = new Color32(color.r, color.g, color.b, 255);
        _guiPanels.SignUppanel.transform.GetChild(0).GetChild(0).GetComponent<InputField>().Select();
        _guiPanels.Userloginpanel.SetActive(false);
        _guiPanels.SignUppanel.SetActive(true);
        _guiPanels.AdminPanel.SetActive(false);
        TranslationScript.TranslateLoginPanel();

    }
    public void ShowConfirmationPanel(string Message)
    {
        JessicaRotationScript.isPaused = true;
        _guiPanels.ConfirmationPanel.SetActive(true);
        _guiPanels.ConfirmationPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = Message;
    }
    public void HideConfirmationPanel()
    {
        JessicaRotationScript.isPaused = false;
        _guiPanels.ConfirmationPanel.SetActive(false);

    }
    public void ShowCreateAppointment()
    {
        _guiPanels.AppointmentPanel.SetActive(true);
        _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
        _guiPanels.LoginPanel.SetActive(false);
        Camera.main.GetComponent<AdminScript>().LoadAppointmentsOFdoctor();
        
    }
    #endregion
}
