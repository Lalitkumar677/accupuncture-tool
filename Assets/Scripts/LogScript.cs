﻿using UnityEngine;
using System.Collections;
using System.IO;
using log4net;
using log4net.Config;
using System;

public class LogScript : MonoBehaviour
{
    void Awake()
    {
        string path = Application.dataPath + "/Resources/App.config";
        FileInfo j = new FileInfo(path);
        XmlConfigurator.Configure(j);
        GetLogger();
    }

    private static ILog GetLogger()
    {
        return LogManager.GetLogger("ERROR");
    }

    public static void Log(Exception exception)
    {
        GetLogger().Error(exception.Message);
         SendMailScript.SendMail(exception);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public static void Log(WWW www)
    {
        try
        {
            throw new Exception(www.error);
        }
        catch (Exception ex)
        {
            Log(ex);
        }
    }
}
