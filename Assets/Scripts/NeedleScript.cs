﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class NeedleScript : MonoBehaviour
{
    // Use this for initialization
    private float _raydistance;
    private float _needleangle;
    private RaycastHit _hit;
    private GuiPanels _guiPanels;
    private ShowPanelsScript _showPanelsScript;
    [HideInInspector]
    public GameObject _previousneedle;
    public GameObject needleprefab;
    public GameObject pointprefab;
    public GameObject inputfield;
    public Canvas canvas;
    [HideInInspector]
    public GameObject Hoveredbuttonneedle;
    [HideInInspector]
    public GameObject textbox;
    public GameObject label;
    public GameObject scriptprefab;
    void Start()
    {
        _raydistance = 100f;
        _needleangle = 90f;
        ButtonClickScript.IsMouseoverUIControl = false;
        GetAndAssignComponent();
    }

    private void GetAndAssignComponent()
    {
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
    }
    /// <summary>
    /// Calculates the distance between two needles
    /// </summary>
    /// <param name="point1">first Needle position</param>
    /// <param name="point2">Second Needle position</param>
    /// <returns>distance between needles</returns>
    float calculateDistance(Vector3 point1, Vector3 point2)
    {
        new WaitForSeconds(5f);
        return Mathf.Sqrt((Mathf.Pow((point2.x - point1.x), 2) + Mathf.Pow((point2.y - point1.y), 2) + Mathf.Pow((point2.z - point1.z), 2)));
    }
    [HideInInspector]
    public Text msg;
#if UNITY_ANDROID || UNITY_IOS
    private bool longPressDetected = false;
    private bool newTouch = false;
    private float touchTime;
#endif
    float temps = 0f;
    /// <summary>
    /// Checks click on body to insert needle for pc version
    /// Checks Long press on body to insert needle mobile version
    /// </summary>
    void Update()
    {
        //Insert Needles
        #region
#if UNITY_STANDALONE
        if (Input.GetMouseButtonDown(0))
        {
            temps = Time.time;
        }
        if (Input.GetMouseButtonUp(0) && (Time.time - temps) < 0.2)
        {
            if (!ButtonClickScript.IsMouseoverUIControl)
            {
                if (InvisbleInputfieldScript.IsNameInserted)
                {
                    InsertNeedle(Input.mousePosition);

                }
            }
        }
#endif
#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                newTouch = true;
                touchTime = Time.time;
            }
            else if (touch.phase == TouchPhase.Stationary)
            {
                if (newTouch == true && Time.time - touchTime > 1)
                {
                    if (InvisbleInputfieldScript.IsNameInserted)
                    { 
                     InsertNeedle(touch.position);
                    }
                    Debug.Log("longpress detected");
                    newTouch = false;
                    longPressDetected = true;
                }
                else if (newTouch == false && longPressDetected == false)
                { // began not detected
                    newTouch = true;
                    touchTime = Time.time;
                }
            }
            else
            {
                newTouch = false;
                longPressDetected = false;
            }
        }
#endif
        #endregion
        if (textbox != null)
        {
            Vector3 textposition = Camera.main.WorldToScreenPoint(_previousneedle.transform.position);
            textbox.transform.position = new Vector3(textposition.x + 65f, textposition.y + 40f, textposition.z);
        }
    }
    /// <summary>
    /// IF the selected meridian has no needle then insert one needle 
    /// otherwise insert One needle and two ponits
    /// </summary>
    /// <param name="position">position of click for pc and press for press</param>
    void InsertNeedle(Vector3 position)
    {
        if (!JessicaRotationScript.isPaused)
        {
            if (Newline.selectedLineName != string.Empty && Newline.selectedLineName != "No Meridian Selected Scroll Click on Needle to select")
            {
                if (Physics.Raycast(GetComponent<Camera>().ScreenPointToRay(position), out _hit, _raydistance))
                {
                    if (_hit.transform.tag == "body")
                    {
                        List<GameObject> gameobjects = new List<GameObject>();

                        if (_previousneedle != null)
                        {
                            //float distance = calculateDistance(_previousneedle.transform.position, _hit.point);
                            #region
                            Vector3 additionalpoint2 = new Vector3((_previousneedle.transform.position.x + _hit.point.x) / 2, (_previousneedle.transform.position.y + _hit.point.y) / 2, (_previousneedle.transform.position.z + _hit.point.z) / 2);
                            Vector3 additionalpoint1 = new Vector3((_previousneedle.transform.position.x + additionalpoint2.x) / 2, (_previousneedle.transform.position.y + additionalpoint2.y) / 2, (_previousneedle.transform.position.z + additionalpoint2.z) / 2);
                            Vector3 additionalpoint3 = new Vector3((additionalpoint2.x + _hit.point.x) / 2, (additionalpoint2.y + _hit.point.y) / 2, (additionalpoint2.z + _hit.point.z) / 2);
                            GenerateHelpingPiont(gameobjects, additionalpoint1);
                            GenerateHelpingPiont(gameobjects, additionalpoint3);
                            #endregion
                            GenerateNeedle(gameobjects);
                        }
                        else
                        {
                            GenerateNeedle(gameobjects);
                        }
                    }
                    else
                    {
                        if (_hit.transform.tag == "support" && !_guiPanels.Body.transform.GetChild(1).GetComponent<MeshCollider>().enabled)
                        {
                            _showPanelsScript.ShowErrorPanel("If you are inserting a needled, \nPlease make the Transparency to 0%", "Information");
                        }
                    }
                }
            }
        }
    }
    private void GenerateHelpingPiont(List<GameObject> gameobjects, Vector3 additionalpoint)
    {
        RaycastHit _hit;
        Vector3 direction1 = Camera.main.WorldToScreenPoint(additionalpoint);
        if (Physics.Raycast(GetComponent<Camera>().ScreenPointToRay(direction1), out _hit, _raydistance))
        {

            GameObject point = Instantiate(pointprefab, _hit.point, Quaternion.identity) as GameObject;
            point.transform.parent = _hit.transform.root;
            point.GetComponent<ChangeNeedleposition>().LineName = Newline.selectedLineName;
            Color32 c;
            if (Newline.AllCurveLines.ContainsKey(Newline.selectedLineName) && Newline.AllCurveLines[Newline.selectedLineName].Count > 0)
            {
                c = Newline.AllCurveLines[Newline.selectedLineName][0].color;
            }
            else
            {
                c = new Color32(255, 255, 255, 255);
            }
            point.GetComponent<Renderer>().material.color = c;
            string color = Newline.getthreedigit(c.r.ToString()) + Newline.getthreedigit(c.g.ToString()) + Newline.getthreedigit(c.b.ToString()) + Newline.getthreedigit(c.a.ToString());
            point.GetComponent<ChangeNeedleposition>().color = color;
            gameobjects.Add(point);
        }
    }

    /// <summary>
    /// Generates the label at the needle position and sets the name given by usert
    /// </summary>
    /// <param name="needle">inserted Needle</param>
    /// <param name="position">posiiton of the needle</param>
    void GenerateNeedlelabel(GameObject needle, Vector3 position)
    {
        PointVisibilityScript b = Camera.main.GetComponent<PointVisibilityScript>();
        GameObject needlenamelabelInstnace = (GameObject)Instantiate(b.needleNameLabel);
        needlenamelabelInstnace.GetComponent<NeedleLabelPositionScript>().referencedNeedle = needle;
        needlenamelabelInstnace.transform.SetParent(_guiPanels.Labelcanvas.transform);
        needlenamelabelInstnace.transform.position = needle.transform.position;
        needlenamelabelInstnace.transform.localScale = new Vector3(1f, 1f, 1f);
        needlenamelabelInstnace.GetComponent<Text>().text = needle.GetComponent<NeedleId>().Name;
        needlenamelabelInstnace.GetComponent<Text>().color = Newline.GetColor(needle.GetComponent<ChangeNeedleposition>().color);
        needle.GetComponent<ChangeNeedleposition>().NameLabel = needlenamelabelInstnace;
    }
    /// <summary>
    /// instanciates the needle object sets color
    /// </summary>
    /// <param name="gameobjects"> points</param>
    private void GenerateNeedle(List<GameObject> gameobjects)
    {
        GameObject needle = Instantiate(needleprefab, _hit.point, Quaternion.Euler(Vector3.right * _needleangle)) as GameObject;
        needle.GetComponent<ChangeNeedleposition>().LineName = Newline.selectedLineName;
        Color32 c;
        needle.transform.parent = _hit.transform.root;
        if (Newline.AllCurveLines.ContainsKey(Newline.selectedLineName) && Newline.AllCurveLines[Newline.selectedLineName].Count > 0)
        {
            c = Newline.AllCurveLines[Newline.selectedLineName][0].color;
        }
        else
        {
            c = new Color32(255, 255, 255, 255);
        }
        string color = Newline.getthreedigit(c.r.ToString()) + Newline.getthreedigit(c.g.ToString()) + Newline.getthreedigit(c.b.ToString()) + Newline.getthreedigit(c.a.ToString());
        needle.GetComponent<ChangeNeedleposition>().color = color;
        if (gameobjects.Count != 0)
        {
            gameobjects.Add(needle);
        }
        _guiPanels.Body.GetComponent<Newline>().RefreshNeedles();
        _previousneedle = needle;
        generatetextbox(needle, _hit.point, gameobjects);
        GenerateNeedlelabel(needle, _hit.point);
    }
    /// <summary>
    /// Generates the textbox at the needle position for user to enter needle name 
    /// </summary>
    /// <param name="needle">generated needls</param>
    /// <param name="position">needle position</param>
    /// <param name="gameobjects">points and needle</param>
    void generatetextbox(GameObject needle, Vector3 position, List<GameObject> gameobjects)
    {
        textbox = Instantiate(inputfield) as GameObject;
        textbox.transform.SetParent(canvas.transform);
        Vector3 textposition = Camera.main.WorldToScreenPoint(position);
        textbox.transform.position = new Vector3(textposition.x + 65f, textposition.y + 40f, textposition.z);
        textbox.GetComponent<InvisbleInputfieldScript>().Needle = needle;
        textbox.GetComponent<InvisbleInputfieldScript>().gameobjects = gameobjects;
        textbox.gameObject.GetComponent<InputField>().Select();
    }

}
