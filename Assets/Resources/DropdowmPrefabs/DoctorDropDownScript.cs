﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class DoctorDropDownScript : MonoBehaviour
{
    public List<Doctor> DropDownItem = new List<Doctor>();
    List<Doctor> FilteredDropDownItem = new List<Doctor>();
    public GameObject ItemListPanel;
    public GameObject ContentPanel;
    public GameObject scrollbar;
    public GameObject patientDetailPanel;
    GameObject Item;
    void Start()
    {
        Item = Resources.Load("DropdowmPrefabs/Panel") as GameObject;
        ItemListPanel.SetActive(false);
    }
    public void ControlClick()
    {
        selectedindex = -1;
        transform.GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text = string.Empty;
        for (int i = ContentPanel.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(ContentPanel.transform.GetChild(i).gameObject);
        }
        NeedleOperationScript _needleOperationScript = Camera.main.GetComponent<NeedleOperationScript>();
        int CompanyID = Camera.main.GetComponent<UserIdScript>().doctor.CompanyID;
        _needleOperationScript.LoadAllDoctorsForAdmin(CompanyID);
        bool state = !ItemListPanel.activeInHierarchy;
        ItemListPanel.SetActive(state);
        if (state)
        {
            for (int i = 0; i < DropDownItem.Count; i++)
            {
                GeneratePanel1(Getpatientfullname(i));
            }
            if (DropDownItem.Count == 0)
            {
                GeneratePanel1(Language.Get("No_Item"));
            }
        }
        if (DropDownItem.Count < 5)
        {
            scrollbar.SetActive(false);
        }
        else
        {
            scrollbar.SetActive(true);
        }
    }

    public string Getpatientfullname(int i)
    {
        if (DropDownItem.Count > i && i >= 0)
        {
            return DropDownItem[i].DoctorName;
        }
        else
        {
            return string.Empty;
        }
    }
    public string Getpatientname(int i)
    {
        if (DropDownItem.Count > i && i >= 0)
        {
            return DropDownItem[i].DoctorName;
        }
        else
        {
            return string.Empty;
        }
    }
    public void filter()
    {
        selectedindex = -1;
        FilteredDropDownItem.Clear();
        for (int i = ContentPanel.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(ContentPanel.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < DropDownItem.Count; i++)
        {
            string text = transform.GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text;
            if (Getpatientfullname(i).ToLower().Contains(text.ToLower()))
            {
                GeneratePanel1(Getpatientfullname(i));
                FilteredDropDownItem.Add(DropDownItem[i]);
            }
        }
        if (FilteredDropDownItem.Count == 0)
        {
            GeneratePanel1(Language.Get("No_Item"));
        }
        if (FilteredDropDownItem.Count < 5)
        {
            scrollbar.SetActive(false);
        }
    }
    private void GeneratePanel1(string text)
    {
        Item = Resources.Load("DropdowmPrefabs/Panel") as GameObject;
        GameObject instancedItem = (GameObject)Instantiate(Item);
        instancedItem.transform.SetParent(ContentPanel.gameObject.transform);
        instancedItem.transform.localScale = new Vector3(1f, 1f, 1f);
        instancedItem.transform.GetChild(0).GetComponent<Text>().text = text;
    }
    int selectedindex = -1;
    void Update()
    {
        if (ItemListPanel.activeInHierarchy)
        {
            DowmArrowPressed();
            UpArrowPressed();
            if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
            {
                GameObject.Find("Scriptprefab").GetComponent<ButtonClickScript>().SelectItem(ContentPanel.transform.GetChild(selectedindex).gameObject);
            }
        }
    }

    private void UpArrowPressed()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            List<Doctor> contentItem = new List<Doctor>();
            if (transform.GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text == string.Empty)
            {
                contentItem = DropDownItem;
            }
            else
            {
                contentItem = FilteredDropDownItem;
            }
            if (selectedindex <= contentItem.Count - 1 && selectedindex >= 0)
            {
                ContentPanel.transform.GetChild(selectedindex).GetComponent<Image>().color = new Color32(255, 255, 255, 100);
            }
            if (selectedindex > 0)
            {
                selectedindex--;
            }
            if (selectedindex <= ContentPanel.transform.childCount - 1 && selectedindex >= 0)
            {
                ContentPanel.transform.GetChild(selectedindex).GetComponent<Image>().color = new Color32(0, 0, 255, 100);
            }
        }
    }

    private void DowmArrowPressed()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (selectedindex >= 0)
            {
                ContentPanel.transform.GetChild(selectedindex).GetComponent<Image>().color = new Color32(255, 255, 255, 100);
            }
            List<Doctor> contentItem = new List<Doctor>();
            if (transform.GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text == string.Empty)
            {
                contentItem = DropDownItem;
            }
            else
            {
                contentItem = FilteredDropDownItem;
            }
            if (selectedindex < contentItem.Count - 1)
            {
                selectedindex++;
            }
            ContentPanel.transform.GetChild(selectedindex).GetComponent<Image>().color = new Color32(0, 0, 255, 100);
        }
    }
}