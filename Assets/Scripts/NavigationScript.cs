﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NavigationScript : MonoBehaviour
{

    EventSystem system;
    GuiPanels _guiPanels;
    public static GameObject previousSelectedObejct;
    void Start()
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        system = EventSystem.current;// EventSystemManager.currentSystem;
    }
    void Update()
    {
        if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyDown(KeyCode.Tab))
        {
            Backward();
        }
        else if (Input.GetKeyDown(KeyCode.Tab))
        {
            Forward();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            NavigationHepler.HitEnterOnControl(system);
        }
    }
    private void Forward()
    {
        if (system.currentSelectedGameObject != null)
        {
            if (_guiPanels.Userloginpanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.Userloginpanel.transform.GetChild(0).gameObject;
                TabNavigation(parent, CommonTypes.Action.Forward);
            }
            if (_guiPanels.SignUppanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.SignUppanel.transform.GetChild(0).gameObject;
                TabNavigation(parent, CommonTypes.Action.Forward);
            }
            if (_guiPanels.Menupanel.activeInHierarchy && !_guiPanels.Addprofilepanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.Menupanel.transform.gameObject;
                TabNavigation(parent, CommonTypes.Action.Forward);
            }
            if (_guiPanels.Addprofilepanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.AddprofileContentpanel.transform.gameObject;
                TabNavigation(parent, CommonTypes.Action.Forward);
            }
            if (_guiPanels.EditProfilePanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.EditprofileContentpanel.transform.gameObject;
                TabNavigation(parent, CommonTypes.Action.Forward);
            }
        }
    }

    private void Backward()
    {
        if (system.currentSelectedGameObject != null)
        {
            if (_guiPanels.Userloginpanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.Userloginpanel.transform.GetChild(0).gameObject;
                TabNavigation(parent, CommonTypes.Action.Backward);
            }
            if (_guiPanels.SignUppanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.SignUppanel.transform.GetChild(0).gameObject;
                TabNavigation(parent, CommonTypes.Action.Backward);
            }
            if (_guiPanels.Menupanel.activeInHierarchy && !_guiPanels.Addprofilepanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.Menupanel.transform.gameObject;
                TabNavigation(parent, CommonTypes.Action.Backward);
            }
            if (_guiPanels.Addprofilepanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.AddprofileContentpanel.transform.gameObject;
                TabNavigation(parent, CommonTypes.Action.Backward);
            }
            if (_guiPanels.EditProfilePanel.activeInHierarchy)
            {
                GameObject parent = _guiPanels.EditprofileContentpanel.transform.gameObject;
                TabNavigation(parent, CommonTypes.Action.Backward);
            }
        }
    }
    private void TabNavigation(GameObject parent, CommonTypes.Action action)
    {
        bool found = false;
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            GameObject child = parent.transform.GetChild(i).gameObject;
            if (child == system.currentSelectedGameObject)
            {
                NavigationHepler.ChangeUIColorTODefaultAndSelectControl(parent, i, child, action, system);
                NavigationHepler.ChangeColorOFNewSelectedControl(system, action);
                found = true;
                break;
            }
        }
        if (!found)
        {
            if (system.currentSelectedGameObject.transform.parent.gameObject.tag == CommonTypes.UIControls.UIGenderPanel.ToString())
            {
                system.SetSelectedGameObject(system.currentSelectedGameObject.transform.parent.gameObject, new BaseEventData(EventSystem.current));
                TabNavigation(parent, action);
            }
        }
    }


}


