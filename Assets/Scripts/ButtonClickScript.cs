﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ButtonClickScript : MonoBehaviour
{
    private GameObject textbox;
    private GuiPanels _guiPanels;
    private ShowPanelsScript _showPanelsScript;
    private PointVisibilityScript _pointVisibilityScript;
    //    private static readonly JCsLogger log = new JCsLogger(typeof(ButtonClickScript));
    public Material selected;
    public Material Nonselected;
    public GameObject inputfield;
    public Canvas canvas;
    public GameObject Hoveredbuttonneedle;
    public static bool IsMouseoverUIControl = false;
    public static bool isSliderValueChanging = false;
    public static bool isPatientSelected = false;
    // Use this for initialization
    void Start()
    {
        GetAndAssignComponent();
    }

    private void GetAndAssignComponent()
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        _pointVisibilityScript = Camera.main.GetComponent<PointVisibilityScript>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void ChangingValue()
    {
        isSliderValueChanging = true;
    }
    public void NotChangingValue()
    {
        isSliderValueChanging = false;
    }

    public void ChangingValue(UnityEngine.Object label)
    {
        DisplayTransparencyOnlabel(label, true);
    }

    private static void DisplayTransparencyOnlabel(UnityEngine.Object label, bool isvaluechanging)
    {
        isSliderValueChanging = isvaluechanging;
        GameObject transparencylabel = (GameObject)label;
        transparencylabel.SetActive(isvaluechanging);
    }

    public void NotChangingValue(UnityEngine.Object label)
    {
        DisplayTransparencyOnlabel(label, false);
    }
    public void SelectItem(UnityEngine.Object row)
    {
        GetAndAssignComponent();
        try
        {
            GameObject panel = (GameObject)row;
            string selectedText = panel.transform.GetComponentInChildren<Text>().text.ToLower();
            if (!_guiPanels.AdminPanel.activeInHierarchy)
            {
                DropDownScript DropDown = panel.transform.parent.parent.parent.GetComponent<DropDownScript>();
                AssignPatientselectionPanel(selectedText, DropDown);
                isPatientSelected = true;
                _pointVisibilityScript.RefreshHistoryPanel();
            }
            else
            {
                DoctorDropDownScript DropDown = panel.transform.parent.parent.parent.GetComponent<DoctorDropDownScript>();
                for (int i = 0; i < DropDown.DropDownItem.Count; i++)
                {
                    if (DropDown.Getpatientfullname(i).ToLower() == selectedText)
                    {
                        GetAndAssignComponent();
                        Camera.main.GetComponent<UserIdScript>().DoctorID = DropDown.DropDownItem[i].DoctorID;
                        Camera.main.GetComponent<UserIdScript>().doctor = DropDown.DropDownItem[i];
                        _guiPanels.AdminPanel.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text = DropDown.Getpatientfullname(i);
                        _guiPanels.AdminPanel.transform.GetChild(2).GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = DropDown.Getpatientfullname(i);
                        _guiPanels.AdminPanel.transform.GetChild(2).GetChild(1).gameObject.SetActive(false);
                        _guiPanels.AdminPanel.transform.GetChild(1).GetComponent<Button>().interactable = true;
                        _guiPanels.AdminPanel.transform.GetChild(5).GetComponent<Button>().interactable = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogScript.Log(ex);
        }
    }
    private void AssignPatientselectionPanel(string selectedText, DropDownScript DropDown)
    {
        for (int i = 0; i < DropDown.DropDownItem.Count; i++)
        {
            if (DropDown.Getpatientfullname(i).ToLower() == selectedText)
            {
                GuiPanels.guiPanels.Treatmentpanel.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = true;
                PointVisibilityScript.selectedpatient = DropDown.DropDownItem[i];
                if (_guiPanels.patientselectionpanel.activeInHierarchy)
                {
                    AssignValuesToPatientSelectionPanel(DropDown, i);
                    break;
                }
                else if (GuiPanels.guiPanels.EditProfilePanel.activeInHierarchy)
                {
                    PointVisibilityScript.EditProfileselectedpatient = DropDown.DropDownItem[i];
                    GuiPanels.guiPanels.EditProfilePanel.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text = DropDown.Getpatientfullname(i);
                    GuiPanels.guiPanels.EditProfilePanel.transform.GetChild(2).GetChild(1).gameObject.SetActive(false);
                    SetDropDownValueEmptyOnNull(DropDown, i);
                    AssignValueToEditProfilePanel(DropDown, i);
                }
            }
        }
    }

    private static void AssignValueToEditProfilePanel(DropDownScript DropDown, int i)
    {
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(1).GetComponent<InputField>().text = DropDown.DropDownItem[i].Title;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(1).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Title;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(3).GetComponent<InputField>().text = DropDown.DropDownItem[i].FName;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(3).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].FName;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(5).GetComponent<InputField>().text = DropDown.DropDownItem[i].LName;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(5).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].LName;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(7).GetComponent<InputField>().text = DropDown.DropDownItem[i].Pwork;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(7).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Pwork;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(9).GetComponent<InputField>().text = DropDown.DropDownItem[i].Pmobile;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(9).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Pmobile;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(11).GetComponent<InputField>().text = DropDown.DropDownItem[i].Email;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(11).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Email;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(15).GetComponent<InputField>().text = DropDown.DropDownItem[i].Houseno;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(15).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Houseno;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(17).GetComponent<InputField>().text = DropDown.DropDownItem[i].Locality;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(17).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Locality;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(19).GetComponent<InputField>().text = DropDown.DropDownItem[i].Town;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(19).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Town;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(21).GetComponent<InputField>().text = DropDown.DropDownItem[i].Pincode;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(21).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].Pincode;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(23).GetComponent<InputField>().text = DropDown.DropDownItem[i].state;
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(23).GetComponentInChildren<Text>().text = DropDown.DropDownItem[i].state;
        int index = (DropDown.DropDownItem[i].Gender == CommonTypes.Gender.Male.ToString()) ? 0 : 1;
        selectGenderRadioButton(index);
    }

    private static void selectGenderRadioButton(int index)
    {
        GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(13).GetChild(index).GetComponent<Toggle>().isOn = true;
    }

    private static void SetDropDownValueEmptyOnNull(DropDownScript DropDown, int i)
    {
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Title))
        {
            DropDown.DropDownItem[i].Title = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].FName))
        {
            DropDown.DropDownItem[i].FName = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].LName))
        {
            DropDown.DropDownItem[i].LName = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Email))
        {
            DropDown.DropDownItem[i].Email = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Pmobile))
        {
            DropDown.DropDownItem[i].Pmobile = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Pwork))
        {
            DropDown.DropDownItem[i].Pwork = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Houseno))
        {
            DropDown.DropDownItem[i].Houseno = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Locality))
        {
            DropDown.DropDownItem[i].Locality = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Town))
        {
            DropDown.DropDownItem[i].Town = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].Pincode))
        {
            DropDown.DropDownItem[i].Pincode = string.Empty;
        }
        if (string.IsNullOrEmpty(DropDown.DropDownItem[i].state))
        {
            DropDown.DropDownItem[i].state = string.Empty;
        }
    }

    private void AssignValuesToPatientSelectionPanel(DropDownScript DropDown, int i)
    {
        GetAndAssignComponent();
        _guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(1).gameObject.SetActive(false);
        _guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text = DropDown.Getpatientfullname(i);
        _guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = DropDown.Getpatientfullname(i);
        _guiPanels.patientDetailPanel.transform.GetChild(6).GetComponent<Text>().text = DropDown.DropDownItem[i].Title;
        _guiPanels.patientDetailPanel.transform.GetChild(7).GetComponent<Text>().text = DropDown.DropDownItem[i].FName;
        _guiPanels.patientDetailPanel.transform.GetChild(8).GetComponent<Text>().text = DropDown.DropDownItem[i].LName;
        _guiPanels.patientDetailPanel.transform.GetChild(9).GetComponent<Text>().text = DropDown.DropDownItem[i].Email;
        _guiPanels.patientDetailPanel.transform.GetChild(10).GetComponent<Text>().text = DropDown.DropDownItem[i].Pwork;
        _guiPanels.patientDetailPanel.transform.GetChild(11).GetComponent<Text>().text = DropDown.DropDownItem[i].Pmobile;
        _guiPanels.patientDetailPanel.transform.GetChild(12).GetComponent<Text>().text = DropDown.DropDownItem[i].Gender;
#if UNITY_ANDROID || UNITY_IOS
       _guiPanels.patientDetailPanel.transform.GetChild(15).GetComponent<Text>().text = "House No. " + DropDown.DropDownItem[i].Houseno + "\n" + DropDown.DropDownItem[i].Locality + "\n" + DropDown.DropDownItem[i].Town + " " + DropDown.DropDownItem[i].Pincode + "\n"+ DropDown.DropDownItem[i].state;
#endif

#if UNITY_STANDALONE
        _guiPanels.patientDetailPanel.transform.GetChild(15).GetComponent<Text>().text = "House No. " + DropDown.DropDownItem[i].Houseno + "\n" + DropDown.DropDownItem[i].Locality + "\n" + DropDown.DropDownItem[i].Town + " " + DropDown.DropDownItem[i].Pincode + ", " + DropDown.DropDownItem[i].state;
#endif
    }

    public void selectedpanelcolor(UnityEngine.Object row)
    {
        AssignColor(row, selected.color);
    }
    public void Nonselectedpanelcolor(UnityEngine.Object row)
    {
        AssignColor(row, Nonselected.color);
    }
    private void AssignColor(UnityEngine.Object row, Color color)
    {
        GameObject panel = (GameObject)row;
        panel.GetComponent<Image>().color = color;
    }
    public void generatetextboxbybutton()
    {
        NeedleScript needlescript = Camera.main.GetComponent<NeedleScript>();
        canvas = needlescript.canvas;
        inputfield = needlescript.inputfield;
        Hoveredbuttonneedle = needlescript.Hoveredbuttonneedle;
        ShowNeedleNameScript label = Hoveredbuttonneedle.GetComponent<ShowNeedleNameScript>();
        Destroy(label.Label);
        Destroy(label);
        textbox = Instantiate(inputfield) as GameObject;
        textbox.transform.SetParent(canvas.transform);
        textbox.GetComponentInChildren<Text>().text = Hoveredbuttonneedle.GetComponent<NeedleId>().Name;
        Vector3 textposition = Camera.main.WorldToScreenPoint(Hoveredbuttonneedle.transform.position);
        textbox.transform.position = new Vector3(textposition.x + 65f, textposition.y + 40f, textposition.z);
        textbox.GetComponent<InvisbleInputfieldScript>().Needle = Hoveredbuttonneedle;
        textbox.GetComponent<InvisbleInputfieldScript>().shouldupdate = true;
        textbox.gameObject.GetComponent<InputField>().Select();
    }

    public void Mousehover()
    {
        IsMouseoverUIControl = true;
    }

    public void Mousehoverexit()
    {
        IsMouseoverUIControl = false;
    }

    public void OnMouseEnter()
    {
        IsMouseoverUIControl = true;
    }

    public void OnMouseExit()
    {
        IsMouseoverUIControl = false;
    }
    public void LoadScene(GameObject button)
    {
        GameObject parent = button.transform.parent.gameObject;
        string pid = parent.transform.GetChild(0).gameObject.GetComponent<Text>().text;
        int id;
        bool numberconverted = int.TryParse(pid, out id);
        if (numberconverted)
        {
            NeedleOperationScript _needleoperationscript = Camera.main.GetComponent<NeedleOperationScript>();
            _needleoperationscript = Camera.main.GetComponent<NeedleOperationScript>();
            _needleoperationscript.LoadNeedles();
        }
    }

    public void SendBillButtonClick(UnityEngine.Object row)
    {
        GameObject.Find("Scriptprefab").GetComponent<ButtonClickScript>().StartCoroutine("SendBill", row);
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.2f);
        ButtonClickScript.IsMouseoverUIControl = false;
    }
    IEnumerator SendBill(GameObject paymentrow)
    {
        string invoice = paymentrow.transform.GetChild(5).GetComponent<Text>().text;
        string Name = paymentrow.transform.GetChild(0).GetComponent<Text>().text;
        string Sessiondate = paymentrow.transform.GetChild(1).GetComponent<Text>().text;
        if (!paymentrow.transform.GetChild(3).GetComponent<Button>().IsInteractable())
        {
            yield return StartCoroutine("ConfirmAndUnmarkSentBill", new object[] { paymentrow, Name, invoice, Sessiondate });
        }
        else
        {
            yield return StartCoroutine("ConfirmandSendBill", new object[] { paymentrow, Name, invoice, Sessiondate });
        }
    }
    IEnumerator ConfirmAndUnmarkSentBill(object[] para)
    {
        GetAndAssignComponent();
        GameObject paymentrow = (GameObject)para[0];
        string Name = para[1].ToString();
        string invoice = para[2].ToString();
        string Sessiondate = para[3].ToString();
        _showPanelsScript.ShowConfirmationPanel("Do you confirm that the bill No " + invoice + " for " + Name + " for the treatment date of " + Sessiondate + "that has been marked as sent should be unmarked?");
        while (_guiPanels.ConfirmationPanel.activeInHierarchy)
        {
            yield return null;
        }
        if (JessicaRotationScript.ConfirmationYes)
        {
            paymentrow.transform.GetChild(3).GetComponent<Button>().interactable = true;
            paymentrow.transform.GetChild(4).GetComponent<Image>().sprite = Camera.main.GetComponent<PaymentDetailScript>().red;
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).Sent = 0;
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).BillsendDate = string.Empty;
            paymentrow.transform.GetChild(4).GetChild(1).GetComponent<Text>().text = string.Empty;
            yield return Camera.main.GetComponent<PaymentDetailScript>().StartCoroutine("UpdatePayment", PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice));
        }
    }
    IEnumerator ConfirmAndUnmarkPayment(object[] para)
    {
        ShowPanelsScript _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        GameObject paymentrow = (GameObject)para[0];
        string Name = para[1].ToString();
        string invoice = para[2].ToString();
        string Sessiondate = para[3].ToString();
        _showPanelsScript.ShowConfirmationPanel("Do you confirm that the Payment for bill No " + invoice + " for " + Name + " for the treatment date of " + Sessiondate + "that has been marked as Unpaid?");
        while (_guiPanels.ConfirmationPanel.activeInHierarchy)
        {
            yield return null;
        }
        if (JessicaRotationScript.ConfirmationYes)
        {
            paymentrow.transform.GetChild(3).GetComponent<Button>().interactable = true;
            paymentrow.transform.GetChild(6).GetComponent<Image>().sprite = Camera.main.GetComponent<PaymentDetailScript>().red;
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).Paid = 0;
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).PaymentDate = string.Empty;
            paymentrow.transform.GetChild(6).GetChild(1).GetComponent<Text>().text = string.Empty;
            yield return Camera.main.GetComponent<PaymentDetailScript>().StartCoroutine("UpdatePayment", PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice));
        }
    }
    IEnumerator ConfirmandSendBill(object[] para)
    {
        GetAndAssignComponent();
        GameObject paymentrow = (GameObject)para[0];
        string Name = para[1].ToString();
        string invoice = para[2].ToString();
        string Sessiondate = para[3].ToString();
        _showPanelsScript.ShowConfirmationPanel("Do you confirm that the bill No " + invoice + " for " + Name + " for the treatment date of " + Sessiondate + " has been sent?");
        while (_guiPanels.ConfirmationPanel.activeInHierarchy)
        {
            yield return null;
        }
        if (JessicaRotationScript.ConfirmationYes)
        {
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).Sent = 1;
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).BillsendDate = DateTime.Now.Date.ToString("MM-dd-yyyy") + "\n" + DateTime.Now.ToString("HH:mm:ss");
            yield return Camera.main.GetComponent<PaymentDetailScript>().StartCoroutine("UpdatePayment", PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice));
            if (!_guiPanels.FinancePanel.transform.GetChild(2).GetChild(4).GetComponent<Toggle>().isOn)
            {
                DestroyImmediate(paymentrow.gameObject);
            }
            else
            {
                paymentrow.transform.GetChild(3).GetComponent<Button>().interactable = false;
                paymentrow.transform.GetChild(4).GetComponent<Image>().sprite = Camera.main.GetComponent<PaymentDetailScript>().green;
                paymentrow.transform.GetChild(4).GetChild(1).GetComponent<Text>().text = DateTime.Now.Date.ToString("MM-dd-yyyy") + "\n" + DateTime.Now.ToString("HH:mm:ss");
            }
        }
    }
    IEnumerator ConfirmAndMakePayment(object[] para)
    {
        GetAndAssignComponent();
        GameObject paymentrow = (GameObject)para[0];
        string Name = para[1].ToString();
        string invoice = para[2].ToString();
        string Sessiondate = para[3].ToString();
        _showPanelsScript.ShowConfirmationPanel("Do you confirm that Payment for bill No " + invoice + " for " + Name + " for the treatment date of " + Sessiondate + " has been Paid?");
        while (_guiPanels.ConfirmationPanel.activeInHierarchy)
        {
            yield return null;
        }
        if (JessicaRotationScript.ConfirmationYes)
        {
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).Paid = 1;
            PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice).PaymentDate = DateTime.Now.Date.ToString("MM-dd-yyyy") + "\n" + DateTime.Now.ToString("HH:mm:ss");
            yield return Camera.main.GetComponent<PaymentDetailScript>().StartCoroutine("UpdatePayment", PaymentDetailScript.paymentsRecords.Find(p => p.PaymentId.ToString() == invoice));
            if (!_guiPanels.FinancePanel.transform.GetChild(3).GetChild(4).GetComponent<Toggle>().isOn)
            {
                DestroyImmediate(paymentrow.gameObject);
            }
            else
            {
                paymentrow.transform.GetChild(3).GetComponent<Button>().interactable = false;
                paymentrow.transform.GetChild(6).GetComponent<Image>().sprite = Camera.main.GetComponent<PaymentDetailScript>().green;
                paymentrow.transform.GetChild(6).GetChild(1).GetComponent<Text>().text = DateTime.Now.Date.ToString("MM-dd-yyyy") + "\n" + DateTime.Now.ToString("HH:mm:ss");
            }
        }
    }
    public void MakePaymentButtonClick(UnityEngine.Object row)
    {
        GameObject.Find("Scriptprefab").GetComponent<ButtonClickScript>().StartCoroutine("MakePayment", row);
    }
    IEnumerator MakePayment(GameObject paymentrow)
    {
        string invoice = paymentrow.transform.GetChild(4).GetComponent<Text>().text;
        string Name = paymentrow.transform.GetChild(0).GetComponent<Text>().text;
        string Sessiondate = paymentrow.transform.GetChild(1).GetComponent<Text>().text;
        if (!paymentrow.transform.GetChild(3).GetComponent<Button>().IsInteractable())
        {
            yield return StartCoroutine("ConfirmAndUnmarkPayment", new object[] { paymentrow, Name, invoice, Sessiondate });
        }
        else
        {
            yield return StartCoroutine("ConfirmAndMakePayment", new object[] { paymentrow, Name, invoice, Sessiondate });
        }
    }


    public void DeleteFromList(UnityEngine.Object row)
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        GameObject ListRow = (GameObject)row;
        string id = ListRow.transform.GetChild(0).GetComponent<Text>().text;
        for (int i = 0; i < _guiPanels.Body.transform.childCount; i++)
        {
            if (_guiPanels.Body.transform.GetChild(i).gameObject.tag == CommonTypes.PrefabTag.needle.ToString())
            {
                if (_guiPanels.Body.transform.GetChild(i).GetComponent<NeedleId>().Id.ToString() == id)
                {
                    GameObject selecteditem = _guiPanels.Body.transform.GetChild(i).gameObject;
                    Color r = PointVisibilityHelper.SetNeedleDefaultColor();
                    selecteditem.transform.gameObject.GetComponent<Renderer>().material.color = r;
                    break;
                }
            }
        }
        if (PointVisibilityScript.selectedNeedles.ContainsKey(id))
        {
            PointVisibilityScript.selectedNeedles.Remove(id);
        }
        DestroyImmediate(ListRow.gameObject);
    }
    public void ViewButton(UnityEngine.Object row)
    {
        ViewPdf(row, 5);
    }

    public void ViewButtonPayment(UnityEngine.Object row)
    {
        ViewPdf(row, 4);
    }
    private static void ViewPdf(UnityEngine.Object row, int Childindex)
    {
        GameObject BillRow = (GameObject)row;
        string invoice = BillRow.transform.GetChild(Childindex).GetComponent<Text>().text;
        Camera.main.GetComponent<PdfGenerateScript>().generatebill(invoice);
    }
    public void SessionNeedleClicked(UnityEngine.Object Item)
    {
        GameObject item = (GameObject)Item;
        string ID = item.transform.GetChild(0).GetComponent<Text>().text;
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        _guiPanels.SessionNeedleFilterContentPanel.transform.parent.gameObject.SetActive(false);
        //Needles needle = PointVisibilityScript.MasterNeedles.Find(p => p.Id.ToString() == ID);
        for (int i = 0; i < _guiPanels.Body.transform.childCount; i++)
        {
            if (_guiPanels.Body.transform.GetChild(i).gameObject.tag == CommonTypes.PrefabTag.needle.ToString())
            {
                if (_guiPanels.Body.transform.GetChild(i).GetComponent<NeedleId>().Id.ToString() == ID)
                {
                    GameObject selecteditem = _guiPanels.Body.transform.GetChild(i).gameObject;
                    Camera.main.GetComponent<PointVisibilityScript>().SessionNeedleCreate(selecteditem);
                    break;
                }
            }
        }
    }
}
