﻿public class Doctor
{
    public int DoctorID { get; set; }
    public string DoctorName { get; set; }
    public string EmailID { get; set; }
    public string Address { get; set; }
    public string Gender { get; set; }
    public float PerHourFees { get; set; }
    public string Houseno { get; set; }
    public string Locality { get; set; }
    public string Town { get; set; }
    public string Pincode { get; set; }
    public string state { get; set; }
    public string ASCA { get; set; }
    public string EGK { get; set; }
    public string EMR { get; set; }
    public int RoleID { get; set; }
    public int CompanyID { get; set; }
}
