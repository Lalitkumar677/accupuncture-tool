﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Boomlagoon.JSON;
using System;

public class LoginOperationScript : MonoBehaviour, ILoginOperationScript
{

    private ServiceUrlscript _serviceUrlscript;
    private GuiPanels _guiPanels;
    private ShowPanelsScript _showPanelsScript;
    private NeedleOperationScript _needleOperationScript;
    private string selectedGender;
    public static CommonTypes.Roles LoginAs = CommonTypes.Roles.User;
    // private static readonly JCsLogger log = new JCsLogger(typeof(LoginOperationScript));
    // Use this for initialization
    void Start()
    {
        selectedGender = CommonTypes.Gender.Male.ToString();
        _serviceUrlscript = new ServiceUrlscript();
        GetandAssignComponent();
    }
    /// <summary>
    /// Gets the script from camera 
    /// </summary>
    private void GetandAssignComponent()
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        _needleOperationScript = Camera.main.GetComponent<NeedleOperationScript>();

    }
    /// <summary>
    /// Call the AuthenticateDoctor method
    /// </summary>
    public void Authenticate()
    {
        _showPanelsScript.ShowMessage("Authenticating...");
        AuthenticateDoctor();
    }
    /// <summary>
    /// Authenticates the doctor
    /// </summary>
    void AuthenticateDoctor()
    {
        string doctorname = _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(0).GetComponent<InputField>().text;
        string password = _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(1).GetComponent<InputField>().text;
        if (!string.IsNullOrEmpty(doctorname) && !string.IsNullOrEmpty(password))
        {
            WWWForm form = new WWWForm();
            form.AddField("DoctorName", doctorname);
            form.AddField("Password", password);
            WWW www = new WWW(_serviceUrlscript.AuthenticateDoctorUrl, form);
            StartCoroutine("AuthenticateDoctors", www);
        }
        else
        {
            _showPanelsScript.ShowErrorPanel(Language.Get("Enter_Details"), Language.Get("WORD_Error"));
        }
    }
    /// <summary>
    /// Ienumerator for adding doctor
    /// </summary>
    /// <param name="www"></param>
    /// <returns></returns>
    IEnumerator AuthenticateDoctors(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            _showPanelsScript.HideMessageDialogbox();
            JSONObject json = JSONObject.Parse(www.text);
            if (json != null)
            {
                int result = int.Parse(json["DoctorID"].ToString());
                if (result == -1)
                {
                    _showPanelsScript.ShowErrorPanel(Language.Get("Invalid_Credentials"), Language.Get("WORD_Warning"));
                }

                else
                {
                    var doctor = PdfGenerateScript.CreateDoctorFromJson(json);
                    Camera.main.GetComponent<UserIdScript>().doctor = doctor;
                    if (doctor.RoleID == (int)CommonTypes.Roles.User)
                    {
                        Camera.main.GetComponent<UserIdScript>().DoctorID = result;
                        LoginAs = CommonTypes.Roles.User;
                        RedirectToHomeMenu();

                    }
                    else if (doctor.RoleID == (int)CommonTypes.Roles.Admin)
                    {
                        _showPanelsScript.ShowAdminPanel();
                         LoginAs = CommonTypes.Roles.Admin;
                        _showPanelsScript.ShowMessage(Language.Get("WORD_Loading") + "...");
                        _needleOperationScript.LoadAllDoctorsForAdmin(doctor.CompanyID);
                    }
                }
            }
        }
        else
        {
            LogScript.Log(www);
            _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
        }
    }

    public void RedirectToHomeMenu()
    {
        _guiPanels.LoginPanel.SetActive(false);
        _guiPanels.Mainmenupanel.SetActive(true);
        _guiPanels.patientselectionpanel.SetActive(true);
        Camera.main.GetComponent<ShowPanelsScript>().ShowHome();
        TranslatePanels();
        PointVisibilityHelper.clearPatientselectionPanel(_guiPanels);
        _needleOperationScript.LoadDoctorDataAfterLogin();

    }

    /// <summary>
    /// translate the panels
    /// </summary>
    public static void TranslatePanels()
    {
        TranslationScript.TranslateTreatmentpanel();
        TranslationScript.TranslateMenupanel();
        TranslationScript.TranslatePatientSelectionPanel();
        TranslationScript.TranslateButtonpanel();
    }

    /// <summary>
    /// Invokes the AddDoctorDetail() and shows the message box
    /// </summary>
    public void AddDoctor()
    {
        _showPanelsScript.ShowMessage(Language.Get("WORD_Saving") + "...");
        AddDoctorDetail();
    }
    /// <summary>
    /// Add doctor to database
    /// </summary>
    private void AddDoctorDetail()
    {
        bool isformContainError = false;
        string Password = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(2).GetComponent<InputField>().text;
        string Confirmpassword = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(3).GetComponent<InputField>().text;
        isformContainError = CheckForPasswordandConfirmPasswordMatch(isformContainError, Password, Confirmpassword);
        if (isformContainError) return;
        Doctor doctor = CreateDoctorandAssignFields();
        WWWForm form = new WWWForm();
        isformContainError = IsSignUpFormValid(isformContainError, doctor, form);
        isformContainError = CheckForEmptyPassword(isformContainError, Password, form);
        isformContainError = validatePerHourFee(isformContainError, doctor, form);
        if (!string.IsNullOrEmpty(selectedGender))
            form.AddField("Gender", selectedGender);
        form.AddField("RoleID", 2);
        if (!isformContainError)
        {
            WWW www = new WWW(_serviceUrlscript.AddDoctor, form);
            StartCoroutine("AddDoctorDetails", www);
        }
        else
        {
            _showPanelsScript.ShowErrorPanel(Language.Get("Please_Fill"), Language.Get("WORD_Error"));
        }
    }
    /// <summary>
    /// Checks is the perhour fee is entered and greater then zero
    /// </summary>
    /// <param name="isformContainError"></param>
    /// <param name="doctor"></param>
    /// <param name="form"></param>
    /// <returns></returns>
    private bool validatePerHourFee(bool isformContainError, Doctor doctor, WWWForm form)
    {
        if (doctor.PerHourFees != 0f)
        {
            form.AddField("PerHourFees", doctor.PerHourFees.ToString());
        }
        else
        {
            isformContainError = true;
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(9).GetComponent<InputField>().text = string.Empty;
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(9).GetComponentInChildren<Text>().text = Language.Get("Enter_Per_Hour_Fees");
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(9).GetComponentInChildren<Text>().color = Color.red;
        }

        return isformContainError;
    }
    /// <summary>
    /// Check if password is empty then show error
    /// </summary>
    /// <param name="isformContainError"></param>
    /// <param name="Password"></param>
    /// <param name="form"></param>
    /// <returns></returns>
    private bool CheckForEmptyPassword(bool isformContainError, string Password, WWWForm form)
    {
        if (!string.IsNullOrEmpty(Password))
            form.AddField("Password", Password);
        else
        {
            isformContainError = true;
            DisplayErrorForMismatchingOrEmptyPassword();
        }
        return isformContainError;
    }
    /// <summary>
    /// Check if password matches or not
    /// </summary>
    /// <param name="isformContainError"></param>
    /// <param name="Password"></param>
    /// <param name="Confirmpassword"></param>
    /// <returns></returns>
    private bool CheckForPasswordandConfirmPasswordMatch(bool isformContainError, string Password, string Confirmpassword)
    {
        if (Password != Confirmpassword)
        {
            isformContainError = true;
            DisplayErrorForMismatchingOrEmptyPassword();
            _showPanelsScript.ShowErrorPanel(Language.Get("Password_mismatch"), Language.Get("WORD_Error"));
        }
        return isformContainError;
    }
    /// <summary>
    /// Displays error if password and confirm password mismatch
    /// </summary>
    private void DisplayErrorForMismatchingOrEmptyPassword()
    {
        for (int i = 2; i <= 3; i++)
        {
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(i).GetComponent<InputField>().text = string.Empty;
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(i).GetComponentInChildren<Text>().text = (i == 2) ? Language.Get("Enter_Password") : Language.Get("Confirm_Password");
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(i).GetComponentInChildren<Text>().color = Color.red;
        }
    }
    /// <summary>
    /// Checks if the form contain the error
    /// </summary>
    /// <param name="isformContainError"></param>
    /// <param name="doctor">Doctor to validate</param>
    /// <param name="form">Www form object</param>
    /// <returns></returns>
    private bool IsSignUpFormValid(bool isformContainError, Doctor doctor, WWWForm form)
    {
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("DoctorName", isformContainError, doctor.DoctorName, form, "Enter_Doctor_Name", 0);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("EmailId", isformContainError, doctor.EmailID, form, "Enter_Email_ID", 1);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("Houseno", isformContainError, doctor.Houseno, form, "Enter_House_No", 4);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("Locality", isformContainError, doctor.Locality, form, "Enter_Locality", 5);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("Town", isformContainError, doctor.Town, form, "Enter_Town", 6);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("Pincode", isformContainError, doctor.Pincode, form, "Enter_Pincode", 7);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("state", isformContainError, doctor.state, form, "WORD_State", 8);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("ASCA", isformContainError, doctor.ASCA, form, "Enter_ASCA", 10);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("EMR", isformContainError, doctor.EMR, form, "Enter_EMR", 11);
        isformContainError = ValidateSignUpPanelFieldAndAddInFormObject("EGK", isformContainError, doctor.EGK, form, "Enter_EGK", 12);
        return isformContainError;
    }
    /// <summary>
    /// Create the doctor object and assign the value entered by the user
    /// </summary>
    /// <returns>Doctor Object</returns>
    private Doctor CreateDoctorandAssignFields()
    {
        Doctor doctor = new Doctor();
        doctor.DoctorName = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(0).GetComponent<InputField>().text;
        doctor.EmailID = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(1).GetComponent<InputField>().text;
        doctor.Houseno = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(4).GetComponent<InputField>().text;
        doctor.Locality = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(5).GetComponent<InputField>().text;
        doctor.Town = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(6).GetComponent<InputField>().text;
        doctor.Pincode = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(7).GetComponent<InputField>().text;
        doctor.state = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(8).GetComponent<InputField>().text;
        string PerhourFees = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(9).GetComponent<InputField>().text;
        float perhourfeenumber;
        bool isperhourfeesNumber = float.TryParse(PerhourFees, out perhourfeenumber);
        if (isperhourfeesNumber)
        {
            doctor.PerHourFees = perhourfeenumber;
        }
        doctor.ASCA = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(10).GetComponent<InputField>().text;
        doctor.EMR = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(11).GetComponent<InputField>().text;
        doctor.EGK = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(12).GetComponent<InputField>().text;
        return doctor;
    }
    /// <summary>
    /// Validate the given fields and add into the wwwform object
    /// </summary>
    /// <param name="formfieldKey">Key to add in the form object</param>
    /// <param name="validate"></param>
    /// <param name="formfieldValue">Value to add in the form object</param>
    /// <param name="form">WWW form object</param>
    /// <param name="LanguageWord"></param>
    /// <param name="childIndex">Child Index of signup panel to show the warning</param>
    /// <returns></returns>
    private bool ValidateSignUpPanelFieldAndAddInFormObject(string formfieldKey, bool validate, string formfieldValue, WWWForm form, string LanguageWord, int childIndex)
    {
        if (!string.IsNullOrEmpty(formfieldValue))
            form.AddField(formfieldKey, formfieldValue);
        else
        {
            validate = true;
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(childIndex).GetComponent<InputField>().text = string.Empty;
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(childIndex).GetComponentInChildren<Text>().text = Language.Get(LanguageWord);
            _guiPanels.SignUppanel.transform.GetChild(0).GetChild(childIndex).GetComponentInChildren<Text>().color = Color.red;
        }
        return validate;
    }
    IEnumerator AddDoctorDetails(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            _showPanelsScript.HideMessageDialogbox();
            JSONObject json = JSONObject.Parse(www.text);
            if (json != null)
            {
                int Id = Convert.ToInt32(json["Id"].Number);
                Camera.main.GetComponent<UserIdScript>().DoctorID = Id;
                _guiPanels.LoginPanel.SetActive(false);
                _guiPanels.Mainmenupanel.SetActive(true);
                _guiPanels.patientselectionpanel.SetActive(true);
                _needleOperationScript.LoadDoctorDataAfterLogin();
            }
        }
        else
        {
            _showPanelsScript.HideMessageDialogbox();
            Debug.Log("WWW Error: " + www.error);
            LogScript.Log(www);
            _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
        }
    }
    public void MaleSelect()
    {
        GameObject MaleToggle = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(13).gameObject;
        GameObject FemaleToggle = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(14).gameObject;
        MaleToggle.GetComponent<Toggle>().isOn = (FemaleToggle.GetComponent<Toggle>().isOn) ? MaleToggle.GetComponent<Toggle>().isOn : true;
        FemaleToggle.GetComponent<Toggle>().isOn = (MaleToggle.GetComponent<Toggle>().isOn) ? false : FemaleToggle.GetComponent<Toggle>().isOn;
        selectedGender = CommonTypes.Gender.Male.ToString();
        MaleToggle.GetComponentInChildren<Text>().color = Color.white;

    }
    public void FemaleSelect()
    {
        GameObject MaleToggle = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(13).gameObject;
        GameObject FemaleToggle = _guiPanels.SignUppanel.transform.GetChild(0).GetChild(14).gameObject;
        FemaleToggle.GetComponent<Toggle>().isOn = (MaleToggle.GetComponent<Toggle>().isOn) ? FemaleToggle.GetComponent<Toggle>().isOn : true;
        MaleToggle.GetComponent<Toggle>().isOn = (FemaleToggle.GetComponent<Toggle>().isOn) ? false : MaleToggle.GetComponent<Toggle>().isOn;
        selectedGender = CommonTypes.Gender.Female.ToString();
        FemaleToggle.GetComponentInChildren<Text>().color = Color.white;
    }
}
