﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface INeedleOperationScript
{
    /// <summary>
    /// Send the information of single needle to the IEnumerator addneedle
    /// Sends the request to the Service
    /// </summary>
    /// <param name="needle">Needle needs to be inserted</param>
    /// <param name="name">Name entered by the user</param>
    /// <param name="LineName">Meridian Name of needle</param>
    /// <param name="color">Color of the needle</param>
    /// <param name="prened">previous needlde to draw Meridian</param>
    void AddNeedle(Needles needle, GameObject prened,GameObject needleGameObject);
    /// <summary>
    /// Send the information of multiple needle to the IEnumerator addneedlelists
    /// Sends the request to the Service 
    /// </summary>
    /// <param name="needleObjects">List of needles to be inserted</param>
    /// <param name="prened">previous needlde to draw Meridian</param>
    /// <param name="needlename">Color of the needle</param>
    void AddNeedleslist(List<GameObject> needleObjects, GameObject prened, string needlename);
    /// <summary>
    /// Sends the information of single patient to the IEnumerator 
    /// Sends the request to the Service
    /// </summary>
    /// <param name="title">Title of patient</param>
    /// <param name="fname">First name of patient</param>
    /// <param name="lname">Last name of patient</param>
    /// <param name="email">Email ID of patient</param>
    /// <param name="phonework">Work phone number of patient</param>
    /// <param name="phonmobile">Mobile Number of patient</param>
    void AddPatient(Patient patient);
    /// <summary>
    /// Send the color of line to the IEnumerator Changecolors
    /// </summary>
    /// <param name="linecolor">Color of line</param>
    void Changecolor(LineColor linecolor);
    /// <summary>
    /// Delete the needle from the database
    /// </summary>
    /// <param name="Id">Id of needle or point</param>
    void DeleteNeedle(int Id);
    /// <summary>
    /// Loads all the patient data for a perticular doctor from service
    /// </summary>
    void LoadAllpatient(int DoctorID);
    /// <summary>
    /// loads all the sessiond from service
    /// </summary>
    IEnumerator LoadAllSession();
    /// <summary>
    /// Loads all needles fron the service
    /// </summary>
    void LoadNeedles();
    /// <summary>
    /// Wait untill the service gets Executed on server
    /// Converts the json and store in list
    /// </summary>
    /// <param name="www">url to the service</param>
    /// <returns>returns null untill data load from service  </returns>
    IEnumerator LoadNeedless(WWW www);
    /// <summary>
    /// Calls the Ienumerator LoadNeedless
    /// Wait untill the Ienumerator LoadNeedless gets executed
    /// </summary>
    /// <returns>returns null untill Ienumerator LoadNeedless gets executed</returns>
    IEnumerator LoadNeedlesWithWait();
    /// <summary>
    /// Loads single patient
    /// </summary>
    /// <param name="pid">Id of the patient</param>
    /// <param name="title">Label(Text) to assign the Loaded Title to that Label</param>
    /// <param name="fname">Label(Text) to assign the Loaded first name to that Label</param>
    /// <param name="lname">Label(Text) to assign the Loaded last name to that Label</param>
    /// <param name="email">Label(Text) to assign the Loaded Email ID to that Label</param>
    /// <param name="pwork">Label(Text) to assign the Loaded name to that Label</param>
    /// <param name="pmobile">Label(Text) to assign the Loaded name to that Label</param>
    void LoadPatient(string pid, GameObject title, GameObject fname, GameObject lname, GameObject email, GameObject pwork, GameObject pmobile);
    /// <summary>
    /// send the request to the service
    /// calls the IEnumerator LoadPatients
    /// </summary>
    /// <param name="sessionID">Id of session which needs to be displayed</param>
    void LoadsessionNeedles(int sessionID);
    /// <summary>
    /// Send the request to the service
    /// calls the Ienumerator LoadsessionNeedlesEn
    /// </summary>
    /// <param name="date">Date and time Session</param>
    /// <param name="patientid">Id of patient for which the session needs to be saved</param>
    /// <param name="duration">Duration of the session</param>
    /// <param name="SessionNeedles">list of needles of the session</param>
    void SaveCurrentSession(string date, string patientid, string duration, List<Needles> SessionNeedles);
    /// <summary>
    /// Sends the request for updating the needle information to services
    /// calls the Ienumertor UpdateNeedles
    /// </summary>
    /// <param name="needle">needle object</param>
    void UpdateNeedle(Needles needle);
    /// <summary>
    /// Sends the request for update patient
    /// calls IEnumerator UpdatePatient
    /// </summary>
    /// <param name="patient">Patient object</param>
    void UpdatePatient(Patient patient);
}

