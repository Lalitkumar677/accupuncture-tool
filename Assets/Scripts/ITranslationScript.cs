﻿using System;

interface ITranslationScript
{
    /// <summary>
    /// Changes the language 
    /// </summary>
    /// <param name="Button">Languagebutton</param>
    void LanguageButton(UnityEngine.Object Button);
}

