﻿using System;
using UnityEngine;
interface INewline
{
    /// <summary>
    /// Adds new Curve in the Meridian
    /// </summary>
    void AddNewCurve();
    /// <summary>
    /// Changes the color of the meridian
    /// </summary>
    /// <param name="color">color to b applied on needle and merdian</param>
    void changecolor(Color color);
    /// <summary>
    /// deletes all meridian
    /// </summary>
    void DeleteAllLine();
    /// <summary>
    /// stores the all needle of the body in list
    /// </summary>
    void RefreshNeedles();
    /// <summary>
    /// Update all meridians
    /// </summary>
    void updateall();
    /// <summary>
    /// updates the curve when needle or points moved
    /// </summary>
    /// <param name="objectnumber">index number of needle in needle list </param>
    /// <param name="linename">name of the meridian</param>
    void updateCurve(int objectnumber, string linename);
}

