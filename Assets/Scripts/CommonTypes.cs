﻿using UnityEngine;
using System.Collections;

public class CommonTypes
{
    public enum UIControls
    {
        UIToggle,
        UITextBox,
        UIButton,
        UIGenderPanel
    }
    public enum PrefabTag
    {
        needle,
        point,
        body
    }
    public enum Gender
    {
        Male,
        Female
    }
    public enum Action
    {
        Forward,
        Backward
    }
    public enum FinanceAction
    {
        Billsent,
        Payment
    }
    public enum SessionSelectionAction
    {
        Next,
        Previous
    }
    public enum Roles
    {
        Admin = 1,
        User
    }
    
}
