﻿using System;

interface IPaymentDetailScript
{
    /// <summary>
    /// filters the bill records on the basis of textbox values
    /// </summary>
    void FilterbillRecords();
    /// <summary>
    /// filters the payment records on the basis of textbox values
    /// </summary>
    void FilterpaymentRecords();
    /// <summary>
    /// Loads all the payment record of doctor
    /// </summary>
    void LoadDoctorAllpayment();
    /// <summary>
    /// Show the history of payments(paid and unpaid)
    /// </summary>
    void SortbyshowhistoryPayment();
    /// <summary>
    /// Show the history of bill status(sent and not sent)
    /// </summary>
    void SortbyshowhistorySendbill();
    /// <summary>
    /// Updates the payments
    /// </summary>
    /// <param name="payment">Details of payment</param>
    /// <returns></returns>
    System.Collections.IEnumerator UpdatePayment(Payment payment);
}

