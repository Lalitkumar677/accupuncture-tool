﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Session
{
    public int sessionID { get; set; }
    public int PatientID { get; set; }
    public string SessionBegin { get; set; }
    public int Duration { get; set; }
    public List<Needles> Needles { get; set; }
}

