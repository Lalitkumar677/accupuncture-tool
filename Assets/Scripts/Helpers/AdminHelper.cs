﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Boomlagoon.JSON;

public class AdminHelper
{
    public static Appointment CreateAppointmentObject(GuiPanels _guiPanels)
    {
        int DoctorId = Camera.main.GetComponent<UserIdScript>().DoctorID;
        DateTime appointmentdate = DateTimePickerScript.selecteddate;
        //_guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(0).GetChild(4).GetComponentInChildren<Text>().text;
        string ClientName = _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<InputField>().text;
        DateTime startTime = _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(0).GetChild(6).GetComponent<TimeScript>().selectedTime;
        TimeSpan st = startTime.TimeOfDay;
        DateTime endTime = _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(0).GetChild(7).GetComponent<TimeScript>().selectedTime;
        TimeSpan et = endTime.TimeOfDay;
        Appointment appointment = new Appointment();
        appointment.DoctorID = DoctorId;
        appointment.AppointmentDate = appointmentdate;
        appointment.ClientName = ClientName;
        appointment.StartTime = new TimeSpan(st.Hours, st.Minutes, 0);
        appointment.EndTime = new TimeSpan(et.Hours, et.Minutes, 0);
        return appointment;
    }
    public static void ChangeWeek(ref DateTime SelectedWeekMondayDate, out DateTime SelectedWeekSundayDate, CommonTypes.Action action)
    {
        if (action == CommonTypes.Action.Backward)
        {
            SelectedWeekMondayDate = SelectedWeekMondayDate.AddDays(-7);
        }
        else
        {
            SelectedWeekMondayDate = SelectedWeekMondayDate.AddDays(7);
        }
        SelectedWeekSundayDate = SelectedWeekMondayDate.AddDays(6);
    }
    public static void GetCurrentWeekDays(DateTime currentDate, out DateTime SelectedWeekMondayDate, out DateTime SelectedWeekSundayDate)
    {
        var currentDay = currentDate.DayOfWeek;
        int days = (int)currentDay;
        SelectedWeekMondayDate = currentDate.AddDays(-days + 1);
        SelectedWeekSundayDate = SelectedWeekMondayDate.AddDays(6);
    }
    public static Appointment CreateAppointmentObjectFromJson(JSONObject json)
    {
        Appointment appointment = new Appointment();
        appointment.AppointmentID = Convert.ToInt32(json["AppointmentID"].Number);
        appointment.AppointmentDate = DateTime.Parse(json["AppointmentDate"].Str);
        appointment.ClientName = json["ClientName"].Str;
        appointment.DoctorID = Convert.ToInt32(json["DoctorID"].Number);
        appointment.EndTime = TimeSpan.Parse(json["EndTime"].Str);
        appointment.StartTime = TimeSpan.Parse(json["StartTime"].Str);
        return appointment;
    }
   
}
