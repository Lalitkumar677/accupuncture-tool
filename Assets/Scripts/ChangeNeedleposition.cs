﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(MeshCollider))]
public class ChangeNeedleposition : MonoBehaviour
{
    private Vector3 _screenPoint;
    private Vector3 _offset;
    private Vector3 initailPosition;
#if UNITY_ANDROID || UNITY_IOS
      private GuiPanels _guiPanels;
#endif
    private NeedleOperationScript _needleOperationScript;
    private JessicaRotationScript jessicaRotationScript;
    public int objectnumber;
    public string LineName;
    public string color;
    public static bool isneedlemoving;
    public bool isneedleInBody = true;
    public GameObject NameLabel;
    void Start()
    {
        _needleOperationScript = Camera.main.GetComponent<NeedleOperationScript>();
        jessicaRotationScript = GameObject.Find("Jessica").GetComponent<JessicaRotationScript>();
#if UNITY_ANDROID || UNITY_IOS
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
#endif
    }
    /// <summary>
    /// Draw the cursor
    /// </summary>
    void OnMouseDown()
    {
        isneedleInBody = false;
        if (!JessicaRotationScript.isPaused)
        {
            if (PointVisibilityScript.ReadOnly == false)
            {
                if (!ButtonClickScript.IsMouseoverUIControl)
                {
#if UNITY_STANDALONE
                    jessicaRotationScript.MoveCursor.SetActive(true);
                    jessicaRotationScript.MoveCursor.transform.position = Input.mousePosition;
                    Cursor.visible = false;
#endif
                    isneedlemoving = true;
#if UNITY_ANDROID || UNITY_IOS
            if (_guiPanels.Buttonpanel.transform.GetChild(2).gameObject.GetComponent<Toggle>().isOn == true)
            {
#endif
                    if (gameObject.tag == CommonTypes.PrefabTag.needle.ToString())
                    {
                        if (GameObject.Find("Jessica").transform.GetChild(1).GetComponent<MeshCollider>().enabled)
                        {
                            grabneedlepoint();
                        }
                    }
                    else
                    {
                        grabneedlepoint();
                    }
#if UNITY_ANDROID || UNITY_IOS
            }
#endif
                }
            }
        }
    }
    /// <summary>
    /// Gets the initial position
    /// Gets Offset between screen position and world coordinate 
    /// </summary>
    private void grabneedlepoint()
    {
        initailPosition = transform.position;
        _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
    }
    /// <summary>
    /// 
    /// </summary>
    void OnMouseDrag()
    {
        if (!JessicaRotationScript.isPaused)
        {
            isneedlemoving = true;
            if (!PointVisibilityScript.ReadOnly)
            {
                if (!ButtonClickScript.IsMouseoverUIControl)
                {
#if UNITY_ANDROID || UNITY_IOS
            if (_guiPanels.Buttonpanel.transform.GetChild(2).gameObject.GetComponent<Toggle>().isOn == true)
            {
#endif
#if UNITY_STANDALONE
                    jessicaRotationScript.MoveCursor.SetActive(true);
                    jessicaRotationScript.MoveCursor.transform.position = Input.mousePosition;
                    Cursor.visible = false;
#endif
                    if (gameObject.tag == CommonTypes.PrefabTag.needle.ToString())
                    {
                        if (GameObject.Find("Jessica").transform.GetChild(1).GetComponent<MeshCollider>().enabled)
                        {
                            Dragneedlepoint();
                        }
                        else
                        {
                            jessicaRotationScript.infoLabel.SetActive(true);
                            jessicaRotationScript.infoLabel.transform.position = new Vector2(Input.mousePosition.x + jessicaRotationScript.MoveCursor.GetComponent<RectTransform>().rect.width, Input.mousePosition.y);
                        }
                    }
                    else
                    {
                        Dragneedlepoint();
                    }
#if UNITY_ANDROID || UNITY_IOS
            }
#endif
                }
            }
        }
    }
    /// <summary>
    /// Update the needle position with mouse position
    /// </summary>
    private void Dragneedlepoint()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
        transform.position = curPosition;
        GameObject.Find("Jessica").GetComponent<Newline>().updateCurve(objectnumber, LineName);
    }
    /// <summary>
    /// Check whether the needle is on body and then update the needle and point 
    /// </summary>
    void OnMouseUp()
    {
        if (!JessicaRotationScript.isPaused)
        {
            GameObject.Find("Jessica").transform.Rotate(0f, 0.02f, 0f);
            isneedlemoving = false;
            if (PointVisibilityScript.ReadOnly == false)
            {
                if (!ButtonClickScript.IsMouseoverUIControl)
                {
#if UNITY_ANDROID || UNITY_IOS
            if (_guiPanels.Buttonpanel.transform.GetChild(2).gameObject.GetComponent<Toggle>().isOn == true)
            {
#endif
#if UNITY_STANDALONE
                    GameObject.Find("Jessica").GetComponent<JessicaRotationScript>().MoveCursor.SetActive(false);

                    Cursor.visible = true;
#endif
                    if (transform.tag == CommonTypes.PrefabTag.needle.ToString() || transform.tag == CommonTypes.PrefabTag.point.ToString())
                    {
                        jessicaRotationScript.infoLabel.SetActive(false);
                        if (gameObject.tag == CommonTypes.PrefabTag.needle.ToString())
                        {
                            if (GameObject.Find("Jessica").transform.GetChild(1).GetComponent<MeshCollider>().enabled)
                            {
                                Dropneedlepoint();
                            }
                        }
                        else
                        {
                            Dropneedlepoint();
                        }
                    }
#if UNITY_ANDROID || UNITY_IOS
            }
#endif
                }
            }
        }
    }
    /// <summary>
    /// update the needle and points 
    /// </summary>
    private void Dropneedlepoint()
    {
        NeedleId needleIdScript = transform.GetComponent<NeedleId>();
        int id = needleIdScript.Id;
        string name = needleIdScript.Name;
        if (transform.tag == CommonTypes.PrefabTag.point.ToString())
        {
            name = string.Empty;
            Needles needle = CreateAndAssignNeedleObject(id, name);
            _needleOperationScript.UpdateNeedle(needle);
        }
        else
        {
            object[] para = new object[] { id, name };
            StartCoroutine("UpdateNeedle", para);
        }
    }

    private Needles CreateAndAssignNeedleObject(int id, string name)
    {
        Needles needle = new Needles();
        needle.Id = id;
        needle.PositionX = transform.localPosition.x;
        needle.PositionY = transform.localPosition.y;
        needle.PositionZ = transform.localPosition.z;
        needle.RotationX = transform.localRotation.x;
        needle.RotationY = transform.localRotation.y;
        needle.RotationZ = transform.localRotation.z;
        needle.RotationW = transform.localRotation.w;
        needle.ScaleX = transform.localScale.x;
        needle.ScaleY = transform.localScale.y;
        needle.ScaleZ = transform.localScale.z;
        needle.NeedleName = name;
        needle.Linename = LineName;
        needle.color = color;
        needle.Prefab = transform.tag;
        return needle;
    }

    /// <summary>
    /// update needle if needle is on body other wise set to its initial position
    /// </summary>
    /// <param name="para">an aaray with id  and name of needles </param>
    /// <returns>Returns null</returns>
    IEnumerator UpdateNeedle(object[] para)
    {
        if (GameObject.Find("Jessica").transform.GetChild(1).GetComponent<MeshCollider>().enabled)
        {
            yield return new WaitForSeconds(0.05f);
            int id = (int)para[0];
            string name = (string)para[1];
            if (isneedleInBody)
            {
                Needles needle = CreateAndAssignNeedleObject(id, name);
                _needleOperationScript.UpdateNeedle(needle);
            }
            else
            {
                transform.position = initailPosition;
                GameObject.Find("Jessica").GetComponent<Newline>().updateall();
            }
        }
    }
    /// <summary>
    /// Tells that the needle is in touch with body
    /// </summary>
    /// <param name="other">Game object which collides with needle</param>
    void OnTriggerEnter(Collider other)
    {
        AssignIsNeedleInBody(other, true);
    }

    /// <summary>
    /// Tells that the needle is in touch with body
    /// </summary>
    /// <param name="other">Game object which collides with needle</param>
    void OnTriggerStay(Collider other)
    {
        AssignIsNeedleInBody(other, true);
    }
    /// <summary>
    /// Tells that the needle is in touch with body
    /// </summary>
    /// <param name="other">Game object which collides with needle</param>
    void OnTriggerExit(Collider other)
    {
        AssignIsNeedleInBody(other, false);
    }
    /// <summary>
    /// Assign the isneedleinbody to the given value if the collider is tagged as body and is enable
    /// </summary>
    /// <param name="other"></param>
    /// <param name="isinbody"></param>
    private void AssignIsNeedleInBody(Collider other, bool isinbody)
    {
        if (transform.tag == CommonTypes.PrefabTag.needle.ToString() && other.tag == "body" && GameObject.Find("Jessica").transform.GetChild(1).GetComponent<MeshCollider>().enabled)
        {
            isneedleInBody = isinbody;
        }
    }
}