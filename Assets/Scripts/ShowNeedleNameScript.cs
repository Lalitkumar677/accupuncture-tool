﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShowNeedleNameScript : MonoBehaviour
{
    private NeedleScript needlescript;
    public Canvas canvas;
    public GameObject Label;
    public GameObject Labelprefab;
    public bool instanciated = false;
    public bool shoulddestroylabel = false;
    // Use this for initialization
    void Start()
    {
        needlescript = Camera.main.GetComponent<NeedleScript>();
        canvas = needlescript.canvas;
        Labelprefab = needlescript.label;
    }
    // Update is called once per frame
    void Update()
    {
        if (Label != null)
        {
            Vector3 textposition = Camera.main.WorldToScreenPoint(transform.position);
            Label.transform.position = new Vector3(textposition.x + 65f, textposition.y + 40f, textposition.z);
        }
    }
    /// <summary>
    /// On mouse hover changes  the color of the needle
    /// </summary>
    void OnMouseEnter()
    {
        if (!ButtonClickScript.IsMouseoverUIControl)
        {
            if (!JessicaRotationScript.isPaused)
            {
                NeedlePreviousColor = gameObject.GetComponent<Renderer>().material.color;
                GameObject selectedneedle = GameObject.Find("Jessica").GetComponent<JessicaRotationScript>().SelectedNeedle;
                if (Camera.main.GetComponent<GuiPanels>().SessionEditorPanel.activeInHierarchy)
                {
                    string id = gameObject.transform.GetComponent<NeedleId>().Id.ToString();
                    if (!PointVisibilityScript.selectedNeedles.ContainsKey(id))
                    {
                        gameObject.GetComponent<Renderer>().material.color = Color.magenta;
                    }
                }
                else if (selectedneedle != gameObject)
                {
                    gameObject.GetComponent<Renderer>().material.color = Color.magenta;
                }
            }
        }
    }
    Color NeedlePreviousColor;
    /// <summary>
    /// onclick select the needle and changes the needle color to Red
    /// </summary>
    void OnMouseDown()
    {
        if (!ButtonClickScript.IsMouseoverUIControl)
        {
            if (!JessicaRotationScript.isPaused)
            {
                GameObject selectedneedle = GameObject.Find("Jessica").GetComponent<JessicaRotationScript>().SelectedNeedle;
                if (gameObject.transform.GetComponent<NeedleId>().Name == null)
                {
                    gameObject.transform.GetComponent<NeedleId>().Name = string.Empty;
                }
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(3).GetComponent<Text>().text = gameObject.transform.GetComponent<NeedleId>().Name;
                if (!Camera.main.GetComponent<GuiPanels>().SessionEditorPanel.activeInHierarchy)
                {
                    if (selectedneedle != null)
                    {
                       
                        selectedneedle.GetComponent<Renderer>().material.color = PointVisibilityHelper.SetNeedleDefaultColor();
                    }
                }
                gameObject.GetComponent<Renderer>().material.color = Color.red;
                GameObject.Find("Jessica").GetComponent<JessicaRotationScript>().SelectedNeedle = gameObject;
                instanciatelabel();
                needlescript.Hoveredbuttonneedle = gameObject;
            }
        }
    }
    /// <summary>
    /// on mouse hover Exit loads the default color of the needle
    /// </summary>
    void OnMouseExit()
    {
        if (!JessicaRotationScript.isPaused)
        {
            GameObject selectedneedle = GameObject.Find("Jessica").GetComponent<JessicaRotationScript>().SelectedNeedle;
            if (!PointVisibilityScript.ReadOnly)
            {
                if (selectedneedle != gameObject)
                {
                    gameObject.GetComponent<Renderer>().material.color = PointVisibilityHelper.SetNeedleDefaultColor();
                }
            }
            else
            {
                if (!Camera.main.GetComponent<GuiPanels>().SessionEditorPanel.activeInHierarchy)
                {
                    gameObject.GetComponent<Renderer>().material.color = NeedlePreviousColor;
                }
                else
                {
                    string id = gameObject.transform.GetComponent<NeedleId>().Id.ToString();
                    if (!PointVisibilityScript.selectedNeedles.ContainsKey(id))
                    {
                        gameObject.GetComponent<Renderer>().material.color = PointVisibilityHelper.SetNeedleDefaultColor();
                    }
                }
            }
            StartCoroutine("destroylabel");
        }
    }
    /// <summary>
    /// destroys the genrated label after 2 seconds
    /// </summary>
    /// <returns>returns null</returns>
    IEnumerator destroylabel()
    {
        yield return new WaitForSeconds(2f);
        if (Label != null)
        {
            ButtonClickScript.IsMouseoverUIControl = false;
            Destroy(Label);
            instanciated = false;
        }
    }
    /// <summary>
    /// Generates the label to show name of the needle
    /// </summary>
    void instanciatelabel()
    {
        if (!instanciated)
        {
            NeedleId _needleIdscript = transform.GetComponent<NeedleId>();
            if (_needleIdscript != null)
            {
                Label = Instantiate(Labelprefab) as GameObject;
                Label.transform.SetParent(canvas.transform);
                Label.GetComponentInChildren<Text>().text = _needleIdscript.Name;
                if (PointVisibilityScript.ReadOnly == true)
                {
                    Destroy(Label.transform.GetChild(2).gameObject);
                }
                Vector3 textposition = Camera.main.WorldToScreenPoint(transform.position);
                Label.transform.position = new Vector3(textposition.x + 65f, textposition.y + 40f, textposition.z);
                instanciated = true;
            }
        }
    }
}
