﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using System;

public class NeedleOperationHelper
{
    /// <summary>
    /// create the json string for the needle and helping point to be added while adding a new needle
    /// </summary>
    /// <param name="needleObjects">list of Needle and two other helping point</param>
    /// <param name="prened">Previous needle in the meridian</param>
    /// <param name="needlename">Name of needle</param>
    /// <returns></returns>
    public static string CreateJsonForNeedleAndpoint(List<GameObject> needleObjects, GameObject prened, string needlename)
    {
        string JsonForNeedlesandPoint = "{";
        if (needleObjects.Count > 0)
        {
            JsonForNeedlesandPoint += "\"Needles\":";
            JsonForNeedlesandPoint += "[{";
            for (int i = 0; i < needleObjects.Count; i++)
            {
                JsonForNeedlesandPoint += "\"PositionX\":\"" + needleObjects[i].transform.localPosition.x + "\",";
                JsonForNeedlesandPoint += "\"PositionY\":\"" + needleObjects[i].transform.localPosition.y + "\",";
                JsonForNeedlesandPoint += "\"PositionZ\":\"" + needleObjects[i].transform.localPosition.z + "\",";
                JsonForNeedlesandPoint += "\"RotationX\":\"" + needleObjects[i].transform.localRotation.x + "\",";
                JsonForNeedlesandPoint += "\"RotationY\":\"" + needleObjects[i].transform.localRotation.y + "\",";
                JsonForNeedlesandPoint += "\"RotationZ\":\"" + needleObjects[i].transform.localRotation.z + "\",";
                JsonForNeedlesandPoint += "\"RotationW\":\"" + needleObjects[i].transform.localRotation.w + "\",";
                JsonForNeedlesandPoint += "\"ScaleX\":\"" + needleObjects[i].transform.localScale.x + "\",";
                JsonForNeedlesandPoint += "\"ScaleY\":\"" + needleObjects[i].transform.localScale.y + "\",";
                JsonForNeedlesandPoint += "\"ScaleZ\":\"" + needleObjects[i].transform.localScale.z + "\",";
                JsonForNeedlesandPoint += "\"Prefab\":\"" + needleObjects[i].tag + "\",";
                if (needleObjects[i].tag == CommonTypes.PrefabTag.needle.ToString())
                {
                    JsonForNeedlesandPoint += "\"NeedleName\":\"" + needlename + "\",";
                }
                string linename = needleObjects[i].GetComponent<ChangeNeedleposition>().LineName;
                string color = needleObjects[i].GetComponent<ChangeNeedleposition>().color;
                JsonForNeedlesandPoint += "\"LineName\":\"" + linename + "\",";
                JsonForNeedlesandPoint += "\"Color\":\"" + color + "\"";
                JsonForNeedlesandPoint += "},{";
            }
            JsonForNeedlesandPoint = JsonForNeedlesandPoint.Substring(0, JsonForNeedlesandPoint.Length - 2) + "]";
        }
        JsonForNeedlesandPoint += "}";
        return JsonForNeedlesandPoint;
    }
    /// <summary>
    /// Creates the json string for the session needs to  be inserted,
    /// </summary>
    /// <param name="date">Date of the session</param>
    /// <param name="patientid">Id of the patient for which the session is going to be inserted</param>
    /// <param name="duration"> Duartion of session</param>
    /// <param name="SessionNeedles">List of the needle which are selected in the session</param>
    /// <returns></returns>
    public static string CreateJsonForsession(string date, string patientid, string duration, List<Needles> SessionNeedles)
    {
        string JsonForSession = "{\"Duration\":\"" + duration + "\",";
        JsonForSession += "\"PatientId\":\"" + patientid + "\",";
        JsonForSession += "\"SessionDate\":\"" + date + "\"";
        if (SessionNeedles.Count > 0)
        {
            JsonForSession += ",\"SessionNeedles\":";
            JsonForSession += "[{";
            for (int i = 0; i < SessionNeedles.Count; i++)
            {
                JsonForSession += "\"PositionX\":\"" + SessionNeedles[i].PositionX + "\",";
                JsonForSession += "\"PositionY\":\"" + SessionNeedles[i].PositionY + "\",";
                JsonForSession += "\"PositionZ\":\"" + SessionNeedles[i].PositionZ + "\",";
                JsonForSession += "\"RotationX\":\"" + SessionNeedles[i].RotationX + "\",";
                JsonForSession += "\"RotationY\":\"" + SessionNeedles[i].RotationY + "\",";
                JsonForSession += "\"RotationZ\":\"" + SessionNeedles[i].RotationZ + "\",";
                JsonForSession += "\"RotationW\":\"" + SessionNeedles[i].RotationW + "\",";
                JsonForSession += "\"ScaleX\":\"" + SessionNeedles[i].ScaleX + "\",";
                JsonForSession += "\"ScaleY\":\"" + SessionNeedles[i].ScaleY + "\",";
                JsonForSession += "\"ScaleZ\":\"" + SessionNeedles[i].ScaleZ + "\",";
                JsonForSession += "\"Prefab\":\"" + SessionNeedles[i].Prefab + "\",";
                JsonForSession += "\"NeedleName\":\"" + SessionNeedles[i].NeedleName + "\",";
                JsonForSession += "\"LineName\":\"" + SessionNeedles[i].Linename + "\",";
                JsonForSession += "\"Color\":\"" + SessionNeedles[i].color + "\"";
                JsonForSession += "},{";
            }
            JsonForSession = JsonForSession.Substring(0, JsonForSession.Length - 2) + "]";
        }
        JsonForSession += "}";
        return JsonForSession;
    }
    /// <summary>
    /// Add Patient field in www Form object
    /// </summary>
    /// <param name="patient"></param>
    /// <param name="form"></param>
    public static void AddPatientfieldinFormobject(Patient patient, WWWForm form)
    {
        if (!string.IsNullOrEmpty(patient.Title))
            form.AddField("Title", patient.Title);
        if (!string.IsNullOrEmpty(patient.FName))
            form.AddField("FirstName", patient.FName);
        if (!string.IsNullOrEmpty(patient.LName))
            form.AddField("LastName", patient.LName);
        if (!string.IsNullOrEmpty(patient.Email))
            form.AddField("Email", patient.Email);
        if (!string.IsNullOrEmpty(patient.Pwork))
            form.AddField("PhoneNo", patient.Pwork);
        if (!string.IsNullOrEmpty(patient.Pmobile))
            form.AddField("MobileNo", patient.Pmobile);
        if (!string.IsNullOrEmpty(patient.Gender))
            form.AddField("Gender", patient.Gender);
        if (!string.IsNullOrEmpty(patient.Houseno))
            form.AddField("Houseno", patient.Houseno);
        if (!string.IsNullOrEmpty(patient.Locality))
            form.AddField("Locality", patient.Locality);
        if (!string.IsNullOrEmpty(patient.Town))
            form.AddField("Town", patient.Town);
        if (!string.IsNullOrEmpty(patient.Pincode))
            form.AddField("Pincode", patient.Pincode);
        if (!string.IsNullOrEmpty(patient.state))
            form.AddField("state", patient.state);
        string DoctorId = Camera.main.GetComponent<UserIdScript>().DoctorID.ToString();
        if (!string.IsNullOrEmpty(DoctorId))
        {
            form.AddField("DoctorId", DoctorId);
        }
    }
    /// <summary>
    /// Display Error to user and create log for error
    /// </summary>
    /// <param name="www"></param>
    /// <param name="_showPanelsScript"></param>
    public static void DisplayErrorTouserAndCreateLog(WWW www, ShowPanelsScript _showPanelsScript)
    {
        Debug.Log("WWW Error: " + www.error);
        if (www.error != "Could not resolve host: accupuntureservice.de.wvps46-163-110-202.dedicated.hosteurope.de; Host not found")
        {
            LogScript.Log(www);
        }
        _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
    }
    /// <summary>
    /// Gets the value from the json array and creata a needle object
    /// </summary>
    /// <param name="json">Json object</param>
    /// <param name="i">index</param>
    /// <returns>needle object</returns>
    public static Needles CreateNeedleObjectfromjson(JSONArray json, int i)
    {
        Needles n = new Needles();
        n.Id = Convert.ToInt32(json[i].Obj["Id"].Number);
        n.PositionX = float.Parse(json[i].Obj["PositionX"].Number.ToString());
        n.PositionY = float.Parse(json[i].Obj["PositionY"].Number.ToString());
        n.PositionZ = float.Parse(json[i].Obj["PositionZ"].Number.ToString());
        n.RotationX = float.Parse(json[i].Obj["RotationX"].Number.ToString());
        n.RotationY = float.Parse(json[i].Obj["RotationY"].Number.ToString());
        n.RotationZ = float.Parse(json[i].Obj["RotationZ"].Number.ToString());
        n.RotationW = float.Parse(json[i].Obj["RotationW"].Number.ToString());
        n.ScaleX = float.Parse(json[i].Obj["ScaleX"].Number.ToString());
        n.ScaleY = float.Parse(json[i].Obj["ScaleY"].Number.ToString());
        n.ScaleZ = float.Parse(json[i].Obj["ScaleZ"].Number.ToString());
        n.Linename = json[i].Obj["LineName"].Str;
        n.Prefab = json[i].Obj["Prefab"].Str;
        n.NeedleName = json[i].Obj["NeedleName"].Str;
        n.color = json[i].Obj["Color"].Str;
        return n;
    }
    /// <summary>
    /// Create the patient object from the json values
    /// </summary>
    /// <param name="json">json object</param>
    /// <param name="i">index</param>
    /// <returns>Patient object</returns>
    public static Patient CreatePatientObjectFromJson(JSONArray json, int i)
    {
        Patient patient = new Patient();
        patient.ID = Convert.ToInt32(json[i].Obj["PatientId"].Number);
        patient.Title = json[i].Obj["Title"].Str;
        patient.FName = json[i].Obj["FirstName"].Str;
        patient.LName = json[i].Obj["LastName"].Str;
        patient.Email = json[i].Obj["Email"].Str;
        patient.Pwork = json[i].Obj["PhoneNo"].Str;
        patient.Pmobile = json[i].Obj["MobileNo"].Str;
        patient.Gender = json[i].Obj["Gender"].Str;
        patient.Houseno = json[i].Obj["Houseno"].Str;
        patient.Locality = json[i].Obj["Locality"].Str;
        patient.Pincode = json[i].Obj["Pincode"].Str;
        patient.state = json[i].Obj["state"].Str;
        patient.Town = json[i].Obj["Town"].Str;
        patient.DoctorID = Convert.ToInt32(json[i].Obj["DoctorID"].Number);
        return patient;
    }
    /// <summary>
    /// Create www from object from the needle object to insert int the database
    /// </summary>
    /// <param name="needle">Needle object which needs to be inserted</param>
    /// <returns>WWWWForm Object</returns>
    public static WWWForm CreateFormObjectFromNeedleInfo(Needles needle)
    {
        WWWForm form = new WWWForm();
        form.AddField("PositionX", needle.PositionX.ToString());
        form.AddField("PositionY", needle.PositionY.ToString());
        form.AddField("PositionZ", needle.PositionZ.ToString());
        form.AddField("RotationX", needle.RotationX.ToString());
        form.AddField("RotationY", needle.RotationY.ToString());
        form.AddField("RotationZ", needle.RotationZ.ToString());
        form.AddField("RotationW", needle.RotationW.ToString());
        form.AddField("ScaleX", needle.ScaleX.ToString());
        form.AddField("ScaleY", needle.ScaleY.ToString());
        form.AddField("ScaleZ", needle.ScaleZ.ToString());
        if (needle.Id != -1)
        {
            form.AddField("Id", needle.Id.ToString());
        }
        form.AddField("Prefab", needle.Prefab.ToString());
        if (string.IsNullOrEmpty(needle.NeedleName))
        {
            needle.NeedleName = string.Empty;
        }
        if (string.IsNullOrEmpty(needle.Linename))
        {
            needle.Linename = string.Empty;
        }
        if (string.IsNullOrEmpty(needle.color))
        {
            needle.color = string.Empty;
        }
        form.AddField("NeedleName", needle.NeedleName);
        form.AddField("LineName", needle.Linename);
        form.AddField("Color", needle.color);
        return form;
    }
    /// <summary>
    /// Destroy Needle And Points IF Error Occure
    /// </summary>
    /// <param name="needle"></param>
    /// <param name="_needlescript"></param>
    public static void DestroyNeedleAndPointsIFErrorOccure(List<GameObject> needle, NeedleScript _needlescript)
    {
        for (int i = needle.Count - 1; i >= 0; i--)
        {
            if (_needlescript.textbox != null)
            {
                UnityEngine.Object.DestroyImmediate(_needlescript.textbox);
            }
            if (needle[i].GetComponent<ChangeNeedleposition>() != null)
            {
                if (needle[i].tag == CommonTypes.PrefabTag.needle.ToString())
                {
                    needle[i].GetComponent<ChangeNeedleposition>().NameLabel.transform.parent = null;
                    UnityEngine.Object.DestroyImmediate(needle[i].GetComponent<ChangeNeedleposition>().NameLabel);
                }
            }
            UnityEngine.Object.DestroyImmediate(needle[i]);
        }
    }
    /// <summary>
    /// Create the session object from the given json object
    /// </summary>
    /// <param name="json">json object</param>
    /// <param name="i">index</param>
    /// <returns>session object</returns>
    public static Session CreateSessionObjectfromJson(JSONArray json, int i)
    {
        Session session = new Session();
        session.sessionID = int.Parse(json[i].Obj["Sid"].Number.ToString());
        session.Duration = int.Parse(json[i].Obj["Duration"].Number.ToString());
        session.SessionBegin = json[i].Obj["SessionDate"].Str;
        session.PatientID = int.Parse(json[i].Obj["PatientId"].Number.ToString());
        return session;
    }
}
