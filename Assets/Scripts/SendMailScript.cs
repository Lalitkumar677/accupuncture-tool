﻿using UnityEngine;
using System.Collections;
using System;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using log4net;

public class SendMailScript
{
    public static void SendMail(Exception exception)
    {
        MailMessage mailMessage = new MailMessage("acupuncturelogger@gmail.com", " DEViNIT@gmx.ch");
        mailMessage.CC.Add("lalit.kumar@devinit.ch");
        CreateContent(exception, mailMessage);
        mailMessage.Subject = "Log For Acupuncture";
        SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
        smtpClient.Credentials = new NetworkCredential()
        {
            UserName = "Acupuncturelogger@gmail.com",
            Password = "Devindia_01"
        };
        smtpClient.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        };
        smtpClient.Send(mailMessage);
    }

    private static void CreateContent(Exception exception, MailMessage mailMessage)
    {
        StringBuilder sbEmailBody = new StringBuilder();
        sbEmailBody.Append("Dear User,<br/><br/>");
        sbEmailBody.Append("Log for Acupuncture");
        sbEmailBody.Append("<br/><br/>");
        sbEmailBody.Append("Exception Type   :   " + exception.GetType().ToString());
        sbEmailBody.Append("<br/><br/>");
        sbEmailBody.Append("Exception Message   :   " + exception.Message);
        sbEmailBody.Append("<br/><br/>");
        sbEmailBody.Append("Exception StackTrace   :   " + exception.StackTrace);
        sbEmailBody.Append("<br/><br/>");
        mailMessage.IsBodyHtml = true;
        mailMessage.Body = sbEmailBody.ToString();
    }
}
