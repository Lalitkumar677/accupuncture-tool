﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class TimeScript : MonoBehaviour
{

    // Use this for initialization
    GameObject Hours;
    GameObject Minutes;
    GameObject AmPm;
    GameObject selectedGameObject;
    static DateTime currenttime;
    public DateTime selectedTime;
    void Start()
    {
        Hours = gameObject.transform.GetChild(0).gameObject;
        Minutes = gameObject.transform.GetChild(1).gameObject;
        AmPm = gameObject.transform.GetChild(2).gameObject;
        currenttime = DateTime.Now;
        selectedTime = currenttime;
        Hours.transform.GetComponentInChildren<InputField>().text = currenttime.ToString("hh");
        Minutes.transform.GetComponentInChildren<InputField>().text = currenttime.ToString("mm");
        AmPm.transform.GetComponentInChildren<Text>().text = currenttime.ToString("tt");
        selectedGameObject = Hours;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SelectGameobject(UnityEngine.Object obj)
    {
        selectedGameObject = (GameObject)obj;
        Hours.GetComponent<Image>().color = new Color(255, 255, 255, 255);
        Minutes.GetComponent<Image>().color = new Color(255, 255, 255, 255);
        AmPm.GetComponent<Image>().color = new Color(255, 255, 255, 255);
        selectedGameObject.GetComponent<Image>().color = new Color(0, 0, 255, 100);
    }

    public void Next()
    {
        ChangeTime(CommonTypes.Action.Forward);
    }

    private void ChangeTime(CommonTypes.Action action)
    {
        if (selectedGameObject.tag == "HH")
        {
            ChangeHours(action);
        }
        else if (selectedGameObject.tag == "MM")
        {
            ChangeMinute(action);
        }
        else if (selectedGameObject.tag == "TT")
        {
            if (action == CommonTypes.Action.Forward)
            {
                selectedTime = selectedTime.AddHours(12);
            }
            else
            {
                selectedTime = selectedTime.AddHours(-12);
            }
        }
        Hours.transform.GetComponentInChildren<InputField>().text = selectedTime.ToString("hh");
        Minutes.transform.GetComponentInChildren<InputField>().text = selectedTime.ToString("mm");
        AmPm.transform.GetComponentInChildren<Text>().text = selectedTime.ToString("tt");
    }
    public void valuechange()
    {
        if (selectedGameObject != null)
        {
            if (selectedGameObject.tag == "HH")
            {
                string hour = Hours.transform.GetComponentInChildren<InputField>().text;
                int hh = 0;
                bool HourConverted = int.TryParse(hour, out hh);
                if (HourConverted)
                {
                    if (hh > 12 || hh < 0)
                    {
                        Hours.transform.GetComponentInChildren<InputField>().text = selectedTime.Hour.ToString();
                    }
                }

            }
            else if (selectedGameObject.tag == "MM")
            {
                string minutes = Minutes.transform.GetComponentInChildren<InputField>().text;
                int mm = 0;
                bool minuteConverted = int.TryParse(minutes, out mm);
                if (minuteConverted)
                {
                    if (mm > 59 || mm < 0)
                    {
                        Minutes.transform.GetComponentInChildren<InputField>().text = selectedTime.Minute.ToString();
                    }
                }
            }
        }
    }

    private void ChangeHours(CommonTypes.Action action)
    {
        string h = Hours.transform.GetComponentInChildren<InputField>().text;
        int hh = 0;
        bool HourConverted = int.TryParse(h, out hh);
        if (HourConverted)
        {
            if (action == CommonTypes.Action.Forward)
            {
                hh++;
            }
            else
            {
                hh--;
            }
            int selectedhour = selectedTime.Hour;
            selectedhour = selectedhour % 12;
            int diff = hh - selectedhour;
            selectedTime = selectedTime.AddHours(diff);
        }
    }
    private void ChangeMinute(CommonTypes.Action action)
    {
        string m = Minutes.transform.GetComponentInChildren<InputField>().text;
        int mm = 0;
        bool MinutesConverted = int.TryParse(m, out mm);
        if (MinutesConverted)
        {
            if (action == CommonTypes.Action.Forward)
            {
                mm++;
            }
            else
            {
                mm--;
            }
            int selectedminute = selectedTime.Minute;
            int diff = mm - selectedminute;
            selectedTime = selectedTime.AddMinutes(diff);
        }
    }
    public void Previous()
    {
        ChangeTime(CommonTypes.Action.Backward);
    }
}
