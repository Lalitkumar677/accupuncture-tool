﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Vectrosity;
using System.Collections.Generic;

public class JessicaRotationScript : MonoBehaviour, IJessicaRotationScript
{
    public static float _rotationSpeed;
    private float _zoomSpeed;
    private RaycastHit _hit;
    private float temps = 0f;
    private Vector3 _screenPoint;
    private Vector3 _offset;
    private GuiPanels _guiPanels;
    private ShowPanelsScript _showPanelsScript;
    private int segment = 60;
    private NeedleOperationScript _needleOperationScript;
    private Newline _newline;
    public static bool isPaused = false;
    public GameObject cube;
    public GameObject infoLabel;
    public GameObject HandGrip;
    public GameObject MoveCursor;
    public static float orthoZoomSpeed;
    public static float perspectiveZoomSpeed;
    public GameObject selectedlabel;
    public static float moveSpeed;
    public GameObject SelectedNeedle;
    // Use this for initialization
    void Start()
    {
        GetAndAssignComponent();
#if UNITY_STANDALONE
        _guiPanels.Buttonpanel.transform.GetChild(2).gameObject.SetActive(false);
        _rotationSpeed = 5.0F;
#endif
#if UNITY_IOS
        _rotationSpeed = 1.0F;
        orthoZoomSpeed = 0.3f;
        perspectiveZoomSpeed = 0.3f;
#endif
#if UNITY_ANDROID
        moveSpeed = 0.2f;
        orthoZoomSpeed = 1.5f;
        perspectiveZoomSpeed = 1.5f;
        _rotationSpeed = 15.0F;
#endif
        _zoomSpeed = 5.0F;
    }
    private void GetAndAssignComponent()
    {
        if (_guiPanels == null)
        {
            _guiPanels = Camera.main.GetComponent<GuiPanels>();
        }
        if (_needleOperationScript == null)
        {
            _needleOperationScript = Camera.main.GetComponent<NeedleOperationScript>();
        }
        if (_newline == null)
        {
            _newline = transform.GetComponent<Newline>();
        }
        if (_showPanelsScript == null)
        {
            _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        }
    }

    // Update is called once per frame
    /// <summary>
    /// Destroy the point related to the needle
    /// </summary>
    /// <param name="index1">Index of First point</param>
    /// <param name="index2">Index of Second point</param>
    /// <param name="LineName">Meridian Name</param>
    private void Destroypoint(int index1, int index2, string LineName)
    {
        DestroyPointOneByOne(index1, LineName);
        DestroyPointOneByOne(index2, LineName);
    }

    private void DestroyPointOneByOne(int index, string LineName)
    {
        GameObject point = Newline.LinesNeedles[LineName][index];
        Deletepoint(point);
        point.transform.parent = null;
        Destroy(point);
    }

    /// <summary>
    /// deletes the points from the database
    /// </summary>
    /// <param name="obj">point to be deleted</param>
    private void Deletepoint(GameObject obj)
    {
        GetAndAssignComponent();
        int needleId = obj.transform.gameObject.GetComponent<NeedleId>().Id;
        _needleOperationScript.DeleteNeedle(needleId);
        
    }
    /// <summary>
    /// On mouse scroll click  and move mouse Rotate the body for pc version.
    /// Rotate the body with single touch and move finger for mobile version.
    /// Zoom in and Zoom out with mouse scroll for pc version.
    /// Zoom in and Zoom out with two finger(Expand and shrink) for Mobile version.
    /// move the body with click and move mouse for pc version.
    /// move the body with three finger touch and move fingers for Mobile version.
    /// </summary>
    void Update()
    {
        GetAndAssignComponent();
        //Rotate

#if UNITY_STANDALONE
        RotateBodyForStandAlone();
#endif
#if UNITY_ANDROID || UNITY_IOS
        RatateForAndriodAndIos();
#endif
        //Zoom
#if UNITY_STANDALONE
        ZoomInAndZoomOutForStandALone();
#endif
#if UNITY_ANDROID || UNITY_IOS
        ZoomInAndZoomoutForAndriodAndIos();
#endif

        //Move
#if UNITY_STANDALONE

        MoveForStandAlone();
#endif
#if UNITY_ANDROID || UNITY_IOS
        MoveForAndriodandIos();
#endif
    }

    private void MoveForAndriodandIos()
    {
        if (!isPaused && !ButtonClickScript.isSliderValueChanging)
        {
            if (_guiPanels.Buttonpanel.transform.GetChild(2).gameObject.GetComponent<Toggle>().isOn == false)
            {
                if (!ButtonClickScript.IsMouseoverUIControl)
                {
                    if (Input.touchCount == 1)
                    {
                        Touch touch = Input.GetTouch(0);
                        if (touch.phase == TouchPhase.Began)
                        {
                            _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
                            _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, _screenPoint.z));
                        }
                        else if (touch.phase == TouchPhase.Moved)
                        {
                            Vector3 curScreenPoint = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, _screenPoint.z);
                            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
                            transform.position = curPosition;
                            _newline.updateall();
                        }
                    }
                }
            }
        }
    }
    private void ZoomInAndZoomoutForAndriodAndIos()
    {
        if (!isPaused)
        {
            if (Input.touchCount == 2)
            {            // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
                if (Camera.main.orthographic)
                {
                    Camera.main.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                    Camera.main.orthographicSize = Mathf.Max(GetComponent<Camera>().orthographicSize, 0.1f);
                }
                else
                {
                    Camera.main.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
                    if (Camera.main.fieldOfView < 2 || Camera.main.fieldOfView > 60)
                    {
                        Camera.main.fieldOfView -= deltaMagnitudeDiff * perspectiveZoomSpeed;
                    }
                }
            }
        }
    }

    private void RatateForAndriodAndIos()
    {
        if (!isPaused && !ButtonClickScript.isSliderValueChanging)
        {
            if (!_guiPanels.Buttonpanel.transform.GetChild(2).gameObject.GetComponent<Toggle>().isOn)
            {
                if (Input.touchCount == 2)
                {
                    float movementoneX = Input.touches[0].deltaPosition.x;
                    float movementoneY = Input.touches[0].deltaPosition.y;
                    float movementtwoX = Input.touches[1].deltaPosition.x;
                    float movementtwoY = Input.touches[1].deltaPosition.y;

                    if (Mathf.Abs(movementoneX) > Mathf.Abs(movementoneY) && Mathf.Abs(movementtwoX) > Mathf.Abs(movementtwoY))
                    {
                        transform.Rotate(0f, -(movementoneX + movementtwoX) / 2 * _rotationSpeed, 0f);
                    }
                    else
                    {
                        transform.RotateAround(cube.transform.position, Vector3.left, -(movementoneY + movementtwoY) / 2 * _rotationSpeed);
                    }
                    _newline.updateall();
                }
            }
        }
    }

    /// <summary>
    /// method for moving the body in standalone pc by long press the mouse left click
    /// </summary>
    private void MoveForStandAlone()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            _offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
            temps = Time.time;
        }
        if (Input.GetMouseButton(0) && (Time.time - temps) > 0.2)
        {
            if (!ButtonClickScript.IsMouseoverUIControl)
            {
                if (!ChangeNeedleposition.isneedlemoving)
                {
                    if (!ButtonClickScript.isSliderValueChanging)
                    {
                        if (!isPaused)
                        {
                            HandGrip.SetActive(true);
                            Cursor.visible = false;
                            HandGrip.transform.position = Input.mousePosition;
                            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
                            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset;
                            transform.position = curPosition;
                            _newline.updateall();
                        }
                    }
                }
            }
        }
        else
        {
            HandGrip.SetActive(false);
            Cursor.visible = true;
        }
    }
    /// <summary>
    /// ZoomIn And ZoomOut For StandALone pc by scrolling the mouse scroll
    /// </summary>
    private void ZoomInAndZoomOutForStandALone()
    {
        if (!isPaused)
        {
            Camera.main.fieldOfView -= (Input.GetAxis("Mouse ScrollWheel") * _zoomSpeed);
            if (Camera.main.fieldOfView < 2 || Camera.main.fieldOfView > 60)
            {
                Camera.main.fieldOfView += (Input.GetAxis("Mouse ScrollWheel") * _zoomSpeed);
            }
        }
    }
    /// <summary>
    /// method for roatating the body by mouse scroll click
    /// </summary>
    private void RotateBodyForStandAlone()
    {
     if (Input.GetMouseButton(2))
        {
            if (!isPaused)
            {
                if (Mathf.Abs(Input.GetAxis("Mouse X")) > Mathf.Abs(Input.GetAxis("Mouse Y")))
                {
                    transform.Rotate(0f, -Input.GetAxis("Mouse X") * _rotationSpeed, 0f);
                }
                else
                {
                    transform.RotateAround(cube.transform.position, Vector3.left, -Input.GetAxis("Mouse Y") * _rotationSpeed);
                }
                _newline.updateall();
            }
        }   
    }
    /// <summary>
    /// Show the confirmation panel on delete button click 
    /// </summary>
    public void ShowConfirmationPanel()
    {
        isPaused = true;
        if (SelectedNeedle != null)
        {
            _guiPanels.ConfirmationPanel.SetActive(true);
            StartCoroutine("DeleteResultConfirmation");
            string s = SelectedNeedle.GetComponent<NeedleId>().Name;
            ShowNeedleNameScript Needlelable = SelectedNeedle.GetComponent<ShowNeedleNameScript>();
            if (Needlelable != null)
            {
                DestroyImmediate(Needlelable.Label);
                Needlelable.instanciated = false;
            }
            _guiPanels.ConfirmationPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "Do you really want to delete Needle " + s;
            s = string.Empty;
        }
        else
        {
            _showPanelsScript.ShowErrorPanel("Please select a needle to delete", "Information");
        }
    }
    /// <summary>
    ///Setting the  IsMouseoverUIControl to false when a dialog box or panel disappears from under the mouse
    ///like clicking in Ok button 
    ///so that we can know the there is no UI control under the mouse and making the insert needle activated
    /// </summary>
    /// <returns></returns>
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.2f);
        ButtonClickScript.IsMouseoverUIControl = false;
    }

    public static bool ConfirmationYes = false;
    /// <summary>
    /// disappear the confirmation panel on confirmation
    /// </summary>
    public void DeleteConfirmed()
    {
        DisappearConfirmationPanelAfterGettinginput(true);
    }
    /// <summary>
    ///disappear the confirmation panel and Cancel the deletion
    /// </summary>
    public void Cancel()
    {
        DisappearConfirmationPanelAfterGettinginput(false);
    }
    private void DisappearConfirmationPanelAfterGettinginput(bool state)
    {
        ConfirmationYes = state;
        _guiPanels.ConfirmationPanel.SetActive(false);
        isPaused = false;
        GameObject.Find("Scriptprefab").GetComponent<ButtonClickScript>().StartCoroutine("Wait");
    }
    /// <summary>
    /// Wait untill user presses button on confiramtion panel and delete the needle on confirmation
    /// </summary>
    /// <returns></returns>
    IEnumerator DeleteResultConfirmation()
    {
        while (_guiPanels.ConfirmationPanel.activeInHierarchy)
        {
            yield return null;
        }
        if (ConfirmationYes)
        {
            DeleteNeedle();
        }
    }
    /// <summary>
    /// delete selected needle
    /// </summary>
    public void DeleteNeedle()
    {
        if (SelectedNeedle != null)
        {
            if (SelectedNeedle.transform.tag == CommonTypes.PrefabTag.needle.ToString())
            {
                GetAndAssignComponent();
                int needleId = SelectedNeedle.transform.gameObject.GetComponent<NeedleId>().Id;
                ChangeNeedleposition changescript = SelectedNeedle.transform.gameObject.GetComponent<ChangeNeedleposition>();
                ShowNeedleNameScript label = SelectedNeedle.transform.gameObject.GetComponent<ShowNeedleNameScript>();
                int objectindex = Newline.LinesNeedles[changescript.LineName].IndexOf(SelectedNeedle.transform.gameObject);
                DeleteHelpingPointAndLines(changescript, objectindex);
                DestroyLabelForNeedleName(label);
                _needleOperationScript.DeleteNeedle(needleId);
                SelectedNeedle.transform.parent = null;
                _newline.RefreshNeedles();
                SelectedNeedle.GetComponent<ChangeNeedleposition>().NameLabel.transform.SetParent(null);
                DestroyImmediate(SelectedNeedle.GetComponent<ChangeNeedleposition>().NameLabel);
                Destroy(SelectedNeedle.transform.gameObject);
                AssignPreviousNeedleOrRemoveLineNameFromCollection(changescript);
                SelectedNeedle = null;
                selectedlabel.GetComponent<Text>().text = "No Needle Selected";
            }
        }
        ConfirmationYes = false;
    }
    /// <summary>
    /// If Lineneedle collection contains the Line name and there is no needle in the given line name then set the previous needle to null else assign the previous needle
    /// </summary>
    /// <param name="changescript"></param>
    private static void AssignPreviousNeedleOrRemoveLineNameFromCollection(ChangeNeedleposition changescript)
    {
        if (Newline.LinesNeedles.ContainsKey(changescript.LineName))
        {
            if (Newline.LinesNeedles[changescript.LineName].Count == 0)
            {
                Camera.main.GetComponentInChildren<NeedleScript>()._previousneedle = null;
                Newline.MeridianNames.Remove(changescript.LineName);
            }
            else
            {
                Camera.main.GetComponentInChildren<NeedleScript>()._previousneedle = Newline.LinesNeedles[changescript.LineName][Newline.LinesNeedles[changescript.LineName].Count - 1];
            }
        }
        else
        {
            Camera.main.GetComponentInChildren<NeedleScript>()._previousneedle = null;
            Newline.selectedLineName = string.Empty;
            GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "No Meridian Selected Scroll Click on Needle to select";
        }
    }
    /// <summary>
    /// Delete the helping points on the basis of index of needle in the line
    /// if needle is first in the whole merdian then delete the next two helping points
    /// if needle is last in the whole merdian then delete the previous two helping points
    /// else delete one before the needle and one after the needle and recreate the new line by connecting the other sides needle and helping point of deleted needle
    /// </summary>
    /// <param name="changescript"></param>
    /// <param name="objectindex"></param>
    private void DeleteHelpingPointAndLines(ChangeNeedleposition changescript, int objectindex)
    {
        if (objectindex == 0)
        {
            if (Newline.LinesNeedles[changescript.LineName].Count > 3)
            {
                int LineNumberInMeridian = (int)(changescript.objectnumber / 3);
                DestroyPoint_Meridian(changescript.LineName, objectindex + 1, objectindex + 2, LineNumberInMeridian);
            }
        }
        else if (objectindex == Newline.LinesNeedles[changescript.LineName].Count - 1)
        {
            if (Newline.LinesNeedles[changescript.LineName].Count >= 4)
            {
                int LineNumberInMeridian = (int)((changescript.objectnumber / 3) - 1);
                DestroyPoint_Meridian(changescript.LineName, objectindex - 1, objectindex - 2, LineNumberInMeridian);
            }
        }
        else
        {
            Destroypoint(objectindex + 1, objectindex - 1, changescript.LineName);
            DestroyLine(changescript.LineName, (int)(changescript.objectnumber / 3));
            DestroyLine(changescript.LineName, (int)(changescript.objectnumber / 3) - 1);
            CreateNewLine(changescript, objectindex);
            RefreshAndUpdateLines();
        }
    }
    /// <summary>
    /// destroy the label appeard for displaying name of needle while selecting the needle
    /// </summary>
    /// <param name="labelScript"></param>
    private static void DestroyLabelForNeedleName(ShowNeedleNameScript labelScript)
    {
        if (labelScript != null)
        {
            Destroy(labelScript.Label);
            Destroy(labelScript);
        }
    }
    /// <summary>
    /// Destroy the line and remove the line index from the all curveline collection
    /// </summary>
    /// <param name="LineName">Name of line</param>
    /// <param name="LineNumberInMerdian">index of line in the collection</param>
    private static void DestroyLine(string LineName, int LineNumberInMerdian)
    {
        VectorLine line1 = Newline.AllCurveLines[LineName][LineNumberInMerdian];
        VectorLine.Destroy(ref line1);
        Newline.AllCurveLines[LineName].RemoveAt(LineNumberInMerdian);
    }
    /// <summary>
    /// create the new line by connecting the other sides needle and helping point of deleted needle
    /// </summary>
    /// <param name="changescript"></param>
    /// <param name="objectindex"></param>
    private void CreateNewLine(ChangeNeedleposition changescript, int objectindex)
    {
        VectorLine line = new VectorLine(changescript.LineName, new Vector3[segment + 1], null, 1.2f, LineType.Continuous, Joins.Weld);
        line.drawTransform = gameObject.transform;
        List<GameObject> obj = Newline.LinesNeedles[changescript.LineName];
        Color c = Newline.GetColor(changescript.color);
        line.SetColor(c);
        line.MakeCurve(obj[objectindex - 3].transform.localPosition, obj[objectindex - 2].transform.localPosition, obj[objectindex + 2].transform.localPosition, obj[objectindex + 3].transform.localPosition, segment);
        line.Draw3D();
        Newline.AllCurveLines[changescript.LineName].Insert((int)(changescript.objectnumber / 3) - 1, line);
    }
    /// <summary>
    /// destory the helping point and merdian and refresh the line and update all lines
    /// </summary>
    /// <param name="LineName">name of the line </param>
    /// <param name="Firstindex">index of first helping point</param>
    /// <param name="secondindex">index of second helping point</param>
    /// <param name="LineNumberInMeridian">Line index in the line collection</param>
    private void DestroyPoint_Meridian(string LineName, int Firstindex, int secondindex, int LineNumberInMeridian)
    {
        Destroypoint(Firstindex, secondindex, LineName);
        DestroyLine(LineName, LineNumberInMeridian);
        RefreshAndUpdateLines();
    }
    /// <summary>
    ///Refresh and update the lines 
    /// </summary>
    private void RefreshAndUpdateLines()
    {
        _newline.RefreshNeedles();
        _newline.updateall();
    }
}
