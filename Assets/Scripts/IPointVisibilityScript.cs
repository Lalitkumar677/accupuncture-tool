﻿using System;

interface IPointVisibilityScript
{
    /// <summary>
    /// Add the patient
    /// </summary>
    void AddPatient();
    /// <summary>
    /// perform the female radio button functioality in add profile
    /// </summary>
    void AddprofileFemaleSelect();
    /// <summary>
    ///  perform the male radio button functioality in add profile
    /// </summary>
    void AddprofileMaleSelect();
    
    /// <summary>
    /// creates the row for needles in show history for a session
    /// </summary>
    /// <param name="session">Id of session</param>
    void CreateNeedlesInScrollViewforSession(Session session);
    /// <summary>
    /// perform the female radio button functionality in edit profile
    /// </summary>
    void EditprofileFemaleSelect();
    /// <summary>
    /// perform the male radio button functionality in edit profile
    /// </summary>
    void EditprofileMaleSelect();
    /// <summary>
    /// insert the needles in the body for master data, show history and add session
    /// </summary>
    void InsertNeedlesInBody();
    /// <summary>
    /// loads the needles for a perticular session
    /// </summary>
    /// <param name="sessionId">Id of session</param>
    void LoadSessionNeedles(int sessionId);
    /// <summary>
    /// loads all session
    /// </summary>
    void loadSessions();
    /// <summary>
    ///  creates the new meridian
    /// </summary>
    void NewMeridian();
    /// <summary>
    /// selects the next session in show history
    /// </summary>
    void NextSession();
    /// <summary>
    /// hides the messsage dialog box and start the wait ienumerator
    /// </summary>
    void okbuttonclick();
    /// <summary>
    ///  selects the previous session in show history
    /// </summary>
    void PreviousSession();
    /// <summary>
    /// reloads the show history panel
    /// </summary>
    void RefreshHistoryPanel();
    /// <summary>
    /// Reset the fields of add profile panel
    /// </summary>
    void resetaddpatientfield();
    /// <summary>
    /// Resets the fields of edit profile panel
    /// </summary>
    void reseteditpatientfield();
    /// <summary>
    /// Saves the session
    /// </summary>
    void SaveCurrentSession();
    /// <summary>
    /// shows the calender
    /// </summary>
    /// <param name="Datepicker">Calender game object</param>
    void ShowDateTimePicker(global::UnityEngine.Object Datepicker);
    /// <summary>
    /// Displays the New meridian panel for giving the name of meridian 
    /// </summary>
    void showNewMeridianPanel();
    /// <summary>
    /// Makes the helping points appear and disappear
    /// </summary>
    void ShowpointButtonClick();
    /// <summary>
    /// Change the session on sliding the silder in show history
    /// </summary>
    void SlidingValueChange();
    /// <summary>
    /// changes the transparency of body
    /// </summary>
    void TransparencyChanged();
    /// <summary>
    /// updates the patient profile
    /// </summary>
    void UpdatePatient();
    /// <summary>
    /// waits for until the needles loaded from the server
    /// </summary>
    /// <returns></returns>
    global::System.Collections.IEnumerator WaitUntillNeedleLoaded();
}

