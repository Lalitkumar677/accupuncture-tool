﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NavigationHepler
{
    /// <summary>
    /// Check if the game object is UI control
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static bool isControl(GameObject obj)
    {
        return (obj.tag == CommonTypes.UIControls.UIButton.ToString() || obj.tag == CommonTypes.UIControls.UITextBox.ToString() || obj.tag == CommonTypes.UIControls.UIToggle.ToString() || obj.tag == CommonTypes.UIControls.UIGenderPanel.ToString()) ? true : false;
    }
    public static void SelectControl(GameObject parent, int i, EventSystem system, CommonTypes.Action action)
    {
        if (action == CommonTypes.Action.Forward)
        {
            i = (i >= parent.transform.childCount - 1) ? -1 : i;
            i++;
        }
        else
        {
            i = (i <= 0) ? parent.transform.childCount : i;
            i--;
        }
        CheckAndSelectControl(parent, i, system, action);
    }
    private static void CheckAndSelectControl(GameObject parent, int index, EventSystem system, CommonTypes.Action action)
    {
        if (isControl(parent.transform.GetChild(index).gameObject))
        {
            system.SetSelectedGameObject(parent.transform.GetChild(index).gameObject, new BaseEventData(system));
        }
        else
        {
            SelectControl(parent, index, system, action);
        }
    }
    public static void HitEnterOnControl(EventSystem system)
    {
        if (system.currentSelectedGameObject != null)
        {
            switch (system.currentSelectedGameObject.tag)
            {
                case "UIToggle":
                    {
                        system.currentSelectedGameObject.GetComponent<Toggle>().isOn = (!system.currentSelectedGameObject.GetComponent<Toggle>().isOn);
                        break;
                    }
            }
        }
    }
    public static void ChangeColorOFNewSelectedControl(EventSystem system, CommonTypes.Action action)
    {
        switch (system.currentSelectedGameObject.tag)
        {
            case "UIButton":
                {
                    ChangeCurrentSelectedButtonColortoselectedColor(system);
                    break;
                }
            case "UIToggle":
                {
                    system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
                    break;
                }
            case "UIGenderPanel":
                {
                    if (action == CommonTypes.Action.Forward)
                    { SetColor_NewSelection_UIGenderPanel_FORWARD(system); }
                    else
                    { SetColor_NewSelection_UIGenderPanel_BACKWARD(system); }
                    break;
                }
        }
    }

    private static void SetColor_NewSelection_UIGenderPanel_FORWARD(EventSystem system)
    {
        if (system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.white && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white)
        {
            SelectControl(system.currentSelectedGameObject, -1, system, CommonTypes.Action.Forward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else if (system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.gray && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white)
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            SelectControl(system.currentSelectedGameObject, 0, system, CommonTypes.Action.Forward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color = Color.white;
        }
    }
    private static void SetColor_NewSelection_UIGenderPanel_BACKWARD(EventSystem system)
    {
        if (system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.white && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white)
        {
            SelectControl(system.currentSelectedGameObject, -1, system, CommonTypes.Action.Backward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else if (system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.gray && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white)
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            SelectControl(system.currentSelectedGameObject, 0, system, CommonTypes.Action.Backward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color = Color.white;
        }
    }

    private static void ChangeCurrentSelectedButtonColortoselectedColor(EventSystem system)
    {
        Color32 color = system.currentSelectedGameObject.GetComponent<Image>().color;
        byte b = 175;
        system.currentSelectedGameObject.GetComponent<Image>().color = new Color32(color.r, color.g, color.b, b);
    }
    public static void ChangeUIColorTODefaultAndSelectControl(GameObject parent, int i, GameObject child, CommonTypes.Action action, EventSystem system)
    {
        switch (child.tag)
        {
            case "UITextBox":
                {
                    NavigationHepler.SelectControl(parent, i, system, action);
                    break;
                }
            case "UIButton":
                {
                    ChangeCurrentSelectedButtonColortoDefault(system);
                    NavigationHepler.SelectControl(parent, i, system, action);
                    break;
                }
            case "UIToggle":
                {
                    SelectControlInUIToggle(parent, i, action, system);
                    break;
                }
            case "UIGenderPanel":
                {
                    if (action == CommonTypes.Action.Forward)
                    { SelectNextControlInUIGenderPanel(parent, i, system); }
                    else
                    { SelectPreviousControlInUIGenderPanel(parent, i, system); }
                    break;
                }
        }
    }
    private static void SelectControlInUIToggle(GameObject parent, int i, CommonTypes.Action action, EventSystem system)
    {
        if (system.currentSelectedGameObject.transform.parent.gameObject.tag == CommonTypes.UIControls.UIGenderPanel.ToString())
        {
            system.SetSelectedGameObject(system.currentSelectedGameObject.transform.parent.gameObject, new BaseEventData(system));
        }
        else
        {
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.white;
            SelectControl(parent, i, system, action);
        }
    }

    private static void SelectNextControlInUIGenderPanel(GameObject parent, int i, EventSystem system)
    {
        if (system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.white && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white && system.currentSelectedGameObject.transform.GetChild(0).GetComponent<Toggle>().isOn)
        {
            SelectControl(system.currentSelectedGameObject, -1, system, CommonTypes.Action.Forward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else if (system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.gray && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white)
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            SelectControl(system.currentSelectedGameObject, 0, system, CommonTypes.Action.Forward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color = Color.white;
            SelectControl(parent, i, system, CommonTypes.Action.Forward);
        }
    }

    private static void ChangeCurrentSelectedButtonColortoDefault(EventSystem system)
    {
        Color32 color = system.currentSelectedGameObject.GetComponent<Image>().color;
        byte b = 255;
        system.currentSelectedGameObject.GetComponent<Image>().color = new Color32(color.r, color.g, color.b, b);
    }
    private static void SelectPreviousControlInUIGenderPanel(GameObject parent, int i, EventSystem system)
    {
        if ((system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.gray || system.currentSelectedGameObject.transform.GetChild(0).GetComponent<Toggle>().isOn) && system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white)
        {
            system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color = Color.white;
            system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color = Color.white;
            NavigationHepler.SelectControl(parent, i, system, CommonTypes.Action.Backward);
        }
        else if (system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.white && system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.white)
        {
            NavigationHepler.SelectControl(system.currentSelectedGameObject, 2, system, CommonTypes.Action.Backward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
        else if (system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color == Color.gray && system.currentSelectedGameObject.transform.GetChild(0).GetComponentInChildren<Text>().color == Color.white)
        {
            system.currentSelectedGameObject.transform.GetChild(1).GetComponentInChildren<Text>().color = Color.white;
            NavigationHepler.SelectControl(system.currentSelectedGameObject, 1, system, CommonTypes.Action.Backward);
            system.currentSelectedGameObject.GetComponentInChildren<Text>().color = Color.grey;
        }
    }

}
