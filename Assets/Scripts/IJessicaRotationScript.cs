﻿using System;
interface IJessicaRotationScript
{
    /// <summary>
    /// disappears the confirmation panel
    /// </summary>
    void Cancel();
    /// <summary>
    /// disappears the confirmation panel
    /// calls the delete function
    /// </summary>
    void DeleteConfirmed();
    /// <summary>
    /// deletes the needle and points related to it
    /// </summary>
    void DeleteNeedle();
    /// <summary>
    /// Show the confirmation dialogbox on delete needle button clicked
    /// </summary>
    void ShowConfirmationPanel();
}

