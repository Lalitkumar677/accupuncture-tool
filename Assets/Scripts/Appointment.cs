﻿using UnityEngine;
using System.Collections;
using System;

public class Appointment
{
    public int AppointmentID { get; set; }
    public string ClientName { get; set; }
    public TimeSpan StartTime { get; set; }
    public TimeSpan EndTime { get; set; }
    public DateTime AppointmentDate { get; set; }
    public int DoctorID { get; set; }
}
