﻿using UnityEngine;
using System.Collections;

public class Payment
{
    public int PaymentId { get; set; }
    public string BillsendDate { get; set; }
    public int Paid { get; set; }
    public int Amount { get; set; }
    public string PaymentDate { get; set; }
    public int SessionId { get; set; }
    public int PatientID { get; set; }
    public int DoctorId { get; set; }
    public int Sent { get; set; }
}
