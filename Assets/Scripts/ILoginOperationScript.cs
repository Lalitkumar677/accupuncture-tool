﻿using System;

    interface ILoginOperationScript
    {
        /// <summary>
        /// Add the doctor
        /// </summary>
        void AddDoctor();
        /// <summary>
        /// Authenticates the Doctor credentials
        /// </summary>
        void Authenticate();
        /// <summary>
        /// perform the female radio button functioality in doctor panel
        /// </summary>
        void FemaleSelect();
        /// <summary>
        /// perform the male radio button functioality in doctor panel
        /// </summary>
        void MaleSelect();
    }

