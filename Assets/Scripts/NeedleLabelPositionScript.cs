﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NeedleLabelPositionScript : MonoBehaviour
{
    public GameObject referencedNeedle;
    // Use this for initialization
    void Start()
    {
       
    }
    // Update is called once per frame
    /// <summary>
    /// set the label position with needle
    /// </summary>
    void Update()
    {
        if (referencedNeedle != null)
        {
            gameObject.transform.position = new Vector3(referencedNeedle.transform.position.x, referencedNeedle.transform.position.y, referencedNeedle.transform.position.z - 0.2f);
        }
    }
}
