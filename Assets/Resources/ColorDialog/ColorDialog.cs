﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ColorDialog : MonoBehaviour
{
    // Use this for initialization
    GameObject Red;
    GameObject Black;
    GameObject Blue;
    GameObject Cyan;
    GameObject Gray;
    GameObject Green;
    GameObject Magenta;
    GameObject Yellow;
    GameObject White;
    public static Color seletedcolor;
    GameObject MainPreview;

    void Start()
    {
        seletedcolor = Color.red;
        Red = transform.GetChild(1).GetChild(0).GetChild(0).gameObject;
        Black = transform.GetChild(1).GetChild(0).GetChild(1).gameObject;
        Blue = transform.GetChild(1).GetChild(0).GetChild(2).gameObject;
        Cyan = transform.GetChild(1).GetChild(0).GetChild(3).gameObject;
        Gray = transform.GetChild(1).GetChild(0).GetChild(4).gameObject;
        Green = transform.GetChild(1).GetChild(0).GetChild(5).gameObject;
        Magenta = transform.GetChild(1).GetChild(0).GetChild(6).gameObject;
        Yellow = transform.GetChild(1).GetChild(0).GetChild(7).gameObject;
        White = transform.GetChild(1).GetChild(0).GetChild(8).gameObject;

        Red.GetComponent<Image>().color = Color.red;
        Black.GetComponent<Image>().color = Color.black;
        Blue.GetComponent<Image>().color = Color.blue;
        Cyan.GetComponent<Image>().color = Color.cyan;
        Gray.GetComponent<Image>().color = Color.gray;
        Green.GetComponent<Image>().color = Color.green;
        Magenta.GetComponent<Image>().color = Color.magenta;
        Yellow.GetComponent<Image>().color = Color.yellow;
        White.GetComponent<Image>().color = Color.white;
        MainPreview = transform.GetChild(1).GetChild(1).GetChild(3).gameObject;
        MainPreview.transform.GetComponent<Image>().color = seletedcolor;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void TextChangedRed(string text)
    {
        int number;
        if (string.IsNullOrEmpty(text))
        {
            number = 0;
            transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value = number;
            transform.GetChild(2).GetChild(0).GetChild(8).GetComponent<InputField>().text = "0";
        }

        bool a = int.TryParse(text, out number);
        if (a)
        {
            if (number > 255)
            {
                transform.GetChild(2).GetChild(0).GetChild(8).GetComponent<InputField>().text = "255";
                number = 255;
            }
            transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value = number;
        }


    }
    public void TextChangedGreen(string text)
    {
        int number;

        if (string.IsNullOrEmpty(text))
        {
            number = 0;
            transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value = number;
            transform.GetChild(2).GetChild(0).GetChild(9).GetComponent<InputField>().text = "0";
        }
        bool a = int.TryParse(text, out number);
        if (a)
        {
            if (number > 255)
            {
                transform.GetChild(2).GetChild(0).GetChild(9).GetComponent<InputField>().text = "255";
                number = 255;
            }
            transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value = number;
        }

    }
    public void TextChangedBlue(string text)
    {
        int number;
        if (string.IsNullOrEmpty(text))
        {
            number = 0;
            transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value = number;
            transform.GetChild(2).GetChild(0).GetChild(10).GetComponent<InputField>().text = "0";
        }
        bool a = int.TryParse(text, out number);
        if (a)
        {
            if (number > 255)
            {
                transform.GetChild(2).GetChild(0).GetChild(10).GetComponent<InputField>().text = "255";
                number = 255;
            }
            transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value = number;
        }

    }
    public void Cancel()
    {
        transform.gameObject.SetActive(false);
        GameObject.Find("Scriptprefab").GetComponent<ButtonClickScript>().StartCoroutine("Wait");
    }

    public void TextChangedAlpha(string text)
    {
        int number;
        if (string.IsNullOrEmpty(text))
        {
            number = 0;
            transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value = number;
            transform.GetChild(2).GetChild(0).GetChild(11).GetComponent<InputField>().text = "0";
        }
        bool a = int.TryParse(text, out number);
        if (a)
        {
            if (number > 255)
            {
                transform.GetChild(2).GetChild(0).GetChild(11).GetComponent<InputField>().text = "255";
                number = 255;
            }
            transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value = number;
        }
    }

    public void sliderRed()
    {
        transform.GetChild(2).GetChild(0).GetChild(8).GetComponent<InputField>().text = transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value.ToString();
        byte red = (byte)transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value;
        byte green = (byte)transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value;
        byte blue = (byte)transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value;
        byte alpha = (byte)transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value;
        transform.GetChild(2).GetChild(1).GetChild(2).GetComponent<Image>().color = new Color32(red, green, blue, alpha);
    }
    public void sliderGreen()
    {
        transform.GetChild(2).GetChild(0).GetChild(9).GetComponent<InputField>().text = transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value.ToString();
        byte red = (byte)transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value;
        byte green = (byte)transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value;
        byte blue = (byte)transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value;
        byte alpha = (byte)transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value;
        transform.GetChild(2).GetChild(1).GetChild(2).GetComponent<Image>().color = new Color32(red, green, blue, alpha);


    }
    public void sliderBlue()
    {
        transform.GetChild(2).GetChild(0).GetChild(10).GetComponent<InputField>().text = transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value.ToString();
        byte red = (byte)transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value;
        byte green = (byte)transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value;
        byte blue = (byte)transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value;
        byte alpha = (byte)transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value;
        transform.GetChild(2).GetChild(1).GetChild(2).GetComponent<Image>().color = new Color32(red, green, blue, alpha);


    }
    public void sliderAlpha()
    {
        transform.GetChild(2).GetChild(0).GetChild(11).GetComponent<InputField>().text = transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value.ToString();
        byte red = (byte)transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value;
        byte green = (byte)transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value;
        byte blue = (byte)transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value;
        byte alpha = (byte)transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value;
        transform.GetChild(2).GetChild(1).GetChild(2).GetComponent<Image>().color = new Color32(red, green, blue, alpha);

    }
    public void Selectcolor(Object button)
    {
        GameObject selectedbutton = (GameObject)button;
        seletedcolor = selectedbutton.transform.GetComponent<Image>().color;
        MainPreview.transform.GetComponent<Image>().color = seletedcolor;

    }

    public void ok()
    {
        seletedcolor = transform.GetChild(2).GetChild(1).GetChild(2).GetComponent<Image>().color;
        MainPreview.transform.GetComponent<Image>().color = seletedcolor;
        showcolors();
    }
    public void Done()
    {
        seletedcolor = MainPreview.transform.GetComponent<Image>().color;
        Newline script = GameObject.Find("Jessica").GetComponent<Newline>();
        script.changecolor(seletedcolor);
        Cancel();
    }
    public void showcoustomcolors()
    {
        transform.GetChild(1).gameObject.SetActive(false);
        transform.GetChild(2).gameObject.SetActive(true);
        TranslationScript.TranslateColorBox();
        transform.GetChild(2).GetChild(1).GetChild(2).GetComponent<Image>().color = seletedcolor;
        Color32 c = seletedcolor;
        transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Slider>().value = c.r;
        transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Slider>().value = c.g;
        transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Slider>().value = c.b;
        transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Slider>().value = c.a;

    }
    public void showcolors()
    {
        transform.GetChild(2).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(true);
        TranslationScript.TranslateColorBox();
        MainPreview.transform.GetComponent<Image>().color = seletedcolor;
    }

}
