﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using System;
using UnityEngine.UI;

public class PaymentDetailScript : MonoBehaviour, IPaymentDetailScript
{
    private ServiceUrlscript _serviceUrlscript;
    private GuiPanels _guiPanels;
    private ShowPanelsScript _showPanelsScript;
    public static List<Payment> billsenddetails = new List<Payment>();
    public static List<Payment> Paymentdetails = new List<Payment>();
    public static List<Payment> paymentsRecords = new List<Payment>();
    public GameObject SendBillRowPrefab;
    public GameObject PaymentBillRowPrefab;
    public Sprite red;
    public Sprite green;
    //    private static readonly JCsLogger log = new JCsLogger(typeof(PaymentDetailScript));
    // Use this for initialization
    void Start()
    {
        _serviceUrlscript = new ServiceUrlscript();
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        PaymentBillRowPrefab = Resources.Load("Prefabs/PaymentBillRow") as GameObject;
        SendBillRowPrefab = Resources.Load("Prefabs/SendBillRow") as GameObject;
    }
    // Update is called once per frame
    void Update()
    {
    }
    public void LoadDoctorAllpayment()
    {
        StartCoroutine("LoadpaymentDetail");
    }
    private IEnumerator LoadpaymentDetail()
    {
        int DoctorID = Camera.main.GetComponent<UserIdScript>().DoctorID;
        string url = _serviceUrlscript.LoadPayments + DoctorID;
        _showPanelsScript.ShowMessage(Language.Get("WORD_Loading") + "...");
        WWW www = new WWW(url);
        yield return StartCoroutine("LoadpaymentDetails", www);
        SortbyshowhistorySendbill();
    }
    private IEnumerator LoadpaymentDetails(WWW www)
    {
        yield return www;
        _showPanelsScript.HideMessageDialogbox();
        paymentsRecords.Clear();
        if (www.error == null)
        {
            JSONArray json = JSONArray.Parse(www.text);
            if (json.Length > 0)
            {
                for (int i = 0; i < json.Length; i++)
                {
                    Payment payment = new Payment();
                    payment.PaymentId = Convert.ToInt32(json[i].Obj["PaymentID"].Number);
                    payment.BillsendDate = json[i].Obj["BillSentDate"].Str;
                    payment.Paid = Convert.ToInt32(json[i].Obj["Paid"].Number);
                    payment.PatientID = Convert.ToInt32(json[i].Obj["patientId"].Number);
                    payment.SessionId = Convert.ToInt32(json[i].Obj["sessioniId"].Number);
                    payment.PaymentDate = json[i].Obj["PaymentDate"].Str;
                    payment.Amount = Convert.ToInt32(json[i].Obj["Amount"].Number);
                    payment.DoctorId = Convert.ToInt32(json[i].Obj["DoctorID"].Number);
                    payment.Sent = Convert.ToInt32(json[i].Obj["sent"].Number);
                    paymentsRecords.Add(payment);
                }
            }
        }
        else
        {
            LogScript.Log(www);
            _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
        }
    }
    public void SortbyshowhistorySendbill()
    {
        billsenddetails = _guiPanels.FinancePanel.transform.GetChild(2).GetChild(4).GetComponent<Toggle>().isOn ? paymentsRecords : paymentsRecords.FindAll(p => p.Sent == 0);
        DestroyOldItemAndGenerateNew(_guiPanels.SentbillContentPanel, billsenddetails, CommonTypes.FinanceAction.Billsent);
    }
    public void SortbyshowhistoryPayment()
    {
        Paymentdetails = _guiPanels.FinancePanel.transform.GetChild(3).GetChild(4).GetComponent<Toggle>().isOn ? paymentsRecords.FindAll(p => p.Sent == 1) : paymentsRecords.FindAll(p => p.Sent == 1 && p.Paid == 0);
        DestroyOldItemAndGenerateNew(_guiPanels.PaymentContentPanel, Paymentdetails, CommonTypes.FinanceAction.Payment);
    }
    public void FilterbillRecords()
    {
        Filter(2, _guiPanels.SentbillContentPanel, billsenddetails, CommonTypes.FinanceAction.Billsent);
    }
    public void FilterpaymentRecords()
    {
        Filter(3, _guiPanels.PaymentContentPanel, Paymentdetails, CommonTypes.FinanceAction.Payment);
    }
    private void Filter(int childindex, GameObject ContentPanel, List<Payment> detail, CommonTypes.FinanceAction action)
    {
        string text = _guiPanels.FinancePanel.transform.GetChild(childindex).GetChild(1).GetComponent<InputField>().text;
        DestroyOldItemAndGenerateNew(ContentPanel, detail, action, text);
    }

    private void DestroyOldItemAndGenerateNew(GameObject ContentPanel, List<Payment> detail, CommonTypes.FinanceAction action, string text = "")
    {
        for (int i = ContentPanel.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(ContentPanel.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < detail.Count; i++)
        {
            DropDownScript DropDown = _guiPanels.patientselectionpanel.transform.GetChild(2).GetComponent<DropDownScript>();
            Patient patient = DropDown.DropDownItem.Find(p => p.ID == detail[i].PatientID);
            int index = DropDown.DropDownItem.IndexOf(patient);
            string Name = DropDown.Getpatientfullname(index);
            if (!string.IsNullOrEmpty(text))
            {
                if (detail[i].PaymentId.ToString().Contains(text) || Name.ToLower().Contains(text.ToLower()))
                {
                    GenerateRow(i, Name, action);
                }
            }
            else
            {
                GenerateRow(i, Name, action);
            }
        }
    }

    /// <summary>
    /// Generates the row in the detail panel 
    /// </summary>
    /// <param name="i">index of the payment </param>
    /// <param name="Name">name of the patients</param>
    /// <param name="action">Action</param>
    private void GenerateRow(int i, string Name, CommonTypes.FinanceAction action)
    {
        GameObject row = (action == CommonTypes.FinanceAction.Billsent) ?
            GenaratePrefabRow(i, Name, 5, SendBillRowPrefab, _guiPanels.SentbillContentPanel, billsenddetails) :
            GenaratePrefabRow(i, Name, 4, PaymentBillRowPrefab, _guiPanels.PaymentContentPanel, Paymentdetails);
        if (action == CommonTypes.FinanceAction.Payment)
        {
            row.transform.GetChild(5).GetComponent<Text>().text = Paymentdetails[i].BillsendDate.Replace("T", "\n");
            ShowStatusByGreenOrRedCheck(Paymentdetails[i].Paid, i, row, 3, 6, Paymentdetails[i].PaymentDate);
        }
        else
        {
            ShowStatusByGreenOrRedCheck(billsenddetails[i].Sent, i, row, 3, 4, billsenddetails[i].BillsendDate);
        }
    }

    private void ShowStatusByGreenOrRedCheck(int detailStatus, int i, GameObject row, int pdfIconIndex, int checkIconIndex, string PayOrSendDate)
    {
        if (detailStatus == 1)
        {
            row.transform.GetChild(pdfIconIndex).GetComponent<Button>().interactable = false;
            row.transform.GetChild(checkIconIndex).GetComponent<Image>().sprite = green;
            row.transform.GetChild(checkIconIndex).GetChild(1).GetComponent<Text>().text = PayOrSendDate.Replace("T", "\n");
        }
        else
        {
            row.transform.GetChild(checkIconIndex).GetComponent<Image>().sprite = red;
            row.transform.GetChild(checkIconIndex).GetChild(1).GetComponent<Text>().text = string.Empty;
        }
    }

    private static GameObject GenaratePrefabRow(int i, string Name, int IdChildIndex, GameObject prefabtoGenerate, GameObject parent, List<Payment> details)
    {
        GameObject row = Instantiate(prefabtoGenerate) as GameObject;
        row.transform.SetParent(parent.transform);
        row.transform.localScale = new Vector3(1f, 1f, 1f);
        row.transform.GetChild(0).GetComponent<Text>().text = Name;
        row.transform.GetChild(IdChildIndex).GetComponent<Text>().text = details[i].PaymentId.ToString();
        string sessionDate = string.Empty;
        if (!string.IsNullOrEmpty(PointVisibilityScript.AllPatientsessions.Find(p => p.sessionID == details[i].SessionId).SessionBegin))
        {
            sessionDate = PointVisibilityScript.AllPatientsessions.Find(p => p.sessionID == details[i].SessionId).SessionBegin.ToString();
        }
        row.transform.GetChild(1).GetComponent<Text>().text = sessionDate.Substring(0, sessionDate.IndexOf('T'));
        row.transform.GetChild(2).GetComponent<Text>().text = details[i].Amount.ToString();
        return row;
    }

    public IEnumerator UpdatePayment(Payment payment)
    {
        WWWForm form = new WWWForm();
        form.AddField("PaymentID", payment.PaymentId.ToString());
        if (!string.IsNullOrEmpty(payment.BillsendDate))
        {
            form.AddField("BillSentDate", payment.BillsendDate.ToString());
        }
        if (!string.IsNullOrEmpty(payment.PaymentDate))
        {
            form.AddField("PaymentDate", payment.PaymentDate.ToString());
        }
        form.AddField("Paid", payment.Paid.ToString());
        form.AddField("patientId", payment.PatientID.ToString());
        form.AddField("sessioniId", payment.SessionId.ToString());
        form.AddField("Amount", payment.Amount.ToString());
        form.AddField("DoctorID", payment.DoctorId.ToString());
        form.AddField("sent", payment.Sent.ToString());
        WWW www = new WWW(_serviceUrlscript.UpdatePaymentUrl, form);
        _showPanelsScript.ShowMessage(Language.Get("WORD_Updating") + "...");
        yield return StartCoroutine("UpdatePayments", www);
    }
    /// <summary>
    /// Updates the payment record call the Ienumarator 
    /// </summary>
    /// <param name="www">url of the service</param>
    /// <returns></returns>
    private IEnumerator UpdatePayments(WWW www)
    {
        yield return www;
        _showPanelsScript.HideMessageDialogbox();
        if (www.error != null)
        {
            LogScript.Log(www);
            _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
        }
    }
}

