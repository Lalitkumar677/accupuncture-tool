﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TranslationScript : MonoBehaviour, ITranslationScript
{
    /// <summary>
    /// Selected language for the application
    /// </summary>
    public static string SelectedLanguage = "EN";
    // Use this for initialization
    void Start()
    {
        TranslateLoginPanel();
    }
    public void LanguageButton(Object Button)
    {
        GameObject button = (GameObject)Button;
        if (button.GetComponentInChildren<Text>().text == "English")
        {
            button.GetComponentInChildren<Text>().text = "Deutsch";
            SelectedLanguage = "EN";
            Language.SwitchLanguage(SelectedLanguage);
        }
        else
        {
            if (button.GetComponentInChildren<Text>().text == "Deutsch")
            {
                button.GetComponentInChildren<Text>().text = "English";
                SelectedLanguage = "DE";
                Language.SwitchLanguage(SelectedLanguage);
            }
        }
        TranslateMenupanel();
        TranslateButtonpanel();
        TranslateTreatmentpanel();
        TranslatePatientSelectionPanel();
        TranslateLoginPanel();
    }
    /// <summary>
    /// Translate 
    /// </summary>
    public static void TranslateLoginPanel()
    {
        if (GuiPanels.guiPanels.LoginPanel.activeInHierarchy)
        {
            LanguageCode curlang = Language.CurrentLanguage();
            if (!GuiPanels.guiPanels.AdminPanel.activeInHierarchy)
            {
                if (curlang == LanguageCode.EN)
                {
                    GuiPanels.guiPanels.LoginPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = "Deutsch";
                }
                if (curlang == LanguageCode.DE)
                {
                    GuiPanels.guiPanels.LoginPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = "English";
                }
            }
            if (GuiPanels.guiPanels.Userloginpanel.activeInHierarchy)
            {
                GuiPanels.guiPanels.Userloginpanel.transform.GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Enter_Doctor_Name");
                GuiPanels.guiPanels.Userloginpanel.transform.GetChild(0).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_Password");
                GuiPanels.guiPanels.Userloginpanel.transform.GetChild(0).GetChild(2).GetComponentInChildren<Text>().text = Language.Get("WORD_Login");
            }
            if (GuiPanels.guiPanels.SignUppanel.activeInHierarchy)
            {
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Enter_Doctor_Name");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_Email_ID");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Enter_Password");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(3).GetComponentInChildren<Text>().text = Language.Get("Confirm_Password");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(4).GetComponentInChildren<Text>().text = Language.Get("Enter_House_No");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(5).GetComponentInChildren<Text>().text = Language.Get("Enter_Locality");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(6).GetComponentInChildren<Text>().text = Language.Get("Enter_Town");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(7).GetComponentInChildren<Text>().text = Language.Get("Enter_Pincode");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(8).GetComponentInChildren<Text>().text = Language.Get("Enter_State");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(9).GetComponentInChildren<Text>().text = Language.Get("Enter_Per_Hour_Fees");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(10).GetComponentInChildren<Text>().text = Language.Get("Enter_ASCA");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(11).GetComponentInChildren<Text>().text = Language.Get("Enter_EMR");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(12).GetComponentInChildren<Text>().text = Language.Get("Enter_EGK");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(13).GetComponentInChildren<Text>().text = Language.Get("WORD_Male");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(14).GetComponentInChildren<Text>().text = Language.Get("WORD_Female");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(15).GetComponentInChildren<Text>().text = Language.Get("WORD_Save");
                GuiPanels.guiPanels.SignUppanel.transform.GetChild(0).GetChild(16).GetComponentInChildren<Text>().text = Language.Get("WORD_Cancel");
            }

        }
    }
    public static void TranslateAdminPanel()
    {
        if (GuiPanels.guiPanels.AdminPanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.AdminPanel.transform.GetChild(0).GetComponentInChildren<Text>().text = Language.Get("WORD_SignUp");
        }
    }
    public static void TranslateFinancePanel()
    {
        if (GuiPanels.guiPanels.FinancePanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.FinancePanel.transform.GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Bill_Status");
            GuiPanels.guiPanels.FinancePanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Payment_status");
            if (GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Search");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_Name_or_Invoice");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(4).GetComponentInChildren<Text>().text = Language.Get("Show_History");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Text>().text = Language.Get("Invoice_No");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(2).GetChild(1).GetComponent<Text>().text = Language.Get("WORD_Name");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(2).GetChild(2).GetComponent<Text>().text = Language.Get("Session_Date");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(2).GetChild(3).GetComponent<Text>().text = Language.Get("WORD_Amount");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(2).GetChild(4).GetComponent<Text>().text = Language.Get("View_Bill");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(2).GetChild(2).GetChild(5).GetComponent<Text>().text = Language.Get("Sent_Status");
            }
            if (GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Search");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_Name_or_Invoice");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(4).GetComponentInChildren<Text>().text = Language.Get("Show_History");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(0).GetComponent<Text>().text = Language.Get("Invoice_No");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(1).GetComponent<Text>().text = Language.Get("WORD_Name");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(2).GetComponent<Text>().text = Language.Get("Session_Date");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(3).GetComponent<Text>().text = Language.Get("WORD_Amount");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(4).GetComponent<Text>().text = Language.Get("Bill_File");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(5).GetComponent<Text>().text = Language.Get("Sent_Date");
                GuiPanels.guiPanels.FinancePanel.transform.GetChild(3).GetChild(2).GetChild(6).GetComponent<Text>().text = Language.Get("Payment_status");

            }

        }
    }
    public static void TranslatesessionHistoryPanel()
    {
        TranslatePatientSelectionPanel();
        TranslateButtonpanel();
        if (GuiPanels.guiPanels.sessionHistoryPanel.activeInHierarchy)
        {
            if (GuiPanels.guiPanels.sessionHistoryPanel.transform.GetChild(3).GetChild(0).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.sessionHistoryPanel.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = Language.Get("Session_Date");
            }
            if (GuiPanels.guiPanels.sessionHistoryPanel.transform.GetChild(3).GetChild(1).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.sessionHistoryPanel.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = Language.Get("Session_Duration");
            }
            if (GuiPanels.guiPanels.sessionHistoryPanel.transform.GetChild(3).GetChild(3).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.sessionHistoryPanel.transform.GetChild(3).GetChild(3).GetComponent<Text>().text = Language.Get("WORD_Needle");
            }
        }
    }
    public static void TranslateinstructionPanel()
    {
        if (GuiPanels.guiPanels.instructionPanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.instructionPanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Instructions");
        }
    }
    public static void TranslatetransparencyPanel()
    {
        if (GuiPanels.guiPanels.transparencyPanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.transparencyPanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Transparency");
        }

    }
    public static void TranslateMenupanel()
    {
        if (GuiPanels.guiPanels.Menupanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.Menupanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("WORD_MENU");
            GuiPanels.guiPanels.Menupanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Treatment_Log");
            GuiPanels.guiPanels.Menupanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("WORD_Finance");
            GuiPanels.guiPanels.Menupanel.transform.GetChild(3).GetComponentInChildren<Text>().text = Language.Get("Patient_Profile");
            GuiPanels.guiPanels.Menupanel.transform.GetChild(4).GetComponentInChildren<Text>().text = Language.Get("Master_Data");
            GuiPanels.guiPanels.Menupanel.transform.GetChild(5).GetComponentInChildren<Text>().text = Language.Get("WORD_Instructions");
        }

    }
    public static void TranslateTreatmentpanel()
    {
        if (GuiPanels.guiPanels.Treatmentpanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.Treatmentpanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("TREATMENT_PROFILE");
            GuiPanels.guiPanels.Treatmentpanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Add_Session");
            GuiPanels.guiPanels.Treatmentpanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Show_History");
            GuiPanels.guiPanels.Treatmentpanel.transform.GetChild(3).GetComponentInChildren<Text>().text = Language.Get("Contiue_Session");
        }
    }
    public static void TranslatePatientSelectionPanel()
    {
        if (GuiPanels.guiPanels.patientselectionpanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("Patient_Selection");
        }
        if (GuiPanels.guiPanels.patientDetailPanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = Language.Get("Last_Name");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = Language.Get("Mobile_No");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = Language.Get("First_Name");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(3).GetComponent<Text>().text = Language.Get("Email_ID");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(4).GetComponent<Text>().text = Language.Get("WORD_Title");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(5).GetComponent<Text>().text = Language.Get("Phone_work");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(13).GetComponent<Text>().text = Language.Get("WORD_Gender");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(14).GetComponent<Text>().text = Language.Get("WORD_Address");
            GuiPanels.guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Enter_text");
        }

    }
    public static void TranslatePatientPanel()
    {
        if (GuiPanels.guiPanels.Patientpanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.Patientpanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("Patient_Profile");
            GuiPanels.guiPanels.Patientpanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Add_Profile");
            GuiPanels.guiPanels.Patientpanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Edit_Profile");
        }
    }
    public static void TranslateAddprofilepanel()
    {
        if (GuiPanels.guiPanels.Addprofilepanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.Addprofilepanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("Add_Profile");
        }
        if (GuiPanels.guiPanels.AddprofileContentpanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Title");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_Title");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(2).GetComponent<Text>().text = Language.Get("First_Name");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(3).GetComponentInChildren<Text>().text = Language.Get("Enter_First_Name");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(4).GetComponent<Text>().text = Language.Get("Last_Name");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(5).GetComponentInChildren<Text>().text = Language.Get("Enter_Last_Name");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(6).GetComponent<Text>().text = Language.Get("Phone_work");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(7).GetComponentInChildren<Text>().text = Language.Get("Enter_PhWork");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(8).GetComponent<Text>().text = Language.Get("Mobile_No");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(9).GetComponentInChildren<Text>().text = Language.Get("Enter_MobileNo");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(10).GetComponent<Text>().text = Language.Get("Email_ID");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(11).GetComponentInChildren<Text>().text = Language.Get("Enter_Email_ID");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(12).GetComponent<Text>().text = Language.Get("WORD_Gender");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(13).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("WORD_Male");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(13).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("WORD_Female");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(14).GetComponent<Text>().text = Language.Get("House_No");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(15).GetComponentInChildren<Text>().text = Language.Get("Enter_House_No");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(16).GetComponent<Text>().text = Language.Get("WORD_Locality");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(17).GetComponentInChildren<Text>().text = Language.Get("Enter_Locality");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(18).GetComponent<Text>().text = Language.Get("WORD_Town");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(19).GetComponentInChildren<Text>().text = Language.Get("Enter_Town");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(20).GetComponent<Text>().text = Language.Get("WORD_Pincode");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(21).GetComponentInChildren<Text>().text = Language.Get("Enter_Pincode");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(22).GetComponent<Text>().text = Language.Get("WORD_State");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(23).GetComponentInChildren<Text>().text = Language.Get("Enter_State");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(24).GetComponentInChildren<Text>().text = Language.Get("Add_Profile");
            GuiPanels.guiPanels.AddprofileContentpanel.transform.GetChild(25).GetComponentInChildren<Text>().text = Language.Get("WORD_Reset");
        }
    }
    public static void TranslateButtonpanel()
    {
        if (GuiPanels.guiPanels.Buttonpanel.activeInHierarchy)
        {
            if (GuiPanels.guiPanels.Buttonpanel.transform.GetChild(0).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.Buttonpanel.transform.GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Show_Points");
            }
            GuiPanels.guiPanels.Buttonpanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("WORD_MENU");
            if (GuiPanels.guiPanels.Buttonpanel.transform.GetChild(2).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.Buttonpanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Stop_Rotation_and_movement");
            }
        }
    }

    public static void TranslateEditProfilePanel()
    {
        if (GuiPanels.guiPanels.EditProfilePanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.EditProfilePanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("Edit_Profile");
            GuiPanels.guiPanels.EditProfilePanel.transform.GetChild(2).GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Enter_Text");
            GuiPanels.guiPanels.EditProfilePanel.transform.GetChild(3).GetComponent<Text>().text = Language.Get("Select_Patient");
        }
        if (GuiPanels.guiPanels.EditprofileContentpanel.activeInHierarchy)
        {
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Title");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_Title");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(2).GetComponent<Text>().text = Language.Get("First_Name");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(3).GetComponentInChildren<Text>().text = Language.Get("Enter_First_Name");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(4).GetComponent<Text>().text = Language.Get("Last_Name");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(5).GetComponentInChildren<Text>().text = Language.Get("Enter_Last_Name");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(6).GetComponent<Text>().text = Language.Get("Phone_work");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(7).GetComponentInChildren<Text>().text = Language.Get("Enter_PhWork");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(8).GetComponent<Text>().text = Language.Get("Mobile_No");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(9).GetComponentInChildren<Text>().text = Language.Get("Enter_MobileNo");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(10).GetComponent<Text>().text = Language.Get("Email_ID");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(11).GetComponentInChildren<Text>().text = Language.Get("Enter_Email_ID");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(12).GetComponent<Text>().text = Language.Get("WORD_Gender");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(13).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("WORD_Male");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(13).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("WORD_Female");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(14).GetComponent<Text>().text = Language.Get("House_No");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(15).GetComponentInChildren<Text>().text = Language.Get("Enter_House_No");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(16).GetComponent<Text>().text = Language.Get("WORD_Locality");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(17).GetComponentInChildren<Text>().text = Language.Get("Enter_Locality");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(18).GetComponent<Text>().text = Language.Get("WORD_Town");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(19).GetComponentInChildren<Text>().text = Language.Get("Enter_Town");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(20).GetComponent<Text>().text = Language.Get("WORD_Pincode");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(21).GetComponentInChildren<Text>().text = Language.Get("Enter_Pincode");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(22).GetComponent<Text>().text = Language.Get("WORD_State");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(23).GetComponentInChildren<Text>().text = Language.Get("Enter_State");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(24).GetComponentInChildren<Text>().text = Language.Get("Update_Patient");
            GuiPanels.guiPanels.EditprofileContentpanel.transform.GetChild(25).GetComponentInChildren<Text>().text = Language.Get("WORD_Reset");
        }
    }
    public static void TranslateSessionEditorPanel()
    {
        if (GuiPanels.guiPanels.SessionEditorPanel.activeInHierarchy)
        {
            if (GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(0).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(0).GetComponent<Text>().text = Language.Get("Session_Date");
            }
            if (GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(1).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(1).GetComponent<Text>().text = Language.Get("Session_Duration");
            }
            if (GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(2).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Session_Duration");
            }
            if (GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(5).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(5).GetComponent<Text>().text = Language.Get("Session_Time");
            }
            if (GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(8).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(8).GetComponentInChildren<Text>().text = Language.Get("WORD_Finish");
            }
            if (GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(4).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.SessionEditorPanel.transform.GetChild(4).GetComponent<Text>().text = Language.Get("WORD_Needle");
            }
        }

    }
    public static void TranslateColorBox()
    {
        if (GuiPanels.guiPanels.ColorBox.gameObject.activeInHierarchy)
        {
            GuiPanels.guiPanels.ColorBox.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Color");
        }
        if (GuiPanels.guiPanels.ColorBox.transform.GetChild(1).gameObject.activeInHierarchy)
        {
            GuiPanels.guiPanels.ColorBox.transform.GetChild(1).GetChild(1).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Custom_Color");
            GuiPanels.guiPanels.ColorBox.transform.GetChild(1).GetChild(1).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("WORD_Ok");
            GuiPanels.guiPanels.ColorBox.transform.GetChild(1).GetChild(1).GetChild(2).GetComponent<Text>().text = Language.Get("Selected_Color");
            GuiPanels.guiPanels.ColorBox.transform.GetChild(1).GetChild(1).GetChild(4).GetComponentInChildren<Text>().text = Language.Get("WORD_Cancel");
        }
        if (GuiPanels.guiPanels.ColorBox.transform.GetChild(2).gameObject.activeInHierarchy)
        {
            GuiPanels.guiPanels.ColorBox.transform.GetChild(2).GetChild(1).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("WORD_Color");
            GuiPanels.guiPanels.ColorBox.transform.GetChild(2).GetChild(1).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("WORD_Ok");
            GuiPanels.guiPanels.ColorBox.transform.GetChild(2).GetChild(1).GetChild(3).GetComponent<Text>().text = Language.Get("Selected_Color");
        }
    }
    public static void TranslateMeridianPanel()
    {
        if (GuiPanels.guiPanels.MeridianPanel.activeInHierarchy)
        {
            if (GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = Language.Get("Selected_Meridian");
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = Language.Get("No_Meridian_Selected");
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Change_Color");
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(3).GetComponentInChildren<Text>().text = Language.Get("Create_New_Meridian");
            }
            if (GuiPanels.guiPanels.MeridianPanel.transform.GetChild(1).gameObject.activeInHierarchy)
            {
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = Language.Get("Create_New_Meridian");
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(1).GetChild(1).GetComponentInChildren<Text>().text = Language.Get("Enter_text");
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = Language.Get("Meridian_Name");
                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(1).GetChild(3).GetComponentInChildren<Text>().text = Language.Get("WORD_Create");
            }
            GuiPanels.guiPanels.MeridianPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Delete_Selected_Needle");
            GuiPanels.guiPanels.MeridianPanel.transform.GetChild(3).GetComponent<Text>().text = Language.Get("No_Needle_Selected");
        }

    }
    // Update is called once per frame
    void Update()
    {

    }
}
