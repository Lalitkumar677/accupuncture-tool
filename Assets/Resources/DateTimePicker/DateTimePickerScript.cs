﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DateTimePickerScript : MonoBehaviour
{
    // Use this for initialization
    static DateTime CurrentDate;
    public static DateTime selecteddate = DateTime.Now;
    public GameObject displaydatelabel;
    void Start()
    {
        CurrentDate = DateTime.Now;
        selecteddate = CurrentDate;
        gameObject.transform.GetChild(0).GetComponentInChildren<Text>().text = selecteddate.Year.ToString();
        gameObject.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get(selecteddate.ToString("MMMM"));
        DisplayDays();

    }
    public enum Days
    {
        Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    }
    public void DisplayDays()
    {
        DateTime d = new DateTime(selecteddate.Year, selecteddate.Month, 1);
        gameObject.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<Text>().text = Language.Get("WORD_Sun");
        gameObject.transform.GetChild(2).GetChild(0).GetChild(1).GetComponent<Text>().text = Language.Get("WORD_Mon");
        gameObject.transform.GetChild(2).GetChild(0).GetChild(2).GetComponent<Text>().text = Language.Get("WORD_Tue");
        gameObject.transform.GetChild(2).GetChild(0).GetChild(3).GetComponent<Text>().text = Language.Get("WORD_Wed");
        gameObject.transform.GetChild(2).GetChild(0).GetChild(4).GetComponent<Text>().text = Language.Get("WORD_Thr");
        gameObject.transform.GetChild(2).GetChild(0).GetChild(5).GetComponent<Text>().text = Language.Get("WORD_Fri");
        gameObject.transform.GetChild(2).GetChild(0).GetChild(6).GetComponent<Text>().text = Language.Get("WORD_Sat");

        for (int i = 1; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                string abc = d.DayOfWeek.ToString();
                int index = (int)(Enum.Parse(typeof(Days), abc));
                gameObject.transform.GetChild(2).GetChild(i).GetChild(j).GetComponent<Text>().text = string.Empty;
                if (index == j)
                {
                    if (d.Date == CurrentDate.Date)
                    {
                        gameObject.transform.GetChild(2).GetChild(i).GetChild(j).GetComponent<Text>().color = Color.red;
                    }
                    gameObject.transform.GetChild(2).GetChild(i).GetChild(j).GetComponent<Text>().text = d.Day.ToString();
                    if (gameObject.transform.GetChild(2).GetChild(i - 1).GetChild(j).GetComponent<Text>().text == d.Day.ToString())
                    {
                        gameObject.transform.GetChild(2).GetChild(i).GetChild(j).GetComponent<Text>().text = string.Empty;
                    }
                    if (d.Day < DateTime.DaysInMonth(d.Year, d.Month))
                    {
                        d = d.AddDays(1);
                    }
                }
            }
        }
    }
    public void NextYear()
    {
        selecteddate = selecteddate.AddYears(1);
        gameObject.transform.GetChild(0).GetComponentInChildren<Text>().text = selecteddate.Year.ToString();
        DisplayDays();
        ResetColor();
    }
    public void PreviousYear()
    {
        selecteddate = selecteddate.AddYears(-1);
        gameObject.transform.GetChild(0).GetComponentInChildren<Text>().text = selecteddate.Year.ToString();
        DisplayDays();
        ResetColor();
    }
    public void NextMonth()
    {
        selecteddate = selecteddate.AddMonths(1);
        gameObject.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get(selecteddate.ToString("MMMM"));
        gameObject.transform.GetChild(0).GetComponentInChildren<Text>().text = selecteddate.Year.ToString();
        DisplayDays();
        ResetColor();
    }
    public void PreviousMonth()
    {
        selecteddate = selecteddate.AddMonths(-1);
        gameObject.transform.GetChild(1).GetComponentInChildren<Text>().text = Language.Get(selecteddate.ToString("MMMM"));
        gameObject.transform.GetChild(0).GetComponentInChildren<Text>().text = selecteddate.Year.ToString();
        DisplayDays();
        ResetColor();
    }
    public void ColorNormal(UnityEngine.Object Label)
    {
        GameObject datelabel = (GameObject)Label;
        datelabel.GetComponent<Text>().color = new Color(0, 0, 0, 255);
        string d = datelabel.GetComponent<Text>().text;
        RedColorOnTodaydate(datelabel, d);
    }
    public void ColorHover(UnityEngine.Object Label)
    {
        GameObject datelabel = (GameObject)Label;
        string d = datelabel.GetComponent<Text>().text;
        datelabel.GetComponent<Text>().color = new Color(255, 255, 255, 255);
        RedColorOnTodaydate(datelabel, d);
    }

    private static void RedColorOnTodaydate(GameObject datelabel, string d)
    {
        if (d == CurrentDate.Day.ToString() && selecteddate.Month == CurrentDate.Month && selecteddate.Year == CurrentDate.Year)
        {
            datelabel.GetComponent<Text>().color = Color.red;
        }
    }

    public void Selectdate(UnityEngine.Object Label)
    {
        GameObject datelabel = (GameObject)Label;
        int date;
        ResetColor();
        bool converted = int.TryParse(datelabel.GetComponent<Text>().text, out date);
        if (converted)
        {
            selecteddate = new DateTime(selecteddate.Year, selecteddate.Month, date);
            datelabel.GetComponent<Text>().color = Color.yellow;
        }
        gameObject.SetActive(false);
        displaydatelabel.GetComponent<Text>().text = selecteddate.ToShortDateString();
    }
    public void ResetColor()
    {
        for (int i = 1; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                GameObject obj = gameObject.transform.GetChild(2).GetChild(i).GetChild(j).gameObject;
                obj.GetComponent<Text>().color = new Color(0, 0, 0, 255);
                string d = obj.GetComponent<Text>().text;
                RedColorOnTodaydate(obj, d);
            }
        }
    }
}
