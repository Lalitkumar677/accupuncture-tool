﻿using System.Collections.Generic;

public class BillDetails
{
    public string BillName { get; set; }
    public List<DocumentValue> DocumentValues { get; set; }
}

public class DocumentValue
{
    public string Key { get; set; }
    public string Value { get; set; }
    public DocumentValue(string key, string value)
    {
        this.Key = key;
        this.Value = value;
    }
}


