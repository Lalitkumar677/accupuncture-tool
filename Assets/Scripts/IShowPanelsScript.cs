﻿using System;
public interface IShowPanelsScript
{
    /// <summary>
    /// Shows treatment panel on treatmentlog button click
    /// </summary>
    void TreatmentLogButtonClick();
    /// <summary>
    /// Shows Patient Profile panel on PatientProfile button click
    /// </summary>
    void PatientProfileButtonClick();
    /// <summary>
    /// Loads master data on master data button click
    /// </summary>
    void MasterButtonClick();
    /// <summary>
    /// Shows Instructions panel on Instructions button click
    /// </summary>
    void InstructionsButtonClick();
    /// <summary>
    /// Loads Add session scene on Addsession button click
    /// </summary>
    void AddsessionButtonClick();
    /// <summary>
    /// Loads Session History scene on show history button click
    /// </summary>
    void ShowHistoryButtonClick();
    /// <summary>
    /// Shows Add profile panel on Add profile button click
    /// </summary>
    void AddprofileButtonClick();
    /// <summary>
    /// Shows Edit profile panel on Edit profile button click
    /// </summary>
    void EditprofileButtonClick();
    /// <summary>
    /// Loads the Main menu on Menu button click
    /// </summary>
    void MenuButtonClick();
    /// <summary>
    /// Shows Color dialog box on change color
    /// </summary>
    void ChangeColor();
    /// <summary>
    /// Hides the message dialog box
    /// </summary>
    void HideMessageDialogbox();
    /// <summary>
    /// Show Error dialog Box
    /// </summary>
    /// <param name="massege">Message to display</param>
    /// <param name="title">Title of dialogBox</param>
    void ShowErrorPanel(string massege, string title);
    /// <summary>
    /// Show message dialog Box
    /// </summary>
    /// <param name="message">Message to display</param>
    void ShowMessage(string message);
    /// <summary>
    /// Shows Finance Panel on Finance Button click
    /// </summary>
    void ShowFinancePanelButtonClick();
    /// <summary>
    /// Activates the Send Tab in finance panel
    /// </summary>
    void SendButtonClick();
    /// <summary>
    /// Activates the Recieve Tab in finance panel 
    /// </summary>
    void RecieveButtonClick();
    /// <summary>
    /// Shows the main menu 
    /// </summary>
    void menu();
}