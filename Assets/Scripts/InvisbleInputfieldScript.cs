﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InvisbleInputfieldScript : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        body = GameObject.Find("Jessica");
    }
    public List<GameObject> gameobjects;
    public GameObject Needle;
    public bool shouldupdate = false;
    public GameObject body;
    public static bool IsNameInserted = true;
    // Update is called once per frame
    void Update()
    {
        if (!gameObject.GetComponent<InputField>().isFocused)
        {
            string Needlename = gameObject.GetComponent<InputField>().text;
            if (string.IsNullOrEmpty(Needlename))
            {
                WarnUserForInsertingName();
                return;
            }
            else
            {
                IsNameInserted = true;
                gameObject.GetComponentInChildren<Text>().color = new Color32(50, 50, 50, 255);
            }
            OperateNeedleAndDeleteTextbox(Needlename);
        }
    }

    private void OperateNeedleAndDeleteTextbox(string Needlename)
    {
        NeedleOperationScript _needleOperationScript = Camera.main.GetComponent<NeedleOperationScript>();
        string linename = Needle.GetComponent<ChangeNeedleposition>().LineName;
        string color = Needle.GetComponent<ChangeNeedleposition>().color;
        UpdateOrAdd(Needlename, _needleOperationScript, linename, color);
        Needle.AddComponent<ShowNeedleNameScript>();
        ButtonClickScript.IsMouseoverUIControl = false;
        Destroy(gameObject);
    }
    private void UpdateOrAdd(string Needlename, NeedleOperationScript _needleOperationScript, string linename, string color)
    {
        if (shouldupdate)
        {
            UpdateNeedle(Needlename, _needleOperationScript, linename, color);
        }
        else
        {
            AddNeedle(Needlename, _needleOperationScript, color, linename);
        }
    }
    private void WarnUserForInsertingName()
    {
        IsNameInserted = false;
        //Camera.main.GetComponent<ShowPanelsScript>().ShowErrorPanel("Please provide a name to needle", Language.Get("WORD_INFORMATION"));
        gameObject.GetComponentInChildren<Text>().text = "Please Provide Needle Name";
        gameObject.GetComponentInChildren<Text>().color = Color.red;
        gameObject.GetComponent<InputField>().Select();
    }

    private void UpdateNeedle(string Needlename, NeedleOperationScript _needleOperationScript, string linename, string color)
    {
        if (Needle.GetComponent<NeedleId>() != null)
        {
            int needleId = Needle.GetComponent<NeedleId>().Id;
            Needle.GetComponent<NeedleId>().Name = Needlename;
            Needle.GetComponent<ChangeNeedleposition>().NameLabel.GetComponent<Text>().text = Needlename;
            Needles needleObj = CreateNeedleObject(linename, color, needleId);
            _needleOperationScript.UpdateNeedle(needleObj);
        }
    }

    private void AddNeedle(string Needlename, NeedleOperationScript _needleOperationScript, string color, string linename)
    {
        Needle.GetComponent<NeedleId>().Name = Needlename;
        Needle.GetComponent<ChangeNeedleposition>().NameLabel.GetComponent<Text>().text = Needlename;
        GameObject prened = Camera.main.GetComponent<NeedleScript>()._previousneedle;
        if (gameobjects.Count == 0)
        {
            Needles needleObj = CreateNeedleObject(linename, color, -1);
            _needleOperationScript.AddNeedle(needleObj, prened, Needle);
        }
        else
        {
            _needleOperationScript.AddNeedleslist(gameobjects, prened, Needlename);
        }
    }

    private Needles CreateNeedleObject(string linename, string color, int needleId)
    {
        Needles needleObj = new Needles();
        needleObj.Id = needleId;
        needleObj.PositionX = Needle.transform.localPosition.x;
        needleObj.PositionY = Needle.transform.localPosition.y;
        needleObj.PositionZ = Needle.transform.localPosition.z;
        needleObj.RotationX = Needle.transform.localRotation.x;
        needleObj.RotationY = Needle.transform.localRotation.y;
        needleObj.RotationZ = Needle.transform.localRotation.z;
        needleObj.RotationW = Needle.transform.localRotation.w;
        needleObj.ScaleX = Needle.transform.localScale.x;
        needleObj.ScaleY = Needle.transform.localScale.y;
        needleObj.ScaleZ = Needle.transform.localScale.z;
        needleObj.NeedleName = name;
        needleObj.Linename = linename;
        needleObj.color = color;
        needleObj.Prefab = transform.tag;
        return needleObj;
    }
}
