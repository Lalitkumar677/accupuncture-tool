﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using Boomlagoon.JSON;

public class PdfGenerateScript : MonoBehaviour
{
    // Use this for initialization
//    private static readonly JCsLogger log = new JCsLogger(typeof(PdfGenerateScript));
    ShowPanelsScript _showPanelsScript;
    void Start()
    {
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
    }
    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator LoadDoctor(int id)
    {
        ServiceUrlscript _serviceUrlscript = new ServiceUrlscript();
        string url = _serviceUrlscript.LoadDoctor + id;
        WWW www = new WWW(url);
        yield return StartCoroutine("LoadDoctorDetail", www);
    }

    public void generatebill(string invoice)
    {
        _showPanelsScript.ShowMessage(Language.Get("WORD_Loading") + "...");
        StartCoroutine("AssignValues", invoice);
    }
    IEnumerator AssignValues(string invoice)
    {
       // int id = Camera.main.GetComponent<UserIdScript>().DoctorID;
      // yield return StartCoroutine("LoadDoctor", id);
        yield return StartCoroutine("StartassigningValues", invoice);

    }
    IEnumerator LoadDoctorDetail(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            JSONObject json = JSONObject.Parse(www.text);
            if (json != null)
            {
                Doctor doctor = CreateDoctorFromJson(json);
                Camera.main.GetComponent<UserIdScript>().doctor = doctor;
            }
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }

    public static Doctor CreateDoctorFromJson(JSONObject json)
    {
        Doctor doctor = new Doctor();
        doctor.DoctorID = int.Parse(json["DoctorID"].ToString());
        doctor.DoctorName = json["DoctorName"].Str;
        doctor.EmailID = json["EmailId"].Str;
        doctor.Gender = json["Gender"].Str;
        doctor.Houseno = json["Houseno"].Str;
        doctor.Locality = json["Locality"].Str;
        doctor.PerHourFees = float.Parse(json["PerHourFees"].ToString());
        doctor.Pincode = json["Pincode"].Str;
        doctor.state = json["state"].Str;
        doctor.Town = json["Town"].Str;
        doctor.ASCA = json["ASCA"].Str;
        doctor.EMR = json["EMR"].Str;
        doctor.EGK = json["EGK"].Str;
        doctor.RoleID= int.Parse(json["RoleID"].ToString());
        doctor.CompanyID= int.Parse(json["RoleID"].ToString());
        doctor.Address = doctor.Houseno + ", " + doctor.Locality + " " + doctor.Town + " " + doctor.state + ", " + doctor.Pincode;
        return doctor;
    }
    public static Doctor CreateDoctorFromJsonArray(JSONArray json, int i)
    {
        Doctor doctor = new Doctor();
        doctor.DoctorID = Convert.ToInt32(json[i].Obj["DoctorID"].Number);
        doctor.DoctorName = json[i].Obj["DoctorName"].Str;
        doctor.EmailID = json[i].Obj["EmailId"].Str;
        doctor.Gender = json[i].Obj["Gender"].Str;
        doctor.Houseno = json[i].Obj["Houseno"].Str;
        doctor.Locality = json[i].Obj["Locality"].Str;
        doctor.PerHourFees = float.Parse(json[i].Obj["PerHourFees"].ToString());
        doctor.Pincode = json[i].Obj["Pincode"].Str;
        doctor.state = json[i].Obj["state"].Str;
        doctor.Town = json[i].Obj["Town"].Str;
        doctor.ASCA = json[i].Obj["ASCA"].Str;
        doctor.EMR = json[i].Obj["EMR"].Str;
        doctor.EGK = json[i].Obj["EGK"].Str;
        doctor.RoleID = int.Parse(json[i].Obj["RoleID"].ToString());
        doctor.CompanyID = int.Parse(json[i].Obj["CompanyID"].ToString());
        doctor.Address = doctor.Houseno + ", " + doctor.Locality + " " + doctor.Town + " " + doctor.state + ", " + doctor.Pincode;
        return doctor;
    }

    IEnumerator StartassigningValues(string invoice)
    {
        GuiPanels _guiPanels = Camera.main.GetComponent<GuiPanels>();
        ServiceUrlscript _serviceUrlscript = new ServiceUrlscript();
        Payment payment = PaymentDetailScript.billsenddetails.Find(p => p.PaymentId.ToString() == invoice);
        Session session = PointVisibilityScript.AllPatientsessions.Find(p => p.sessionID == payment.SessionId);
        DropDownScript DropDown = _guiPanels.patientselectionpanel.transform.GetChild(2).GetComponent<DropDownScript>();
        Patient patient = DropDown.DropDownItem.Find(p => p.ID == session.PatientID);
        List<DocumentValue> documentValue = new List<DocumentValue>();
        DocumentValue INVOICENUMBER = CreateDocumentValuesForPatientAndSession(invoice, payment, session, patient, documentValue);
        CreateDocumentValuesForDoctor(documentValue);
        string abc = CreateJsonArrayForGeneratingBill(INVOICENUMBER, documentValue);
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] body = Encoding.UTF8.GetBytes(abc);
        WWW www = new WWW(_serviceUrlscript.GeneratedPdf, body, headers);
        object[] par = new object[] { www, INVOICENUMBER };
        yield return StartCoroutine("GenerateBill", par);
    }

    private static void CreateDocumentValuesForDoctor(List<DocumentValue> documentValue)
    {
        Doctor doctor = Camera.main.GetComponent<UserIdScript>().doctor;
        if (doctor != null)
        {
            DocumentValue PHF = new DocumentValue("PHF", doctor.PerHourFees.ToString());
            DocumentValue DOCTORNAME = new DocumentValue("DOCTORNAME", doctor.DoctorName);
            DocumentValue DOCTORADDRESS = new DocumentValue("DOCTORADDRESS", doctor.Address);
            DocumentValue DOCTOREMAILID = new DocumentValue("DOCTOREMAILID", doctor.EmailID);
            DocumentValue ASCACODE = new DocumentValue("ASCACODE", doctor.ASCA);
            DocumentValue EMRCODE = new DocumentValue("EMRCODE", doctor.EMR);
            DocumentValue EGKCODE = new DocumentValue("EGKCODE", doctor.EGK);
            documentValue.Add(DOCTORNAME);
            documentValue.Add(DOCTORADDRESS);
            documentValue.Add(PHF);
            documentValue.Add(DOCTOREMAILID);
            documentValue.Add(ASCACODE);
            documentValue.Add(EGKCODE);
            documentValue.Add(EMRCODE);
        }
    }
    private static DocumentValue CreateDocumentValuesForPatientAndSession(string invoice, Payment payment, Session session, Patient patient, List<DocumentValue> documentValue)
    {
        DocumentValue PATIENTNAME = new DocumentValue("PATIENTNAME", patient.FName + " " + patient.LName);
        DocumentValue PATIENTADDRESS = new DocumentValue("PATIENTADDRESS", patient.Houseno + " " + patient.Locality);
        DocumentValue PINCODE = new DocumentValue("PINCODE", patient.Pincode);
        DocumentValue TOWN = new DocumentValue("TOWN", patient.Town);
        DocumentValue BILLSENTDATE = new DocumentValue("BILLSENTDATE", DateTime.Now.ToShortDateString());
        DocumentValue INVOICENUMBER = new DocumentValue("INVOICENUMBER", invoice);
        DocumentValue GENDER = (patient.Gender == CommonTypes.Gender.Female.ToString()) ? new DocumentValue("GENDER", " geehrte Frau") : new DocumentValue("GENDER", " geehrter Herr");
        DocumentValue SESSIONDATE = new DocumentValue("SESSIONDATE", session.SessionBegin.Substring(0, session.SessionBegin.IndexOf('T')));
        DocumentValue SESSIONTYPE = new DocumentValue("SESSIONTYPE", "Acupuncture");
        DocumentValue DURATION = new DocumentValue("DURATION", session.Duration.ToString());
        DocumentValue AMOUNT = new DocumentValue("AMOUNT", payment.Amount.ToString());
        DocumentValue MOBILENUMBER = new DocumentValue("MOBILENUMBER", "9876543210");
        documentValue.Add(PATIENTNAME);
        documentValue.Add(PATIENTADDRESS);
        documentValue.Add(PINCODE);
        documentValue.Add(TOWN);
        documentValue.Add(BILLSENTDATE);
        documentValue.Add(INVOICENUMBER);
        documentValue.Add(GENDER);
        documentValue.Add(SESSIONDATE);
        documentValue.Add(SESSIONTYPE);
        documentValue.Add(DURATION);
        documentValue.Add(AMOUNT);
        documentValue.Add(MOBILENUMBER);
        return INVOICENUMBER;
    }

    private static string CreateJsonArrayForGeneratingBill(DocumentValue INVOICENUMBER, List<DocumentValue> documentValue)
    {
        string abc = "{\"BillName\":\"" + INVOICENUMBER.Value + "\",";
        if (documentValue.Count > 0)
        {
            abc += "\"DocumentValues\":";
            abc += "[{";
            for (int i = 0; i < documentValue.Count; i++)
            {
                abc += "\"Key\":\"" + documentValue[i].Key + "\",";
                abc += "\"Value\":\"" + documentValue[i].Value + "\",";
                abc += "},{";
            }
            abc = abc.Substring(0, abc.Length - 2) + "]";
        }
        abc += "}";
        return abc;
    }

    IEnumerator GenerateBill(object[] para)
    {
        WWW www = (WWW)para[0];
        DocumentValue INVOICENUMBER = (DocumentValue)para[1];
        yield return www;
        _showPanelsScript.HideMessageDialogbox();
        if (www.error == null)
        {
            _showPanelsScript.HideMessageDialogbox();
            JSONObject json = JSONObject.Parse(www.text);
            int result = int.Parse(json["Result"].ToString());
            if (result == 1 || result == 2)
            {
                Application.OpenURL((new ServiceUrlscript()).DownloadBill + INVOICENUMBER.Value);
            }
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
            LogScript.Log(www);
            _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
        }
    }
}
