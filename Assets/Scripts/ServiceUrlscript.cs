﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class ServiceUrlscript
{
    public string addNeedleUrl { get; set; }
    public string addPatientUrl { get; set; }
    public string deleteNeedleUrl { get; set; }
    public string updateNeedleUrl { get; set; }
    public string loadNeedlesUrl { get; set; }
    public string updatePatientUrl { get; set; }
    public string loadPatientUrl { get; set; }
    public string loadPatientsUrl { get; set; }
    public string AddSessionUrl { get; set; }
    public string loadSessionNeedles { get; set; }
    public string loadallsessions { get; set; }
    public string changecolorurl { get; set; }
    public string addNeedleListUrl { get; set; }
    public string loadDoctorpatient { get; set; }
    public string AuthenticateDoctorUrl { get; set; }
    public string AddDoctor { get; set; }
    public string LoadPayments { get; set; }
    public string UpdatePaymentUrl { get; set; }
    public string LoadDoctor { get; set; }
    public string GeneratedPdf { get; set; }
    public string DownloadBill { get; set; }
    public string LoadAllDoctors { get; set; }
    public string LoadAllDoctorsbyAdmin { get; set; }
    public string CreateAppointment { get; set; }
    public string LoadAppointmentOfDoctor { get; set; }
    public ServiceUrlscript()
    {
        XmlDocument doc = new XmlDocument();
        TextAsset propStoreTextAsset = (TextAsset)Resources.Load("XMLFile", typeof(TextAsset));
        doc.LoadXml(propStoreTextAsset.text);
        XmlNode root = doc.ChildNodes[1];
        addNeedleUrl = root.ChildNodes[0].InnerText;
        updateNeedleUrl = root.ChildNodes[1].InnerText;
        deleteNeedleUrl = root.ChildNodes[2].InnerText;
        loadNeedlesUrl = root.ChildNodes[4].InnerText;
        addPatientUrl = root.ChildNodes[5].InnerText;
        updatePatientUrl = root.ChildNodes[6].InnerText;
        loadPatientUrl = root.ChildNodes[8].InnerText;
        loadPatientsUrl = root.ChildNodes[9].InnerText;
        AddSessionUrl = root.ChildNodes[10].InnerText;
        loadSessionNeedles = root.ChildNodes[11].InnerText;
        loadallsessions = root.ChildNodes[12].InnerText;
        changecolorurl = root.ChildNodes[13].InnerText;
        addNeedleListUrl = root.ChildNodes[14].InnerText;
        loadDoctorpatient = root.ChildNodes[15].InnerText;
        AuthenticateDoctorUrl = root.ChildNodes[16].InnerText;
        AddDoctor = root.ChildNodes[17].InnerText;
        LoadPayments = root.ChildNodes[18].InnerText;
        UpdatePaymentUrl = root.ChildNodes[19].InnerText;
        LoadDoctor = root.ChildNodes[20].InnerText;
        GeneratedPdf = root.ChildNodes[21].InnerText;
        DownloadBill = root.ChildNodes[22].InnerText;
        LoadAllDoctors= root.ChildNodes[23].InnerText;
        LoadAllDoctorsbyAdmin = root.ChildNodes[24].InnerText;
        CreateAppointment = root.ChildNodes[25].InnerText;
        LoadAppointmentOfDoctor = root.ChildNodes[26].InnerText;
    }
}
