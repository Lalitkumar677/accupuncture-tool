﻿using Boomlagoon.JSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class AdminScript : MonoBehaviour
{
    private GuiPanels _guiPanels;
    private ShowPanelsScript _showPanelsScript;
    ServiceUrlscript serviceUrlscript;
    public static List<Appointment> DoctorAppointment = new List<Appointment>();
    public static List<Appointment> WeekAppointment = new List<Appointment>();
    GameObject appointmentRow;
    public static DateTime SelectedWeekMonday;
    public static DateTime SelectedWeekSunday;
    // Use this for initialization
    void Start()
    {
        GetandAssignComponent();
        serviceUrlscript = new ServiceUrlscript();
        appointmentRow = Resources.Load("Prefabs/AppointmentRow") as GameObject;
        AdminHelper.GetCurrentWeekDays(DateTime.Now, out SelectedWeekMonday, out SelectedWeekSunday);
    }
    private void GetandAssignComponent()
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
    }

    public void OnEnable()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    /// <summary>
    /// Redirect To Main Menu
    /// </summary>
    public void LoadScene()
    {
        Camera.main.GetComponent<LoginOperationScript>().RedirectToHomeMenu();
    }
    /// <summary>
    /// If Logged in as Doctor then Logout and if logged in as Admin then Back
    /// </summary>
    public void BackOrLogOut()
    {
        if (LoginOperationScript.LoginAs == CommonTypes.Roles.Admin)
        {
            _showPanelsScript.ShowLoginPanel();
            _showPanelsScript.ShowAdminPanel();
        }
        else
        {
            Logout();
        }
    }
    /// <summary>
    /// LogOut
    /// </summary>
    public void Logout()
    {
        _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(0).GetComponent<InputField>().text = string.Empty;
        _guiPanels.Userloginpanel.transform.GetChild(0).GetChild(1).GetComponent<InputField>().text = string.Empty;
        _showPanelsScript.ShowLoginPanel();
    }
    /// <summary>
    /// Creates the appointment for current User
    /// </summary>
    public void CreateAppointment()
    {
        StartCoroutine("process");
    }
    IEnumerator process()
    {
        Appointment appointment = AdminHelper.CreateAppointmentObject(_guiPanels);
        string date = appointment.AppointmentDate.ToString().Substring(0, appointment.AppointmentDate.ToString().IndexOf(' '));
        _showPanelsScript.ShowConfirmationPanel("Are you sure you want to fix Appointment with\n <b><Color=whilte>" + appointment.ClientName.ToUpper() + "</Color> \non <Color=whilte>" + date + "</color>\n from <Color=whilte>" + appointment.StartTime + " to " + appointment.EndTime + "</color></b>?");
        while (_guiPanels.ConfirmationPanel.activeInHierarchy)
        {
            yield return null;
        }
        if (JessicaRotationScript.ConfirmationYes)
        {
            WWWForm form = new WWWForm();
            form.AddField("AppointmentDate", appointment.AppointmentDate.ToString());
            form.AddField("ClientName", appointment.ClientName.ToString());
            form.AddField("EndTime", appointment.EndTime.ToString());
            form.AddField("StartTime", appointment.StartTime.ToString());
            form.AddField("DoctorID", appointment.DoctorID);
            WWW www = new WWW(serviceUrlscript.CreateAppointment, form);
            _showPanelsScript.ShowMessage(Language.Get("WORD_Saving") + "...");
            StartCoroutine("AddAppointment", www);
        }
    }
    /// <summary>
    /// Wait untill service gets executed 
    /// </summary>
    /// <param name="www">WWW object with url to sevice and information of appointment</param>
    /// <returns>Returns null </returns>
    IEnumerator AddAppointment(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            JSONObject json = JSONObject.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            Appointment appointment = AdminHelper.CreateAppointmentObjectFromJson(json);
            DoctorAppointment.Add(appointment);
            _showPanelsScript.HideMessageDialogbox();
            _showPanelsScript.ShowErrorPanel(Language.Get("WORD_Saved"), Language.Get("WORD_INFORMATION"));
            while (_guiPanels.ErrorPanel.activeInHierarchy)
            {
                yield return null;
            }
            GetweekAppointment();
            InstanciateAppointmentRow();
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }

    /// <summary>
    /// Loads all appointments of current Doctor
    /// </summary>
    public void LoadAppointmentsOFdoctor()
    {
        _guiPanels.AppointmentPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = (LoginOperationScript.LoginAs == CommonTypes.Roles.User) ? "Menu" : "Back";
        _showPanelsScript.ShowMessage("Loading...");
        int DoctorId = Camera.main.GetComponent<UserIdScript>().DoctorID;
        string _url = serviceUrlscript.LoadAppointmentOfDoctor + DoctorId;
        WWW www = new WWW(_url);
        StartCoroutine("LoadAppointment", www);
    }
    IEnumerator LoadAppointment(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            _showPanelsScript.HideMessageDialogbox();
            JSONArray json = JSONArray.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            DoctorAppointment.Clear();
            if (json.Length > 0)
            {
                for (int i = 0; i < json.Length; i++)
                {
                    Appointment appointment = AdminHelper.CreateAppointmentObjectFromJson(json[i].Obj);
                    DoctorAppointment.Add(appointment);
                }
            }
            GetweekAppointment();
            InstanciateAppointmentRow();
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }

    /// <summary>
    /// Get the selecetd week Appointment of the current user
    /// </summary>
    public static void GetweekAppointment()
    {
        WeekAppointment.Clear();
        WeekAppointment = DoctorAppointment.Where(p => p.AppointmentDate >= SelectedWeekMonday && p.AppointmentDate <= SelectedWeekSunday).OrderBy(p => p.AppointmentDate).ToList();
    }
    /// <summary>
    /// Generates the row in apopointment list of the cuurent week
    /// </summary>
    public void InstanciateAppointmentRow()
    {
        for (int i = _guiPanels.AppointmentContentPanel.transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(_guiPanels.AppointmentContentPanel.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < WeekAppointment.Count; i++)
        {
            GenerateRow(WeekAppointment[i], i);
        }
        if (WeekAppointment.Count == 0)
        {
            Appointment app = new Appointment();
            GenerateRow(app, -1);
        }
        _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = SelectedWeekMonday.ToString("dd/MM/yyyy") + " - " + SelectedWeekSunday.ToString("dd/MM/yyyy");
    }

    private void GenerateRow(Appointment appointment, int i)
    {
        GameObject row = Instantiate(appointmentRow) as GameObject;
        row.transform.SetParent(_guiPanels.AppointmentContentPanel.transform);
        row.transform.localScale = new Vector3(1f, 1f, 1f);
        row.transform.GetChild(3).GetComponent<Text>().text = (i == -1) ? string.Empty : (i + 1).ToString();
        row.transform.GetChild(0).GetComponent<Text>().text = (!string.IsNullOrEmpty(appointment.ClientName)) ? appointment.ClientName : string.Empty;
        string date = (appointment.AppointmentDate.Year != 0001) ? appointment.AppointmentDate.ToLongDateString() : "No Appointment For this Week!!!";
        row.transform.GetChild(1).GetComponent<Text>().text = date;
        row.transform.GetChild(2).GetComponent<Text>().text = (appointment.AppointmentDate.Year != 0001) ? appointment.StartTime.ToString() + " - " + appointment.EndTime.ToString() : string.Empty;
    }

    /// <summary>
    /// Displays the next week appointment in the Appointment list
    /// </summary>
    public void DisplayNextWeekAppointment()
    {
        AdminHelper.ChangeWeek(ref SelectedWeekMonday, out SelectedWeekSunday, CommonTypes.Action.Forward);
        GetweekAppointment();
        InstanciateAppointmentRow();

    }
    /// <summary>
    /// Displays the appointment of the previous week in the appointment list
    /// </summary>
    public void DisplayPrevoiusWeekAppointment()
    {
        AdminHelper.ChangeWeek(ref SelectedWeekMonday, out SelectedWeekSunday, CommonTypes.Action.Backward);
        GetweekAppointment();
        InstanciateAppointmentRow();
    }
    /// <summary>
    /// Search the week for given date and display the week appoitnment in the list
    /// </summary>
    public void SearchDateAppointment()
    {
        GameObject textbox = _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(1).GetChild(1).gameObject;
        string enteredDate = textbox.GetComponent<InputField>().text;
        DateTime Date;
        if (DateTime.TryParse(enteredDate, out Date))
        {
            AdminHelper.GetCurrentWeekDays(Date, out SelectedWeekMonday, out SelectedWeekSunday);
            GetweekAppointment();
            InstanciateAppointmentRow();
        }
        else
        {
            textbox.GetComponent<InputField>().text = string.Empty;
            textbox.GetComponentInChildren<Text>().text = "MM/DD/YYYY";
            textbox.GetComponentInChildren<Text>().color = Color.red;
            textbox.GetComponent<InputField>().Select();
        }
    }
    /// <summary>
    /// Displays the appointment for the current doctor when logged in as doctor
    /// </summary>
    public void ShowDoctorSchedule()
    {
        _showPanelsScript.ShowCreateAppointment();
        _guiPanels.AppointmentPanel.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        LoadAppointmentsOFdoctor();
    }
    /// <summary>
    /// If logged in as Admin the back to admin panel and if logged in as User then redirect to menu.
    /// </summary>
    public void MenuOrBack()
    {
        if (LoginOperationScript.LoginAs == CommonTypes.Roles.User)
        {
            _guiPanels.AppointmentPanel.SetActive(false);
            LoadScene();
        }
        else
        {
            _showPanelsScript.ShowAdminPanel();
        }
    }
}
