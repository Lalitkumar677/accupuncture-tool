﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Needles
{
    public int Id { get; set; }
    public float PositionX { get; set; }
    public float PositionY { get; set; }
    public float PositionZ { get; set; }
    public float RotationX { get; set; }
    public float RotationY { get; set; }
    public float RotationZ { get; set; }
    public float RotationW { get; set; }
    public float ScaleX { get; set; }
    public float ScaleY { get; set; }
    public float ScaleZ { get; set; }
    public string Prefab { get; set; }
    public string NeedleName { get; set; }
    public string Linename { get; set; }
    public string color { get; set; }
}

