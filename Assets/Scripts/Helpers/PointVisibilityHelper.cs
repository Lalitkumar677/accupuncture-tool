﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointVisibilityHelper
{
    private const float needlecolorred = 0.782f;
    private const float needlecolorgreen = 0.791f;
    private const float needlecolorblue = 0.794f;
    private const float needlecoloralpha = 0.118f;
    public static void SettextAndoperatesessionvariable(GameObject MeridianButton, bool state, string buttontext)
    {
        PointVisibilityScript.isSessionOperating = state;
        MeridianButton.transform.GetComponentInChildren<Text>().text = buttontext;
    }
    public static bool RemoveNeedleFromListIsAllreadyAdded(GameObject needleClicked, bool found, Transform needlelistPanel, string id)
    {
        for (int i = 0; i < needlelistPanel.childCount; i++)
        {
            if (needlelistPanel.GetChild(i).GetChild(0).GetComponent<Text>().text == id)
            {
                found = true;
                Object.DestroyImmediate(needlelistPanel.GetChild(i).gameObject);
                needleClicked.transform.gameObject.GetComponent<Renderer>().material.color = SetNeedleDefaultColor();
                break;
            }
        }
        return found;
    }
    public static Color SetNeedleDefaultColor()
    {
        return new Color(needlecolorred, needlecolorgreen, needlecolorblue, needlecoloralpha);
    }
    /// <summary>
    /// setting the needle color to default in history mode on changing sessions
    /// </summary>
    public static void DefaultcolorForneedles(GuiPanels _guiPanels)
    {
        for (int j = 0; j < _guiPanels.Body.transform.childCount; j++)
        {
            GameObject needle = _guiPanels.Body.transform.GetChild(j).gameObject;
            if (needle.tag == CommonTypes.PrefabTag.needle.ToString())
            {
                Color r = PointVisibilityHelper.SetNeedleDefaultColor();
                needle.GetComponent<Renderer>().material.color = r;
            }
        }
    }
    /// <summary>
    /// making the controls invisible if there is no session for the selected patient 
    /// </summary>
    /// <param name="sessionReaderPanel"></param>
    /// <param name="state"></param>
    public static void ShowAndHideLabel(GameObject sessionReaderPanel, bool state)
    {
        for (int i = 0; i <= 5; i++)
        {
            if (i != 3)
            {
                sessionReaderPanel.transform.GetChild(i).gameObject.SetActive(state);
            }
        }
    }
    public static void GetSelectedPatientAllSession()
    {
        foreach (var session in PointVisibilityScript.AllPatientsessions)
        {
            if (session.PatientID == PointVisibilityScript.selectedpatient.ID)
            {
                PointVisibilityScript.SelectedPatientsessions.Add(session);
            }
        }
    }
    public static Needles GetNeedleObjectFromClickedNeedle(GameObject needleClicked)
    {
        Needles need = new Needles();
        need.PositionX = needleClicked.transform.localPosition.x;
        need.PositionY = needleClicked.transform.localPosition.y;
        need.PositionZ = needleClicked.transform.localPosition.z;
        need.RotationX = needleClicked.transform.localRotation.x;
        need.RotationY = needleClicked.transform.localRotation.y;
        need.RotationZ = needleClicked.transform.localRotation.z;
        need.RotationW = needleClicked.transform.localRotation.w;
        need.ScaleX = needleClicked.transform.localScale.x;
        need.ScaleY = needleClicked.transform.localScale.y;
        need.ScaleZ = needleClicked.transform.localScale.z;
        need.Prefab = needleClicked.transform.tag;
        need.NeedleName = needleClicked.transform.GetComponent<NeedleId>().Name;
        need.Linename = needleClicked.transform.GetComponent<ChangeNeedleposition>().LineName;
        need.color = needleClicked.transform.GetComponent<ChangeNeedleposition>().color;
        return need;
    }
    public static Vector3 SetDefaultLocalScale()
    {
        return new Vector3(1f, 1f, 1f);
    }
    /// <summary>
    /// clear the fields of patieny selection menu
    /// </summary>
    public static void clearPatientselectionPanel(GuiPanels _guiPanels)
    {
        _guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.GetComponent<InputField>().text = string.Empty;
        _guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(1).gameObject.SetActive(false);
        _guiPanels.patientselectionpanel.transform.GetChild(2).GetChild(0).GetChild(0).GetComponentInChildren<Text>().text = Language.Get("Enter_text");
        for (int i = 6; i <= 12; i++)
        {
            _guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(i).GetComponent<Text>().text = string.Empty;
        }
        _guiPanels.patientselectionpanel.transform.GetChild(1).GetChild(15).GetComponent<Text>().text = string.Empty;
    }
    public static void SetPropertyToNeedle(GameObject ned, Needles n)
    {
        ned.GetComponent<NeedleId>().Id = n.Id;
        ned.GetComponent<NeedleId>().Name = n.NeedleName;
        ned.transform.parent = GameObject.Find("Jessica").transform;
        ned.transform.localRotation = new Quaternion(n.RotationX, n.RotationY, n.RotationZ,
            n.RotationW);
        ned.transform.localScale = new Vector3(n.ScaleX, n.ScaleY, n.ScaleZ);
        ned.transform.localPosition = new Vector3(n.PositionX, n.PositionY, n.PositionZ);
        ned.GetComponent<ChangeNeedleposition>().LineName = n.Linename;
        ned.GetComponent<ChangeNeedleposition>().color = n.color;
    }
    public static void DestroyAllNeedleFromBody(GuiPanels _guiPanels)
    {
        for (int i = 0; i < _guiPanels.Body.transform.childCount; i++)
        {
            if (_guiPanels.Body.transform.GetChild(i).tag == CommonTypes.PrefabTag.point.ToString() || _guiPanels.Body.transform.GetChild(i).tag == CommonTypes.PrefabTag.needle.ToString())
            {
                Object.DestroyImmediate(_guiPanels.Body.transform.GetChild(i).gameObject);
            }
        }
    }
    public static bool ValidatePatientProfile(Patient pateint)
    {
        bool isvalidationpassed = true;
        if (string.IsNullOrEmpty(pateint.Title.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.FName.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.LName.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Email.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Pwork.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Pmobile.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Houseno.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Pincode.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Town.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.state.ToString())) isvalidationpassed = false;
        if (string.IsNullOrEmpty(pateint.Locality.ToString())) isvalidationpassed = false;
        return isvalidationpassed;
    }
    public static void DestroyNeedleFromPanel(GameObject panel)
    {
        for (int i = 0; i < panel.transform.childCount; i++)
        {
           Object.Destroy(panel.transform.GetChild(i).gameObject);
        }
    }

    public static Color32 GetSliderActivatedButtonColor()
    {
        return new Color32(255, 255, 255, 255);
    }

    public static Color32 GetSliderDeactivatedButtonColor()
    {
        return new Color32(80, 80, 80, 255);
    }

}
