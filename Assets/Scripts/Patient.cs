﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Patient
{
    public int ID { get; set; }
    public string Title { get; set; }
    public string FName { get; set; }
    public string LName { get; set; }
    public string Email { get; set; }
    public string Pwork { get; set; }
    public string Pmobile { get; set; }
    public string Gender { get; set; }
    public int DoctorID { get; set; }
    public string Houseno { get; set; }
    public string Locality { get; set; }
    public string Town { get; set; }
    public string Pincode { get; set; }
    public string state { get; set; }

}
