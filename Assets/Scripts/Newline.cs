﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using UnityEngine.UI;
using System.Linq;
public class Newline : MonoBehaviour, INewline
{
    //=============================================with Gl line=========================
    // void Start()
    // {
    //     for (int i = 0; i < body.transform.childCount; i++)
    //     {
    //         if (body.transform.GetChild(i).gameObject.tag == "needle" || body.transform.GetChild(i).gameObject.tag == "point")
    //         {
    //             points.Add(body.transform.GetChild(i).gameObject);
    //         }
    //     }
    // }
    // void Update()
    // {

    // }
    // void DrawConnectingLines()
    // {
    //     if (points.Count > 1)
    //     {
    //         for (int i = 0; i < points.Count-1; i++)
    //         {
    //             Vector3 mainPointPos = points[i].transform.position;
    //             Vector3 pointPos = points[i+1].transform.position;
    //             GL.Begin(GL.LINES);
    //             lineMat.SetPass(0);
    //             GL.Color(new Color(lineMat.color.r, lineMat.color.g, lineMat.color.b, lineMat.color.a));
    //             GL.Vertex3(mainPointPos.x, mainPointPos.y, mainPointPos.z);
    //             GL.Vertex3(pointPos.x, pointPos.y, pointPos.z);
    //             GL.End();              
    //         }
    //     }
    // }
    // void OnPostRender()
    // {
    //     points.Clear();        
    //     for (int i = 0; i < body.transform.childCount; i++)
    //     {
    //         if (body.transform.GetChild(i).gameObject.tag == "needle" || body.transform.GetChild(i).gameObject.tag == "point")
    //         {
    //             points.Add(body.transform.GetChild(i).gameObject);          
    //         }            
    //     }       
    //     DrawConnectingLines();
    // }
    //===================================================================================
    private int segment = 60;
    private GuiPanels _guiPanels;
    public static List<GameObject> needlespoints = new List<GameObject>();
    public static Dictionary<string, List<GameObject>> LinesNeedles = new Dictionary<string, List<GameObject>>();
    public VectorLine Line;
    public List<VectorLine> curvelines = new List<VectorLine>();
    public static Dictionary<string, List<VectorLine>> AllCurveLines = new Dictionary<string, List<VectorLine>>();
    public static string selectedLineName = string.Empty;
    public static List<string> MeridianNames = new List<string>();
    void Start()
    {
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        DeleteAllLine();
        RefreshNeedles();
        CreatelinesInitially();
    }

    public void CreatelinesInitially()
    {
        for (int j = 0; j < MeridianNames.Count; j++)
        {
            if (LinesNeedles[MeridianNames[j]].Count >= 4)
            {
                curvelines = new List<VectorLine>();
                List<GameObject> obj = LinesNeedles[MeridianNames[j]];
                string colorcode = obj[0].GetComponent<ChangeNeedleposition>().color;
                Color c = GetColor(colorcode);
                for (int i = 0; i <= LinesNeedles[MeridianNames[j]].Count - 4; i++)
                {
                    Line = new VectorLine(MeridianNames[j], new Vector3[segment + 1], null, 1.5f, LineType.Continuous, Joins.Weld);
                    Line.drawTransform = _guiPanels.Body.transform;
                    Line.SetColor(c);
                    Line.MakeCurve(obj[i].transform.localPosition, obj[i + 1].transform.localPosition, obj[i + 3].transform.localPosition, obj[i + 2].transform.localPosition, segment);
                    Line.Draw3D();
                    curvelines.Add(Line);
                    i = i + 2;
                }
                AllCurveLines.Add(MeridianNames[j], curvelines);
            }
        }
    }

    public void DeleteAllLine()
    {
        VectorLine.Destroy(curvelines);
        for (int j = MeridianNames.Count - 1; j >= 0; j--)
        {
            if (AllCurveLines.ContainsKey(MeridianNames[j]))
            {
                VectorLine.Destroy(AllCurveLines[MeridianNames[j]]);
            }
        }
        MeridianNames.Clear();
        AllCurveLines.Clear();
        LinesNeedles.Clear();
        curvelines.Clear();
    }
    public void RefreshNeedles()
    {
        //needlespoints = new List<GameObject>();
        LinesNeedles.Clear();
        MeridianNames.Clear();
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        for (int i = 0; i < _guiPanels.Body.transform.childCount; i++)
        {
            if (_guiPanels.Body.transform.GetChild(i).gameObject.tag == CommonTypes.PrefabTag.needle.ToString() || _guiPanels.Body.transform.GetChild(i).gameObject.tag == CommonTypes.PrefabTag.point.ToString())
            {
                //needlespoints.Add(body.transform.GetChild(i).gameObject);
                string name = _guiPanels.Body.transform.GetChild(i).GetComponent<ChangeNeedleposition>().LineName;
                if (LinesNeedles.ContainsKey(name))
                {
                    LinesNeedles[name].Add(_guiPanels.Body.transform.GetChild(i).gameObject);
                    transform.GetChild(i).GetComponent<ChangeNeedleposition>().objectnumber = LinesNeedles[name].IndexOf(transform.GetChild(i).gameObject);
                }
                else
                {
                    if (PointVisibilityScript.isSessionOperating)
                    {
                        List<Needles> needles = PointVisibilityScript.selectedNeedles.Values.Where(p => p.Linename == name).ToList();
                        if (needles.Count > 0)
                        {
                            AddMerdianNameAndNeedles(i, name);
                        }
                    }
                    else
                    {
                        AddMerdianNameAndNeedles(i, name);
                    }
                }
            }
        }
    }
    public void DeleteLineOnSessionNeedleRemoval(string linename)
    {
        RefreshNeedles();
        List<Needles> needles = PointVisibilityScript.selectedNeedles.Values.Where(p => p.Linename == linename).ToList();
        if (needles.Count <= 0)
        {
            VectorLine.Destroy(AllCurveLines[linename]);
            AllCurveLines.Remove(linename);
        }
        updateall();
    }
    public void createLineOnSessionNeedleSelection(string linename)
    {
        RefreshNeedles();
        if (!AllCurveLines.ContainsKey(linename))
        {
            if (LinesNeedles[linename].Count >= 4)
            {
                curvelines = new List<VectorLine>();
                List<GameObject> obj = LinesNeedles[linename];
                string colorcode = obj[0].GetComponent<ChangeNeedleposition>().color;
                Color c = GetColor(colorcode);
                for (int i = 0; i <= LinesNeedles[linename].Count - 4; i++)
                {
                    Line = new VectorLine(linename, new Vector3[segment + 1], null, 1.5f, LineType.Continuous, Joins.Weld);
                    Line.drawTransform = _guiPanels.Body.transform;
                    Line.SetColor(c);
                    Line.MakeCurve(obj[i].transform.localPosition, obj[i + 1].transform.localPosition, obj[i + 3].transform.localPosition, obj[i + 2].transform.localPosition, segment);
                    Line.Draw3D();
                    curvelines.Add(Line);
                    i = i + 2;
                }
                AllCurveLines.Add(linename, curvelines);
            }
        }
    }
    private void AddMerdianNameAndNeedles(int i, string name)
    {
        List<GameObject> newlineobject = new List<GameObject>();
        MeridianNames.Add(name);
        newlineobject.Add(_guiPanels.Body.transform.GetChild(i).gameObject);
        LinesNeedles.Add(name, newlineobject);
        transform.GetChild(i).GetComponent<ChangeNeedleposition>().objectnumber = LinesNeedles[name].IndexOf(transform.GetChild(i).gameObject);
    }

    //public void RefreshNeedlesForSessionEditor()
    //{
    //    LinesNeedles.Clear();
    //    MeridianNames.Clear();
    //    _guiPanels = Camera.main.GetComponent<GuiPanels>();
    //    for (int i = 0; i < _guiPanels.Body.transform.childCount; i++)
    //    {
    //        if (_guiPanels.Body.transform.GetChild(i).gameObject.tag == "needle" || _guiPanels.Body.transform.GetChild(i).gameObject.tag == "point")
    //        {
    //            //needlespoints.Add(body.transform.GetChild(i).gameObject);
    //            string name = _guiPanels.Body.transform.GetChild(i).GetComponent<ChangeNeedleposition>().LineName;
    //            if (LinesNeedles.ContainsKey(name))
    //            {
    //                LinesNeedles[name].Add(_guiPanels.Body.transform.GetChild(i).gameObject);
    //                transform.GetChild(i).GetComponent<ChangeNeedleposition>().objectnumber = LinesNeedles[name].IndexOf(transform.GetChild(i).gameObject);
    //            }
    //            else
    //            {
    //                List<Needles> needles = PointVisibilityScript.selectedNeedles.Values.Where(p => p.Linename == name).ToList();

    //                if (needles.Count > 0)
    //                {
    //                    List<GameObject> newlineobject = new List<GameObject>();
    //                    MeridianNames.Add(name);
    //                    newlineobject.Add(_guiPanels.Body.transform.GetChild(i).gameObject);
    //                    LinesNeedles.Add(name, newlineobject);
    //                    transform.GetChild(i).GetComponent<ChangeNeedleposition>().objectnumber = LinesNeedles[name].IndexOf(transform.GetChild(i).gameObject);
    //                }
    //            }
    //        }
    //    }

    //}
    public static Color32 GetColor(string color)
    {
        string red = color.Substring(0, 3);
        byte r = byte.Parse(red);
        string green = color.Substring(3, 3);
        byte g = byte.Parse(green);
        string blue = color.Substring(6, 3);
        byte b = byte.Parse(blue);
        string alpha = color.Substring(9, 3);
        byte a = byte.Parse(alpha);
        Color32 newcolor = new Color32(r, g, b, a);
        return newcolor;
    }
    /// <summary>
    /// Checks whether the meridian is under the mouse pointer
    /// if meridian is under the mouse pointer then change the color of meridian
    /// on scroll Click select the hovered meridain for pc versions
    /// on Single touch select the hovered meridain for Mobile version
    /// </summary>
    void Update()
    {
        if (!JessicaRotationScript.isPaused)
        {
#if UNITY_STANDALONE
            CheckandChangeLinecoloronMousehover();
#endif
#if UNITY_STANDALONE
            if (Input.GetMouseButtonDown(2))
            {
#endif
#if UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount == 1 && Input.touches[0].tapCount == 1)
        {
#endif
                for (int i = 0; i < MeridianNames.Count; i++)
                {
                    if (AllCurveLines.ContainsKey(MeridianNames[i]))
                    {
                        for (int j = 0; j < AllCurveLines[MeridianNames[i]].Count; j++)
                        {
#if UNITY_STANDALONE
                            if (AllCurveLines[MeridianNames[i]][j].Selected(Input.mousePosition))
                            {
#endif
#if UNITY_ANDROID || UNITY_IOS
                        if (AllCurveLines[MeridianNames[i]][j].Selected(Input.touches[0].position))
                        {
#endif
                                selectedLineName = MeridianNames[i];
                                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = selectedLineName;
                                Camera.main.GetComponent<NeedleScript>()._previousneedle = LinesNeedles[selectedLineName][LinesNeedles[selectedLineName].Count - 1];
                                string color = LinesNeedles[MeridianNames[i]][0].GetComponent<ChangeNeedleposition>().color;
                                Color32 c = GetColor(color);
                                GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().color = c;
                                byte w = 100;
                                byte alpha = (byte)(c.a - w);
                                if (alpha < 0)
                                {
                                    alpha = 255;
                                }
                                c.a = alpha;
                                AllCurveLines[MeridianNames[i]][j].SetColor(c);
                                break;
                            }
                        }
                        if (selectedLineName != string.Empty && selectedLineName != "No Meridian Selected Scroll Click on Needle to select")
                        {
                            if (AllCurveLines.ContainsKey(selectedLineName))
                            {
                                for (int j = 0; j < AllCurveLines[selectedLineName].Count; j++)
                                {
                                    GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = selectedLineName;
                                    Camera.main.GetComponent<NeedleScript>()._previousneedle = LinesNeedles[selectedLineName][LinesNeedles[selectedLineName].Count - 1];
                                    string color = LinesNeedles[selectedLineName][0].GetComponent<ChangeNeedleposition>().color;
                                    Color32 c = GetColor(color);
                                    byte w = 100;
                                    byte alpha = (byte)(c.a - w);
                                    if (alpha < 0)
                                    {
                                        alpha = 255;
                                    }
                                    c.a = alpha;
                                    AllCurveLines[selectedLineName][j].SetColor(c);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void CheckandChangeLinecoloronMousehover()
    {
        for (int i = 0; i < MeridianNames.Count; i++)
        {
            if (AllCurveLines.ContainsKey(MeridianNames[i]))
            {
                for (int j = 0; j < AllCurveLines[MeridianNames[i]].Count; j++)
                {
                    if (AllCurveLines[MeridianNames[i]][j].Selected(Input.mousePosition))
                    {
                        string color = LinesNeedles[MeridianNames[i]][0].GetComponent<ChangeNeedleposition>().color;
                        Color32 c = GetColor(color);
                        byte w = 100;
                        byte alpha = (byte)(c.a - w);
                        if (alpha < 0)
                        {
                            alpha = 255;
                        }
                        c.a = alpha;
                        AllCurveLines[MeridianNames[i]][j].SetColor(c);
                    }
                    else
                    {
                        if (MeridianNames[i] != selectedLineName)
                        {
                            string color = LinesNeedles[MeridianNames[i]][0].GetComponent<ChangeNeedleposition>().color;
                            Color32 c = GetColor(color);
                            AllCurveLines[MeridianNames[i]][j].SetColor(c);
                        }
                    }
                }
            }
        }
    }

    public float linewidth;
    public void updateCurve(int objectnumber, string linename)
    {
        RefreshNeedles();
        linewidth = (61 - Camera.main.fieldOfView) * 0.17f;
        if (linewidth < 1.5f)
        {
            linewidth += 1.5f;
        }
        int curveindex = objectnumber / 3;
        int i = objectnumber - (objectnumber % 3);
        List<GameObject> objs = LinesNeedles[linename];
        //If selected object is not last needle in the Meridian and then update the curve
        if (objectnumber != objs.Count - 1)
        {
            if (AllCurveLines.ContainsKey(linename))
            {
                if (curveindex < AllCurveLines[linename].Count && curveindex >= 0)
                    AllCurveLines[linename][curveindex].MakeCurve(objs[i].transform.localPosition, objs[i + 1].transform.localPosition, objs[i + 3].transform.localPosition, objs[i + 2].transform.localPosition, segment);
                AllCurveLines[linename][curveindex].Draw3D();
            }
        }
        //if the selected object is point then update curve
        if (objectnumber != 0 && objectnumber % 3 == 0)
        {
            i = objectnumber - 3;
            if (AllCurveLines.ContainsKey(linename))
            {
                if (curveindex - 1 >= 0 && curveindex - 1 < AllCurveLines[linename].Count)
                {
                    AllCurveLines[linename][curveindex - 1].MakeCurve(objs[i].transform.localPosition, objs[i + 1].transform.localPosition, objs[i + 3].transform.localPosition, objs[i + 2].transform.localPosition, segment);
                    AllCurveLines[linename][curveindex - 1].Draw3D();
                }
            }
        }
        foreach (var lines in AllCurveLines)
        {
            foreach (var line in lines.Value)
            {
                line.lineWidth = linewidth;
            }
        }
    }
    public void AddNewCurve()
    {
        linewidth = (61 - Camera.main.fieldOfView) * 0.17f;
        if (linewidth < 1.5f)
        {
            linewidth += 1.5f;
        }
        if (MeridianNames.Contains(selectedLineName) == false)
        {
            MeridianNames.Add(selectedLineName);
        }
        List<GameObject> obj = LinesNeedles[selectedLineName];
        string colorcode = obj[0].GetComponent<ChangeNeedleposition>().color;
        Color c = GetColor(colorcode);
        if (obj.Count >= 4)
        {
            int i = LinesNeedles[selectedLineName].Count - 4;
            Line = new VectorLine(selectedLineName, new Vector3[segment + 1], null, linewidth, LineType.Continuous, Joins.Weld);
            Line.drawTransform = _guiPanels.Body.transform;
            Line.SetColor(c);
            Line.MakeCurve(obj[i].transform.localPosition, obj[i + 1].transform.localPosition, obj[i + 3].transform.localPosition, obj[i + 2].transform.localPosition, segment);
            Line.Draw3D();
            if (AllCurveLines.ContainsKey(selectedLineName))
            {
                AllCurveLines[selectedLineName].Add(Line);
            }
            else
            {
                curvelines = new List<VectorLine>();
                curvelines.Add(Line);
                AllCurveLines.Add(selectedLineName, curvelines);
            }
        }
    }
    public void changecolor(Color color)
    {

        if (selectedLineName != string.Empty)
        {
            foreach (var line in AllCurveLines[selectedLineName])
            {
                line.SetColor(color);
            }
            GuiPanels.guiPanels.MeridianPanel.transform.GetChild(0).GetChild(1).GetComponent<Text>().color = color;
            Color32 c = color;
            RefreshNeedles();
            updateall();
            string colorstring = getthreedigit(c.r.ToString()) + getthreedigit(c.g.ToString()) + getthreedigit(c.b.ToString()) + getthreedigit(c.a.ToString());
            foreach (var item in LinesNeedles[selectedLineName])
            {
                if (item.tag == CommonTypes.PrefabTag.point.ToString())
                {
                    item.GetComponent<Renderer>().material.color = color;
                }
                else if (item.tag == CommonTypes.PrefabTag.needle.ToString())
                {
                    item.GetComponent<ChangeNeedleposition>().NameLabel.GetComponent<Text>().color = color;
                }
                item.GetComponent<ChangeNeedleposition>().color = colorstring;
            }
            LineColor linecolor = new LineColor()
            {
                LineName = selectedLineName,
                Color = colorstring
            };
            Camera.main.GetComponent<NeedleOperationScript>().Changecolor(linecolor);
        }
    }
    public static string getthreedigit(string b)
    {
        if (b.Length == 1)
        {
            return "00" + b;
        }
        else if (b.Length == 2)
        {
            return "0" + b;
        }
        else
        {
            return b;
        }
    }
    public void updateall()
    {
        RefreshNeedles();
        linewidth = (61 - Camera.main.fieldOfView) * 0.17f;
        if (linewidth < 1.5f)
        {
            linewidth += 1.5f;
        }
        for (int j = 0; j < MeridianNames.Count; j++)
        {
            if (LinesNeedles[MeridianNames[j]].Count >= 4)
            {
                List<GameObject> obj = LinesNeedles[MeridianNames[j]];
                int LineIndex = 0;
                for (int i = 0; i <= LinesNeedles[MeridianNames[j]].Count - 4; i++)
                {
                    if (AllCurveLines.ContainsKey(MeridianNames[j]))
                    {
                        if (AllCurveLines[MeridianNames[j]].Count > LineIndex)
                        {
                            AllCurveLines[MeridianNames[j]][LineIndex].drawTransform = _guiPanels.Body.transform;
                            AllCurveLines[MeridianNames[j]][LineIndex].lineWidth = linewidth;
                            AllCurveLines[MeridianNames[j]][LineIndex].MakeCurve(obj[i].transform.localPosition, obj[i + 1].transform.localPosition, obj[i + 3].transform.localPosition, obj[i + 2].transform.localPosition, segment);
                            AllCurveLines[MeridianNames[j]][LineIndex].Draw3D();
                            i = i + 2;
                            LineIndex++;
                        }
                    }
                }
            }
        }
    }
}
