﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuiPanels : MonoBehaviour
{
    public GameObject Menupanel;
    public GameObject Treatmentpanel;
    public GameObject Patientpanel;
    public GameObject Addprofilepanel;
    public GameObject AddprofileContentpanel;
    public GameObject Mainmenupanel;
    public GameObject Buttonpanel;
    public GameObject ErrorPanel;
    public GameObject EditProfilePanel;
    public GameObject EditprofileContentpanel;
    public GameObject patientselectionpanel;
    public GameObject SessionEditorPanel;
    public GameObject ColorBox;
    public GameObject MeridianPanel;
    public GameObject transparencyPanel;
    public GameObject instructionPanel;
    public GameObject sessionHistoryPanel;
    public GameObject Labelcanvas;
    public GameObject patientDetailPanel;
    public GameObject FinancePanel;
    public GameObject LoginPanel;
    public GameObject Userloginpanel;
    public GameObject SignUppanel;
    public GameObject SentbillContentPanel;
    public GameObject PaymentContentPanel;
    public static GuiPanels guiPanels;
    public GameObject ConfirmationPanel;
    public GameObject SessionNeedleFilterContentPanel;
    public GameObject AdminPanel;
    public GameObject AppointmentContentPanel;
    [HideInInspector]
    public GameObject Body;
    public GameObject AppointmentPanel;
    void Start()
    {
        guiPanels = Camera.main.GetComponent<GuiPanels>();
        
        Body = GameObject.Find("Jessica");
        if (Body != null)
        {
            Body.SetActive(false);
        }
    }
}
