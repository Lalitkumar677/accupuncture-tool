﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Boomlagoon.JSON;
using UnityEngine.UI;
using System.Text;

public class NeedleOperationScript : MonoBehaviour, INeedleOperationScript
{
    private ServiceUrlscript serviceUrlscript;
    private NeedleScript _needlescript;
    private ShowPanelsScript _showPanelsScript;
    private UserIdScript _userIdScript;
    private GuiPanels _guiPanels;
    public GameObject needleprefab;
    public GameObject pointprefab;
    public static bool isSessionCompleted;

    void Start()
    {
        serviceUrlscript = new ServiceUrlscript();
        isSessionCompleted = false;
        GetAndAssignComponent();
    }

    private void GetAndAssignComponent()
    {
        _showPanelsScript = Camera.main.GetComponent<ShowPanelsScript>();
        _guiPanels = Camera.main.GetComponent<GuiPanels>();
        _needlescript = Camera.main.GetComponent<NeedleScript>();
        _userIdScript = Camera.main.GetComponent<UserIdScript>();
    }

    /// <summary>
    /// Loads the doctor's patient and needles after successful login
    /// </summary>
    public void LoadDoctorDataAfterLogin()
    {
        _guiPanels.Menupanel.transform.GetChild(6).GetComponentInChildren<Text>().text = (LoginOperationScript.LoginAs == CommonTypes.Roles.Admin) ?
          "Back" : "LogOut";
        int DoctorID = _userIdScript.DoctorID;
        LoadAllpatient(DoctorID);
        _showPanelsScript.ShowMessage(Language.Get("WORD_Loading") + "...");
        LoadNeedles();
    }
    /// <summary>
    /// Add the patient detail in www form object 
    /// </summary>
    /// <param name="patient">Patient object to add</param>
    public void AddPatient(Patient patient)
    {
        WWWForm form = new WWWForm();
        NeedleOperationHelper.AddPatientfieldinFormobject(patient, form);
        WWW www = new WWW(serviceUrlscript.addPatientUrl, form);
        StartCoroutine("AddPatients", www);
    }
    /// <summary>
    /// update the patient
    /// </summary>
    /// <param name="patient"></param>
    public void UpdatePatient(Patient patient)
    {
        WWWForm form = new WWWForm();
        if (patient.ID != 0)
        {
            form.AddField("PatientId", patient.ID);
            NeedleOperationHelper.AddPatientfieldinFormobject(patient, form);
            WWW www = new WWW(serviceUrlscript.updatePatientUrl, form);
            StartCoroutine("UpdatePatients", www);
        }
    }

    /// <summary>
    /// Wait untill service gets executed 
    /// </summary>
    /// <param name="www">WWW object with url to sevice and information of patient</param>
    /// <returns>Returns null </returns>
    IEnumerator UpdatePatients(WWW www)
    {
        yield return StartCoroutine("AddPatients", www);
    }

    /// <summary>
    /// Wait untill service gets executed 
    /// </summary>
    /// <param name="www">WWW object with url to sevice and information of patient</param>
    /// <returns>Returns null </returns>
    IEnumerator AddPatients(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            _showPanelsScript.HideMessageDialogbox();
            _showPanelsScript.ShowErrorPanel(Language.Get("WORD_Saved"), Language.Get("WORD_INFORMATION"));
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }
    /// <summary>
    ///Prints on unity console and Displays the Error to user and create log for the error
    /// </summary>
    /// <param name="www"></param>

    public void DeleteNeedle(int Id)
    {
        string _url = serviceUrlscript.deleteNeedleUrl + Id.ToString();
        WWW www = new WWW(_url);
        StartCoroutine("DeleteNeedle", www);
    }
    /// <summary>
    /// Wait untill service gets executed 
    /// </summary>
    /// <param name="www">WWW object with url to sevice</param>
    /// <returns>Returns null </returns>
    IEnumerator DeleteNeedle(WWW www)
    {
        yield return StartCoroutine("ExecuteRequest", new object[] { www, "Record Deleted" });
    }
    IEnumerator ExecuteRequest(object[] parameter)
    {
        WWW www = parameter[0] as WWW;
        string message = parameter[1].ToString();
        yield return www;
        if (www.error == null)
        {
            Debug.Log(message);
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }
    public IEnumerator LoadNeedlesWithWait()
    {
        string _url = serviceUrlscript.loadNeedlesUrl;
        WWW www = new WWW(_url);
        yield return StartCoroutine("LoadNeedless", www);
    }
    public void LoadNeedles()
    {
        string _url = serviceUrlscript.loadNeedlesUrl;
        WWW www = new WWW(_url);
        StartCoroutine("LoadNeedless", www);
    }
    public IEnumerator LoadNeedless(WWW www)
    {
        yield return www;
        // check for errors 
        _guiPanels.ErrorPanel.SetActive(false);
        JessicaRotationScript.isPaused = false;
        if (www.error == null)
        {
            PointVisibilityScript.MasterNeedles.Clear();
            JSONArray json = JSONArray.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            if (json.Length > 0)
            {
                for (int i = 0; i < json.Length; i++)
                {
                    Needles n = NeedleOperationHelper.CreateNeedleObjectfromjson(json, i);
                    PointVisibilityScript.MasterNeedles.Add(n);
                }
            }

        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }

    public void LoadAllpatient(int DoctorID)
    {
        string url = serviceUrlscript.loadDoctorpatient + DoctorID;
        WWW www = new WWW(url);
        StartCoroutine("LoadAllPatients", www);
    }
    /// <summary>
    /// Wait untill service gets executed
    /// store the loaded patients in List 
    /// </summary>
    /// <param name="www">WWW object with url to sevice</param>
    /// <returns>Returns null </returns>
    IEnumerator LoadAllPatients(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            JSONArray json = JSONArray.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            else
            {
                DropDownScript DropDown = _guiPanels.patientselectionpanel.transform.GetChild(2).GetComponent<DropDownScript>();
                DropDown.DropDownItem.Clear();
                DropDownScript DropDown2 = _guiPanels.EditProfilePanel.transform.GetChild(2).GetComponent<DropDownScript>();
                DropDown2.DropDownItem.Clear();
                if (json.Length > 0)
                {
                    for (int i = 0; i < json.Length; i++)
                    {
                        Patient patient = NeedleOperationHelper.CreatePatientObjectFromJson(json, i);
                        DropDown.DropDownItem.Add(patient);
                        DropDown2.DropDownItem.Add(patient);
                    }
                }
            }
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }
    public void AddNeedle(Needles needle, GameObject prened, GameObject needleGameObject)
    {
        WWWForm form = NeedleOperationHelper.CreateFormObjectFromNeedleInfo(needle);
        WWW www = new WWW(serviceUrlscript.addNeedleUrl, form);
        object[] para = new object[] { www, needleGameObject, name, prened };
        StartCoroutine("AddNeedles", para);
    }
    public void UpdateNeedle(Needles needle)
    {
        WWWForm form = NeedleOperationHelper.CreateFormObjectFromNeedleInfo(needle);
        WWW www = new WWW(serviceUrlscript.updateNeedleUrl, form);
        StartCoroutine("UpdateNeedles", www);
    }
    /// <summary>
    /// Wait untill service gets executed 
    /// </summary>
    /// <param name="www">WWW object with url to sevice and information of Needle</param>
    /// <returns>Returns null </returns>
    IEnumerator UpdateNeedles(WWW www)
    {
        yield return StartCoroutine("ExecuteRequest", new object[] { www, "Record Updated" });
    }
    /// <summary>
    /// Wait untill service gets executed
    /// </summary>
    /// <param name="parameters">an aaray with, WWW object with url to sevice, Needle to be stored, Name of needle, Previous needle</param>
    /// <returns>Returns null </returns>
    IEnumerator AddNeedles(object[] parameters)
    {
        WWW www = (WWW)parameters[0];
        yield return www;
        GameObject needle = (GameObject)parameters[1];
        string name = parameters[2].ToString();
        GameObject prened = (GameObject)parameters[3];
        if (www.error == null)
        {
            Debug.Log("Record Inserted");
            JSONObject json = JSONObject.Parse(www.text);
            needle.GetComponent<NeedleId>().Id = int.Parse(json["Id"].ToString());
            needle.GetComponent<NeedleId>().Name = name;
            _guiPanels.Body.GetComponent<Newline>().AddNewCurve();
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
            _needlescript._previousneedle = prened;
            if (_needlescript.textbox != null)
            {
                DestroyImmediate(_needlescript.textbox);
            }
            if (needle.GetComponent<ChangeNeedleposition>() != null)
            {
                needle.GetComponent<ChangeNeedleposition>().NameLabel.transform.SetParent(null);
                DestroyImmediate(needle.GetComponent<ChangeNeedleposition>().NameLabel);
            }
            DestroyImmediate(needle);
        }
    }
    public void LoadPatient(string pid, GameObject title, GameObject fname, GameObject lname, GameObject email, GameObject pwork, GameObject pmobile)
    {
        string url = serviceUrlscript.loadPatientUrl + pid;
        WWW www = new WWW(url);
        object[] para = new object[] { www, title, fname, lname, email, pwork, pmobile };
        StartCoroutine("LoadPatients", para);
    }
    /// <summary>
    /// Wait untill service gets executed
    /// </summary>
    /// <param name="para">an aaray with, WWW object with url to sevice, title label,first name label,last name label, email id label,work phone number label, mobile number label  </param>
    /// <returns></returns>
    IEnumerator LoadPatients(object[] para)
    {
        WWW www = (WWW)para[0];
        yield return www;
        GameObject titlefield = (GameObject)para[1];
        GameObject fnamefield = (GameObject)para[2];
        GameObject lnamefield = (GameObject)para[3];
        GameObject emailfield = (GameObject)para[4];
        GameObject pworkfield = (GameObject)para[5];
        GameObject pmobilefield = (GameObject)para[6];
        if (www.error == null)
        {
            JSONObject json = JSONObject.Parse(www.text);
            titlefield.GetComponent<Text>().text = json["Title"].Str;
            fnamefield.GetComponent<Text>().text = json["FirstName"].Str;
            lnamefield.GetComponent<Text>().text = json["LastName"].Str;
            emailfield.GetComponent<Text>().text = json["Email"].Str;
            pworkfield.GetComponent<Text>().text = json["PhoneNo"].Str;
            pmobilefield.GetComponent<Text>().text = json["MobileNo"].Str;
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
            LogScript.Log(www);
            _showPanelsScript.ShowErrorPanel(www.error, Language.Get("WORD_Error"));
        }
    }
    /// <summary>
    /// Save the session in the database
    /// </summary>
    /// <param name="date"></param>
    /// <param name="patientid"></param>
    /// <param name="duration"></param>
    /// <param name="SessionNeedles"></param>
    public void SaveCurrentSession(string date, string patientid, string duration, List<Needles> SessionNeedles)
    {
        string JsonForSession = NeedleOperationHelper.CreateJsonForsession(date, patientid, duration, SessionNeedles);
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] body = Encoding.UTF8.GetBytes(JsonForSession);
        WWW www = new WWW(serviceUrlscript.AddSessionUrl, body, headers);
        StartCoroutine("AddSession", www);
    }


    /// <summary>
    ///  Wait untill service gets executed
    /// </summary>
    /// <param name="www">WWW object with url to sevice</param>
    /// <returns>Return null</returns>
    IEnumerator AddSession(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            _showPanelsScript.HideMessageDialogbox();
            isSessionCompleted = true;
            _showPanelsScript.ShowErrorPanel(Language.Get("WORD_Saved"), Language.Get("WORD_INFORMATION"));
            StartCoroutine("LoadAllSession");
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
            DestroyRowsfromlist();
        }
    }
    /// <summary>
    ///Destroys the needle from the list when session is added in the database or user abort the session 
    /// </summary>
    public void DestroyRowsfromlist()
    {
        _guiPanels.SessionEditorPanel.transform.GetChild(2).GetComponentInChildren<Text>().text = Language.Get("Session_Duration");
        GameObject panel = _guiPanels.SessionEditorPanel.transform.GetChild(3).GetChild(0).gameObject;
        for (int i = 0; i < panel.transform.childCount; i++)
        {
            Destroy(panel.transform.GetChild(i).gameObject);
        }
    }
    /// <summary>
    /// insert the needle and point there are atleast one needle foe the given meridian
    /// </summary>
    /// <param name="needleObjects">list of points and needle</param>
    /// <param name="prened">previous needle which needs to be assigned for next insertion</param>
    /// <param name="needlename">name of the needle</param>
    public void AddNeedleslist(List<GameObject> needleObjects, GameObject prened, string needlename)
    {
        string JsonForNeedlesandPoint = NeedleOperationHelper.CreateJsonForNeedleAndpoint(needleObjects, prened, needlename);
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        byte[] body = Encoding.UTF8.GetBytes(JsonForNeedlesandPoint);
        WWW www = new WWW(serviceUrlscript.addNeedleListUrl, body, headers);
        object[] para = new object[] { www, needleObjects, prened };
        StartCoroutine("AddNeedleslists", para);
    }

    /// <summary>
    /// Wait untill service gets executed
    /// </summary>
    /// <param name="parameters">an aaray with, WWW object with url to sevice,list of Needle to be stored, Previous needle</param>
    /// <returns>Returns null </returns>
    IEnumerator AddNeedleslists(object[] parameters)
    {
        WWW www = (WWW)parameters[0];
        yield return www;
        List<GameObject> needle = (List<GameObject>)parameters[1];
        GameObject prened = (GameObject)parameters[2];
        if (www.error == null)
        {
            JSONObject json = JSONObject.Parse(www.text);
            string result = json["Id"].Str;
            string[] Ids = result.Split(',');
            for (int i = 0; i < needle.Count; i++)
            {
                needle[i].GetComponent<NeedleId>().Id = int.Parse(Ids[i]);
            }
            _guiPanels.Body.GetComponent<Newline>().AddNewCurve();
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
            _needlescript._previousneedle = prened;
            NeedleOperationHelper.DestroyNeedleAndPointsIFErrorOccure(needle, _needlescript);
        }   
    }

    public IEnumerator LoadAllSession()
    {
        WWW www = new WWW(serviceUrlscript.loadallsessions);
        yield return StartCoroutine("LoadAllSessions", www);
    }
    /// <summary>
    /// Wait untill service gets executed
    /// store the loaded sessions in List 
    /// </summary>
    /// <param name="www">WWW object with url to sevice</param>
    /// <returns>Returns null </returns>
    IEnumerator LoadAllSessions(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            JSONArray json = JSONArray.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            else
            {
                if (json.Length > 0)
                {
                    for (int i = 0; i < json.Length; i++)
                    {
                        Session session = NeedleOperationHelper.CreateSessionObjectfromJson(json, i);
                        PointVisibilityScript.AllPatientsessions.Add(session);
                    }
                }
            }
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }
    public void LoadsessionNeedles(int sessionID)
    {
        string url = serviceUrlscript.loadSessionNeedles + sessionID;
        WWW www = new WWW(url);
        StartCoroutine("LoadsessionNeedlesEn", www);
    }
    /// <summary>
    /// Wait untill service gets executed
    /// store the loaded needle for a session in List 
    /// </summary>
    /// <param name="www">WWW object with url to sevice</param>
    /// <returns>Returns null </returns>
    IEnumerator LoadsessionNeedlesEn(WWW www)
    {
        yield return www;
        if (www.error == null)
        {
            JSONArray json = JSONArray.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            else
            {
                if (json.Length > 0)
                {
                    PointVisibilityScript.selectedSession.Needles = new List<Needles>();
                    for (int i = 0; i < json.Length; i++)
                    {
                        Needles needle = NeedleOperationHelper.CreateNeedleObjectfromjson(json, i);
                        PointVisibilityScript.selectedSession.Needles.Add(needle);
                    }
                    PointVisibilityScript script = Camera.main.GetComponent<PointVisibilityScript>();
                    script.CreateNeedlesInScrollViewforSession(PointVisibilityScript.selectedSession);
                }
            }
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }
    public void Changecolor(LineColor linecolor)
    {
        WWWForm form = new WWWForm();
        form.AddField("LineName", linecolor.LineName);
        form.AddField("Color", linecolor.Color);
        WWW www = new WWW(serviceUrlscript.changecolorurl, form);
        StartCoroutine("Changecolors", www);
    }
    /// <summary>
    /// Wait untill service gets executed
    /// Changes the color of all needles and point 
    /// </summary>
    /// <param name="www">WWW object with url to sevice</param>
    /// <returns>Returns null </returns>
    IEnumerator Changecolors(WWW www)
    {
        yield return StartCoroutine("ExecuteRequest", new object[] { www, "Changed" });
    }
    public void LoadAllDoctorsForAdmin(int companyID)
    {
        string url = serviceUrlscript.LoadAllDoctorsbyAdmin + companyID.ToString();
        WWW www = new WWW(url);
        StartCoroutine("LoadAllDoctor", www);
    }
    IEnumerator LoadAllDoctor(WWW www)
    {
        yield return www;
        _showPanelsScript.HideMessageDialogbox();
        if (www.error == null)
        {
            JSONArray json = JSONArray.Parse(www.text);
            if (json == null)
            {
                Debug.Log("No data converted");
            }
            else
            {
                if (json.Length > 0)
                {
                    DoctorDropDownScript DropDown = _guiPanels.AdminPanel.transform.GetChild(2).GetComponent<DoctorDropDownScript>();
                    DropDown.DropDownItem.Clear();
                    for (int i = 0; i < json.Length; i++)
                    {
                        Doctor doctor = PdfGenerateScript.CreateDoctorFromJsonArray(json, i);
                        DropDown.DropDownItem.Add(doctor);
                    }
                }
            }
        }
        else
        {
            NeedleOperationHelper.DisplayErrorTouserAndCreateLog(www, _showPanelsScript);
        }
    }
}
